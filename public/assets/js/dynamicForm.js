function formDynamicGakken( label ) {

    var g = {};
    g.additional = [];

    //select dom with first element if selected using name class
    var _ = function( name ) {
        var dom = null;

        //checking first word of string to choose whatever dom method to selecting element
        if ( name ) {
            switch ( name.charAt(0) ) {
                case '#':
                    dom = document.getElementById( name.slice(1) );
                    break;
                case '.':
                    dom = document.getElementsByClassName( name.slice(1) )[0];
                    break;
                default:
                    dom = document.getElementsByName( name )[0];
                    break;
            }
        }

        return dom;
    }

    //creating element that didn't show (hidden)
    var crtHideIinput = function( tag , name , val ) {
        var elm = document.createElement(tag);

        if ( tag === 'textarea' )
            elm.style = 'display:none';
        else
            elm.type  = 'hidden';

        elm.name  = name;
        elm.value = val;

        return elm;
    }

    var removeRow = function ( e ) {

        //get index of row we gonna remove it
        var i             = this.getAttribute('data-index') ,
            RemoveButton  = g.tableResult.querySelectorAll('.remove-' + label);

        //reorder data-index of column
        for (var index in RemoveButton) {
            if (RemoveButton.hasOwnProperty(index)) {
                var indexButton = parseInt(RemoveButton[index].getAttribute('data-index'));

                if ( indexButton > i ) RemoveButton[index].setAttribute( 'data-index' , (indexButton-1) );
            }
        }

        g.tableResult.deleteRow(i);


        //if there is no row data anymore, we should hide the table
        if ( g.tableResult.rows.length <= 1) {
            g.tableResult.classList.add('hidden');
        }
    }

    g.setResult = function( data ) {
        var iRow    = g.tableResult.rows.length;
        var newRow  = g.tableResult.insertRow( iRow );

        var cellNum = 0;
        var cell    = [];

        data.forEach( function( d ) {
            //looping all data we gonna show in the table result
            //and making input element, so we can save the data using post or put form
            if (d.parent) {
                //check if this data have a parent, and then adding it to the parent
                if ( !d.hide )
                    cell[d.parent].insertAdjacentHTML('beforeend', d.splitter + d.label);

                cell[d.parent].appendChild(
                    crtHideIinput( d.tag , label + '[' + (iRow-1) + '][' + d.key + ']' , d.val)
                );
            } else {
                cell[d.key] = newRow.insertCell( cellNum );

                if ( !d.hide )
                    cell[d.key].innerHTML   = d.label;

                cell[d.key].appendChild(
                    crtHideIinput( d.tag , label + '[' + (iRow-1) + '][' + d.key + ']' , d.val)
                );
                cellNum++;
            }

        });

        var lastcell = newRow.insertCell( cellNum );

        var a = document.createElement( 'a' );

        a.setAttribute( 'data-index' , ( iRow ) );
        a.classList.add( 'remove-' + label );
        a.innerHTML = '<b class="glyphicon glyphicon-remove"></b>';

        lastcell.appendChild( a );
        a.onclick = removeRow;

        g.renderAdditional( iRow-1 , lastcell );
        g.tableResult.classList.remove( 'hidden' );
    }

    g.setAdditonal = function( data ) {
        g.additional = data;
    }

    g.renderAdditional = function( row , cell ) {
        g.additional.forEach( function( data ){
            var a = document.createElement( 'a' );

            if ( data.att ) {
                for (var key in data.att) {
                    if (data.att.hasOwnProperty(key)) {
                        a.setAttribute( key , data.att[key] );
                    }
                }
            }

            a.innerHTML = data.html;

            cell.insertAdjacentHTML('beforeend', ' ');
            cell.appendChild(a);

            a.onclick = function( e ){
                data.click( e , this , cell , row );
            }
        });
    }

    g.setButton = function( button ) {
        g.btnAdd     = _( button.add )    ? _( button.add ) :  null;
        g.btnCancel  = _( button.cancel ) ? _( button.cancel ) : null;
        g.btnSubmit  = _( button.submit ) ? _( button.submit ) : null;
    }

    g.setElement = function( elm ) {
        g.formElm       = _( elm.form_elm )     ? _( elm.form_elm ) : null;
        g.tableResult   = _( elm.table_result ) ? _( elm.table_result ) : null;
    }

    g.setData = function( data ) {
        g.datas = data;
    }

    g.run = function() {
        g.btnAdd.onclick    = g.add;
        g.btnCancel.onclick = g.cancel;
        g.btnSubmit.onclick = g.submit;
    }

    g.add = function( e ) {
        g.formElm.classList.remove( 'hidden' );
        g.btnAdd.classList.add( 'hidden' );
    }

    g.cancel = function( e ) {
        g.formElm.classList.add( 'hidden' );
        g.btnAdd.classList.remove( 'hidden' );
    }

    g.submit = function( e ) {
        var arr        = [];

        //get value data from input element and passing it to array that we gonna show in result
        g.datas.forEach( function( arrData) {
            var elm = _( arrData.elm );

            if ( elm.tagName === 'SELECT' || elm.tagName === 'select' )
                arrData.label = elm.options[elm.selectedIndex].text;
            else
                arrData.label = elm.value;

            arrData.val = elm.value;
            arr.push( arrData );
        });

        g.setResult( arr );

        g.resetForm();
    }

    g.resetForm = function(e) {
        g.formElm.classList.add( 'hidden' );
        g.btnAdd.classList.remove( 'hidden' );

        g.datas.forEach( function( arrData) {
            var elm = _( arrData.elm );

            if ( elm.tagName != 'SELECT' && elm.tagName != 'select' ) {
                elm.value = '';
            }
        });
    }

    return g;
}
