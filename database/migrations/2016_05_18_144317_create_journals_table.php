<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journals', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('journal_category_id')->unsigned();
            $table->string('issn', 32)->unique()->nullable();
            $table->string('title');
            $table->text('description');
            $table->string('publisher')->nullable();
            $table->string('editor')->nullable();
            $table->string('image_url')->nullable();
            $table->boolean('is_open_access')->default(false);
            $table->integer('created_by')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('journals');
    }
}
