<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTopicsAddIsFreeCertifcateColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topics', function ($table) {
            $table->boolean('is_free_certificate')->after('is_open_access')->nullable()->default(false);
        });
    }

    /**
 * Reverse the migrations.
 *
 * @return void
 */
    public function down()
    {
        Schema::table('topics', function ($table) {
            $table->dropColumn('is_free_certificate');
        });
    }
}
