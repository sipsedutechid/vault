<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertOptionQuizInColumnAppearanceContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contents', function($table) {
            $table->dropColumn('appearance');
        });

        Schema::table('contents', function($table) {
            $table->enum('appearance', ['text',  'image', 'video', 'audio', 'quiz'])->nullable()->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contents', function($table){
            $table->dropColumn('appearance');
        });

        Schema::table('contents', function($table){
            $table->enum('appearance', ['text',  'image', 'video', 'audio'])->nullable()->after('type');
        });
    }
}
