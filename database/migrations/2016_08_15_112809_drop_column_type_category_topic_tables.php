<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnTypeCategoryTopicTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contents', function($table){
            $table->dropColumn('type');
            $table->dropColumn('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contents', function($table){
            $table->enum('category', ['text', 'embed', 'link', 'file'])->nullable();
            $table->enum('type', ['text',  'image', 'video', 'audio', ])->nullable();
        });
    }
}
