<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDefaultValuePuslishedAtTableTopics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topics', function($table){
            $table->dropColumn('publish_at');
        });

        Schema::table('topics', function($table){
            $table->timestamp('publish_at')->nullable()->after('post_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topics', function($table){
            $table->dropColumn('publish_at');
        });

        Schema::table('topics', function($table){
            $table->timestamp('publish_at')->after('post_status');
        });
    }
}
