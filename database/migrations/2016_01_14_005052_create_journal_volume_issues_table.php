<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalVolumeIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journal_volume_issues', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('journal_id')->unsigned();
            $table->integer('journal_volume_id')->unsigned();
            $table->integer('sequence')->unsigned();
            $table->date('published_at')->nullable();
            $table->integer('created_by')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('journal_volume_issues');
    }
}
