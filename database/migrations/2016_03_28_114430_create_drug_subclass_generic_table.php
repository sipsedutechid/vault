<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrugSubclassGenericTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('generics', function (Blueprint $table)
        {
            $table->dropColumn('drug_class_id');
            $table->dropColumn('drug_subclass_id');
        });

        Schema::create('drug_subclass_generic', function (Blueprint $table)
        {
            $table->integer('drug_subclass_id')->unsigned();
            $table->integer('generic_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('drug_subclass_generic');
        Schema::table('generics', function (Blueprint $table)
        {
            $table->integer('drug_class_id')->unsigned()->nullable();
            $table->integer('drug_subclass_id')->unsigned()->nullable();
        });
    }
}
