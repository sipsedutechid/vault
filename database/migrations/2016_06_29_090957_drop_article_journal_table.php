<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropArticleJournalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('article_journal');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('article_journal', function (Blueprint $table)
        {
            $table->integer('article_id')->unsigned();
            $table->integer('journal_id')->unsigned();
            $table->integer('journal_volume_id')->unsigned()->nullable();
            $table->integer('journal_volume_issue_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }
}
