<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrugsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drugs', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('name');
            $table->integer('drug_class_id')->unsigned();
            $table->integer('drug_subclass_id')->unsigned();
            $table->integer('drug_manufacturer_id')->unsigned();
            $table->text('indication');
            $table->text('contraindication');
            $table->text('precautions');
            $table->text('adverse_reactions');
            $table->text('interactions');
            $table->enum('administration', ['cc', 'scc', 's/cc']);
            $table->text('administration_extra');
            $table->enum('classification', ['o', 'g', 'w', 'b']);
            $table->enum('pregnancy_category', ['a', 'b', 'c', 'd', 'x'])->nullable();
            $table->integer('created_by')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('drugs');
    }
}
