<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReanameColumnReleaseMonthYeraToRelease extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topics', function($table){
            $table->dropColumn('release_year_month');
            $table->integer('release')->after('created_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topics', function($table){
            $table->dropColumn('release');
            $table->string('release_year_month')->after('created_by');
        });
    }
}
