<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnJournalVolumeArticles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::table('articles', function($table){
             $table->integer('journal_volume')->unsigned()->after('journal_id');
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::table('articles', function($table){
             $table->dropColumn('journal_volume');
         });
     }
}
