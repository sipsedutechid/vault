<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTypeAppearanceTopicTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contents', function($table) {
            $table->enum('type', ['text', 'embed', 'link', 'file'])->nullable()->after('data');
            $table->enum('appearance', ['text',  'image', 'video', 'audio', ])->nullable()->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contents', function($table){
            $table->dropColumn('appearance');
            $table->dropColumn('type');
        });
    }
}
