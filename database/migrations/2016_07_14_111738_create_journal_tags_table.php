<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalTagsTable extends Migration
{
    /**
        * Run the migrations.
        *
        * @return void
    */
    public function up()
    {
        Schema::create('journal_tags', function (Blueprint $table) {
            $table->string('tag');
            $table->integer('journal_id')->unsigned();
        });
    }

    /**
        * Reverse the migrations.
        *
        * @return void
    */
    public function down()
    {
        Schema::drop('journal_tags');
    }
}
