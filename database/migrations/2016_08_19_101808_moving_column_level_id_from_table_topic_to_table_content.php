<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MovingColumnLevelIdFromTableTopicToTableContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topics', function( $table ) {
            $table->dropColumn('level_id');
        });

        Schema::table('contents', function( $table ) {
            $table->integer('level_id')->nullable()->after('appearance');
        });

        Schema::rename('topic_levels', 'content_levels');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topics', function( $table ) {
            $table->integer('level_id')->nullable()->after('is_p2kb');
        });

        Schema::table('contents', function( $table ) {
            $table->dropColumn('level_id');
        });

        Schema::rename('content_levels', 'topic_levels');
    }
}
