<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrugDrugFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drug_drug_forms', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->integer('drug_id')->unsigned();
            $table->integer('drug_form_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('drug_drug_forms');
    }
}
