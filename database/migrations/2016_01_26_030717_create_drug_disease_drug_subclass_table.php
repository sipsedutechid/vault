<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrugDiseaseDrugSubclassTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drug_disease_drug_subclass', function (Blueprint $table)
        {
            $table->integer('drug_disease_id')->unsigned();
            $table->integer('drug_subclass_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('drug_disease_drug_subclass');
    }
}
