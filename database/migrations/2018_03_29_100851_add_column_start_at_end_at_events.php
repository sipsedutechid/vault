<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStartAtEndAtEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function ($table) {
            $table->date('start_at')->after('lat')->nullable();
            $table->date('end_at')->after('start_at')->nullable();
        });
    }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
    public function down()
    {
        Schema::table('events', function ($table) {
            $table->dropColumn('start_at');
            $table->dropColumn('end_at');
        });
    }
}
