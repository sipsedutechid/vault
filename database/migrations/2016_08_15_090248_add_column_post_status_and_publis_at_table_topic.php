<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPostStatusAndPublisAtTableTopic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topics', function( $table ) {
            $table->integer('post_status')->after('category');
            $table->timestamp('publish_at')->after('post_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topics', function( $table ) {
            $table->dropColumn('post_status');
            $table->dropColumn('publish_at');
        });
    }
}
