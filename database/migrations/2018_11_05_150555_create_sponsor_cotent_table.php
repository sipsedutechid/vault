<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSponsorCotentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sponsor_content', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sponsor_id');
            $table->integer('content_id');
            $table->string('content_type');
            $table->date('start_at')->nullable();
            $table->date('end_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::drop('sponsor_content');
    }
}
