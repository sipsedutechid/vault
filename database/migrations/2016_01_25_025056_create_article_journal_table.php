<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleJournalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_journal', function (Blueprint $table)
        {
            $table->integer('article_id')->unsigned();
            $table->integer('journal_id')->unsigned();
            $table->integer('journal_volume_id')->unsigned()->nullable();
            $table->integer('journal_volume_issue_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('article_journal');
    }
}
