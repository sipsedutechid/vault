<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditColumnFromTableJournalTags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('journal_tags', function($table) {
            $table->dropColumn('tag');
            $table->integer('tag_id');
        });

        Schema::rename('journal_tags', 'journal_tag');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('journal_tag', function($table) {
            $table->dropColumn('tag_id');
            $table->string('tag')->before('journal_id');
        });

        Schema::rename('journal_tag', 'journal_tags');
    }
}
