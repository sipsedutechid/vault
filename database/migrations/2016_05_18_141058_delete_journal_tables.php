<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteJournalTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('journal_journal_category');
        Schema::drop('journal_volumes');
        Schema::drop('journal_volume_issues');
        Schema::drop('articles');
        Schema::drop('article_journal');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
