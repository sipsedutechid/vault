<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDrugPackagingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drug_packaging', function (Blueprint $table)
        {
            $table->dropColumn('drug_drug_form_id');
            $table->integer('drug_form_id')->unsigned();
            $table->integer('drug_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drug_packaging', function (Blueprint $table)
        {
            $table->integer('drug_drug_form_id')->unsigned();
            $table->dropColumn('drug_form_id');
            $table->dropColumn('drug_id');
        });
    }
}
