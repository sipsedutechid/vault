<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnP2kbUrlTopicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topics', function($table){
            $table->string('p2kb_url')
                ->nullable()
                ->after('is_p2kb');
        });
    }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
    public function down()
    {
        Schema::table('topics', function($table){
            $table->dropColumn('p2kb_url');
        });
    }
}
