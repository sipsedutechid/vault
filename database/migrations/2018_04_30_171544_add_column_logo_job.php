<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLogoJob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::table('jobs', function ($table) {
             $table->string('logo')->after('lat')->nullable();
         });
     }

     /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
         Schema::table('jobs', function ($table) {
             $table->dropColumn('logo');
         });
     }
}
