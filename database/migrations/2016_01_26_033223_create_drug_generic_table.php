<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrugGenericTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drug_generic', function (Blueprint $table)
        {
            $table->integer('drug_id')->unsigned();
            $table->integer('generic_id')->unsigned();
            $table->integer('quantity')->unsigned();
            $table->string('unit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('drug_generic');
    }
}
