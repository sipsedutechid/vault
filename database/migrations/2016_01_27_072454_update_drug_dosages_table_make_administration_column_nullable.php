<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDrugDosagesTableMakeAdministrationColumnNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drug_dosages', function (Blueprint $table)
        {
            $table->string('administration')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('drug_dosages', function (Blueprint $table)
        {
            $table->dropColumn('administration');
        });
    }
}
