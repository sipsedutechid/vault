<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDrugGenericTableAddDrugFormColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drug_generic', function (Blueprint $table)
        {
            $table->integer('drug_form_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drug_generic', function (Blueprint $table)
        {
            $table->dropColumn('drug_form_id');
        });
    }
}
