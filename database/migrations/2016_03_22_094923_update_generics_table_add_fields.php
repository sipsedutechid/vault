<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGenericsTableAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('generics', function (Blueprint $table)
        {
            $table->string('alias')->nullable();
            $table->boolean('is_generic')->default(false);
            $table->integer('parent_id')->unsigned()->nullable();
            $table->text('description');
            $table->text('onset');
            $table->text('duration');
            $table->text('absorption');
            $table->text('distribution');
            $table->text('metabolism');
            $table->text('excretion');
            $table->integer('drug_class_id')->unsigned()->nullable();
            $table->integer('drug_subclass_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('generics', function (Blueprint $table)
        {
            $table->dropColumn('alias');
            $table->dropColumn('is_generic');
            $table->dropColumn('parent_id');
            $table->dropColumn('description');
            $table->dropColumn('onset');
            $table->dropColumn('duration');
            $table->dropColumn('absorption');
            $table->dropColumn('distribution');
            $table->dropColumn('metabolism');
            $table->dropColumn('excretion');
            $table->dropColumn('drug_class_id');
            $table->dropColumn('drug_subclass_id');
        });
    }
}
