<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorAcademicTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_academic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('doctor_id')->unique();
            $table->string('school_college');
            $table->string('year');
            $table->string('address');
            $table->string('level_academic_id');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doctor_academic');
    }
}
