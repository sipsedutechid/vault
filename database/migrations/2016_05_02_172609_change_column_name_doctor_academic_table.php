<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnNameDoctorAcademicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doctor_academic', function ($table) {
            $table->renameColumn('level_academic_id', 'academic_level_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doctor_academic', function ($table) {
            $table->renameColumn('academic_level_id', 'level_academic_id');
        });
    }
}
