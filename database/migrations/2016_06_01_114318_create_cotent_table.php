<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCotentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function(Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('desc')->nullable();
            $table->text('data')->nullable();
            $table->enum('type', ['text', 'embed', 'link', 'file'])->nullable();
            $table->boolean('is_downloadable')->default(false);

            $table->integer('created_by')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contents');
    }
}
