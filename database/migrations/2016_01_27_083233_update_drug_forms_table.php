<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDrugFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drug_forms', function (Blueprint $table)
        {
            $table->string('slug')->unique();
            $table->dropColumn('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drug_forms', function (Blueprint $table)
        {
            $table->integer('code')->unique();
            $table->dropColumn('slug');
        });
    }
}
