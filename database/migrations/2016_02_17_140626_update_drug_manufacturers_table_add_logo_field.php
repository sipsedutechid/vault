<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDrugManufacturersTableAddLogoField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drug_manufacturers', function (Blueprint $table)
        {
            $table->string('logo_image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drug_manufacturers', function (Blueprint $table)
        {
            $table->dropColumn('logo_image');
        });
    }
}
