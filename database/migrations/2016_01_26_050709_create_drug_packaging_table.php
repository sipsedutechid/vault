<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrugPackagingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drug_packaging', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('drug_drug_form_id')->unsigned();
            $table->string('name');
            $table->bigInteger('price')->nullable();
            $table->integer('quantity')->unsigned();
            $table->integer('quantity_2')->unsigned()->nullable();
            $table->integer('quantity_3')->unsigned()->nullable();
            $table->integer('created_by')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('drug_packaging');
    }
}
