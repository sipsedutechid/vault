<?php

use Illuminate\Database\Seeder;

class DiseasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('diseases')->insert([
            ['name' => 'Cardiologi'] ,
            ['name' => 'Dermatologi']
        ]);
    }
}
