<?php

use Illuminate\Database\Seeder;

use App\Ability;

class AbilityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersAbility = Ability::create(['name' => 'users']);
        $usersAbility->children()->saveMany([
        	Ability::create(['name' => 'users.add']),
        	Ability::create(['name' => 'users.edit']),
        	Ability::create(['name' => 'users.trash']),
        	]);
        $drugsAbility = Ability::create(['name' => 'drugs']);

        $drugsGenericsAbility = Ability::create(['name' => 'drugs.generics']);
        $drugsGenericsAbility->children()->saveMany([
    		Ability::create(['name'	=> 'drugs.generics.edit']),
    		]);
        $drugsManufacturersAbility = Ability::create(['name' => 'drugs.manufacturers']);
        $drugsManufacturersAbility->children()->saveMany([
    		Ability::create(['name'	=> 'drugs.manufacturers.edit']),
    		Ability::create(['name'	=> 'drugs.manufacturers.trash']),
    		]);
        $drugsAbility->children()->saveMany([
        	Ability::create(['name' => 'drugs.add']),
        	Ability::create(['name' => 'drugs.edit']),
        	Ability::create(['name' => 'drugs.trash']),
        	Ability::create(['name' => 'drugs.classes']),
        	$drugsGenericsAbility,
        	$drugsManufacturersAbility,
        	]);
    }
}
