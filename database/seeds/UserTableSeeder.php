<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'login' => 'endy.hardy',
        	'password' => bcrypt('12345678'),
        	'name' => 'Endy Hardy'
        	]);
    }
}
