<?php

use Illuminate\Database\Seeder;
use App\Ability;
use App\Role;
use App\User;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::create(['slug' => 'admin', 'name' => 'App Administrator']);
        $adminRole->users()->saveMany(User::all());
        $adminRole->abilities()->sync(Ability::all());
    }
}
