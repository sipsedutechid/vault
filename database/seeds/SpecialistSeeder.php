<?php

use Illuminate\Database\Seeder;
use App\Specialist;

class SpecialistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Specialist::create([
        	'name' => 'Umum'
            ]);
        Specialist::create([
        	'name' => 'Dokter Gigi'
            ]);
    }
}
