<?php

use Illuminate\Database\Seeder;

class TopicLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('topic_levels')->insert([
            ['name' => 'General'] ,
            ['name' => 'Advance'] ,
            ['name' => 'Speciality']
        ]);
    }
}
