<?php

use App\JournalCategory;
use Illuminate\Database\Seeder;

class JournalCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JournalCategory::create([
        	'slug' => 'default',
        	'name' => 'Default',
        	'description' => 'Default category for journals',
        	'is_active' => true,
        	'created_by' => 1
        	]);
    }
}
