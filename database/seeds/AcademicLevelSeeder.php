<?php

use Illuminate\Database\Seeder;
use App\AcademicLevel;

class AcademicLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AcademicLevel::create([
        	'name' => 'SD'
        	]);
    }
}
