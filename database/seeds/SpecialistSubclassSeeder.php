<?php

use Illuminate\Database\Seeder;
use App\SpecialistSubclass;

class SpecialistSubclassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SpecialistSubclass::create([
            'id'               => 1 ,
        	'name'             => 'Dokter' ,
            'specialists_id'   => '1'
        ]);
        SpecialistSubclass::create([
            'id'               => 2 ,
        	'name'             => 'Dokter Gigi' ,
            'specialists_id'   => '2'
        ]);
    }
}
