<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopicDisease extends Model
{
    protected $table    = 'topic_disease';
    protected $fillable = ['topic_id' , 'disease_id'];
    public $timestamps  = false;
}
