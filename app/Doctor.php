<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $with = ['academic', 'clinic'];

    public function academic()
    {
       return $this->hasMany('App\DoctorAcademic');
    }

    public function clinic()
    {
        return $this->hasMany('App\Clinic');
    }

    public function getCreatedAtAttribute($value)
    {
        return date('Ymd', strtotime($value)) == date('Ymd') ? date('H:i', strtotime($value)) : date('j M y', strtotime($value));
    }
}
