<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinic extends Model
{
    protected $fillable = ['doctor_id', 'name', 'address', 'city', 'telp'];
}
