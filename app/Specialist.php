<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialist extends Model
{
    public function subclass()
    {
    	return $this->hasMany('App\SpecialistSubclass');
    }
}
