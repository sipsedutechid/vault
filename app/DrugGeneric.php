<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrugGeneric extends Model
{
    protected $table = 'drug_generic';

    public function drugForm()
    {
    	return $this->belongsTo('App\DrugForm');
    }

    public function drug()
    {
    	return $this->belongsTo('App\Drug');
    }
    public function generic()
    {
    	return $this->belongsTo('App\Generic')->select(['id', 'slug', 'name']);
    }
}
