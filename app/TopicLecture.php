<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopicLecture extends Model
{
    //
    protected $table    = 'topic_lecture';
    protected $fillable = ['topic_id' , 'lecture_id'];
    public $timestamps  = false;
}
