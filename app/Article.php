<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public function journal()
	{
        return $this->belongsTo('App\Journal', 'journal_id');
    }
}
