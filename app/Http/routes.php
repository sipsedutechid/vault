<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('landing');
});

// Public resources
Route::get('/backend/drug/manufacturer/logo/{slug}', 'Backend\Drug\ManufacturerController@logo');
Route::get('/drug/manufacturer/logo/{slug}', 'Backend\Drug\ManufacturerController@logo');
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['prefix' => 'api' , 'middleware' => ['cors']], function() {
    Route::get('/content/list', 'Api\ContentController@getFree');
    Route::get('/content/{slug}', 'Api\ContentController@getSingle');
    Route::get('/{disease}/topic/list', 'Api\TopicController@getByDisease');
    Route::get('/disease/list', 'Api\DiseaseController@getList');
    Route::get('/disease/list/{slug}', 'Api\DiseaseController@getList');
    Route::get('/drugs/search', 'Api\DrugController@search');
    Route::get('/drugs/list/{option}', 'Api\DrugController@getList');
    Route::get('/drugs/classes/short', 'Api\DrugController@getClassShort');
    Route::get('/drug/{slug}', 'Api\DrugController@getSingle');
    Route::get('/topic/latest', 'Api\TopicController@getLatest');
    Route::get('/topic/{slug}', 'Api\TopicController@getSingle');
    Route::get('/journal/latest/{limit}', 'Api\JournalController@getLatest');
    Route::get('/journal/article/{slug}', 'Api\JournalController@getArticle');
    Route::get('/journal/list', 'Api\JournalController@getList');
    Route::get('/journal/category', 'Api\JournalController@getCategory');
    Route::get('/journal/{slug}', 'Api\JournalController@getSingle');

    Route::post('/journal/list', 'Api\JournalController@getList');
    Route::post('/content/list', 'Api\ContentController@getFree');

    Route::post('/fcm/save', 'Api\FcmController@save');
});

Route::group(['middleware' => ['web']], function () {

	Route::get('/drugs', 'Frontend\DrugController@home');

    Route::get('lecture/photo/{filename}', function ($filename)
    {
        $path = storage_path('app/lecture_photo/' . $filename);

        if(!File::exists($path)) abort(404);

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    });

    Route::get('journal/files/{loc}/{filename}', function ($loc , $filename)
    {
        $path = storage_path('app/journal_files/' . $loc . '/' . $filename);

        if(!File::exists($path)) abort(404);

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    });

    Route::get('topic/files/{filename}', function ($filename)
    {

        $path = storage_path('app/topic_files/image/' . $filename);

        if(!File::exists($path)) abort(404);

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Cache-Control", 'max-age=1209600, public');
        $response->header("Expires", \Carbon\Carbon::maxValue()->toRfc1123String());
        $response->header("Content-Type", $type);

        return $response;
    });

    Route::get('job/files/{filename}', function ($filename)
    {

        $path = storage_path('app/jobs/' . $filename);

        if(!File::exists($path)) abort(404);

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Cache-Control", 'max-age=1209600, public');
        $response->header("Expires", \Carbon\Carbon::maxValue()->toRfc1123String());
        $response->header("Content-Type", $type);

        return $response;
    });

    // Backend group
	Route::group(['prefix' => 'backend'], function () {

		// Authentication
		// Route::get('/sign-in', 'Auth\AuthController@showLoginForm');
		// Route::get('/sign-out', 'Auth\AuthController@logout');
		// Route::post('/sign-in', 'Auth\AuthController@postLogin');


		// Authenticated group
		Route::group(['middleware' => ['auth']], function () {

			// Dashboard
			Route::get('/', 'Backend\HomeController@dashboard');

			// Journals
            Route::get('/journal/categories', 'Backend\Journal\CategoryController@listAll');
            Route::post('/journal/category/all', 'Backend\Journal\CategoryController@listAll');
            Route::get('/journal/category/new', 'Backend\Journal\CategoryController@create');
            Route::post('/journal/category/new', 'Backend\Journal\CategoryController@postCreate');
            Route::get('/journal/category/{slug}/edit', 'Backend\Journal\CategoryController@edit');
            Route::post('/journal/category/{slug}/edit', 'Backend\Journal\CategoryController@postUpdate');
            Route::get('/journal/category/{slug}/trash', 'Backend\Journal\CategoryController@Trash');
            Route::post('/journal/category/trash', 'Backend\Journal\CategoryController@postTrash');

			Route::get('/journals', 'Backend\Journal\JournalController@home');
			Route::get('/journal/new', 'Backend\Journal\JournalController@create');
            Route::post('/journal/new', 'Backend\Journal\JournalController@postCreate');
            Route::get('/journal/all', 'Backend\Journal\JournalController@listAll');
            Route::post('/journal/all', 'Backend\Journal\JournalController@listAll');
            Route::get('/journal/{slug}/edit', 'Backend\Journal\JournalController@edit');
            Route::post('/journal/{slug}/edit', 'Backend\Journal\JournalController@postUpdate');
            Route::get('/journal/{slug}/trash', 'Backend\Journal\JournalController@Trash');
            Route::post('/journal/trash', 'Backend\Journal\JournalController@postTrash');

			// Drugs
			Route::get('/drugs', 'Backend\Drug\DrugController@home');
			Route::get('/drug/all', 'Backend\Drug\DrugController@listAll');
			Route::get('/drug/new', 'Backend\Drug\DrugController@create');
			Route::get('/drug/classes', 'Backend\Drug\ClassController@home');
			Route::get('/drug/diseases', 'Backend\Drug\DiseaseController@home');
			Route::get('/drug/disease/trash', 'Backend\Drug\DiseaseController@trash');
			Route::get('/drug/disease/{slug}', 'Backend\Drug\DiseaseController@single');
			Route::get('/drug/disease/{slug}/restore', 'Backend\Drug\DiseaseController@restore');
			Route::get('/drug/manufacturers', 'Backend\Drug\ManufacturerController@home');
			Route::get('/drug/manufacturer/{slug}/edit', 'Backend\Drug\ManufacturerController@edit');
			Route::get('/drug/{slug}/edit', 'Backend\Drug\DrugController@edit');
			Route::get('/drug/{slug}/trash', 'Backend\Drug\DrugController@trash');
			Route::post('/drug/new', 'Backend\Drug\DrugController@postCreate');
			Route::post('/drug/{slug}/edit', 'Backend\Drug\DrugController@postEdit');
			Route::post('/drug/trash', 'Backend\Drug\DrugController@postTrash');
			Route::post('/drug/disease/{slug?}', 'Backend\Drug\DiseaseController@postSingle');
			Route::post('/drug/manufacturer/{slug}/edit', 'Backend\Drug\ManufacturerController@postEdit');
			Route::delete('/drug/disease/trash', 'Backend\Drug\DiseaseController@obliterate');
			Route::delete('/drug/disease/{slug}', 'Backend\Drug\DiseaseController@delete');
			Route::delete('/drug/manufacturer/{slug}/trash', 'Backend\Drug\ManufacturerController@trash');

			Route::get('/drug/generics', 'Backend\Drug\GenericController@home');
			Route::get('/drug/generic/new', 'Backend\Drug\GenericController@getCreate');
			Route::get('/drug/generic/{slug}/edit', 'Backend\Drug\GenericController@getEdit');
			Route::post('/drug/generic/{slug}/edit', 'Backend\Drug\GenericController@postEdit');
			Route::post('/drug/generic/{slug}/merge', 'Backend\Drug\GenericController@postMerge');

			// Pending
			Route::get('/articles', function () { return view('backend.pending', ['menu' => null]); });
			Route::get('/hospitals', function () { return view('backend.pending', ['menu' => null]); });

			// User management
			// Route::get('/users', 'Backend\User\UserController@home');						// Users dashboard
			// Route::get('/user/all', 'Backend\User\UserController@listAll');					// Users list
			// Route::get('/user/new', 'Backend\User\UserController@create');					// Create new user
			// Route::get('/user/{login}/edit', 'Backend\User\UserController@edit');			// Edit existing user
			// Route::get('/user/roles', 'Backend\User\RoleController@listAll');				// User roles list
			// Route::get('/user/role/{slug}/edit', 'Backend\User\RoleController@edit');		// Edit existing user role
			// Route::post('/user/new', 'Backend\User\UserController@postCreate');				// Post create user action
			// Route::post('/user/role/{slug}/edit', 'Backend\User\RoleController@postEdit');	// Post edit user action

            // Doctors
            Route::get('/doctors', 'Backend\Doctor\DoctorController@home');
            Route::get('/doctor/new', 'Backend\Doctor\DoctorController@create');
            Route::post('/doctor/new', 'Backend\Doctor\DoctorController@postCreate');
            Route::get('/doctor/{slug}/edit', 'Backend\Doctor\DoctorController@edit');

            //Topic
            Route::get('/topics', 'Backend\Topic\TopicController@home');
            Route::get('/topic/all', 'Backend\Topic\TopicController@listAll');
            Route::post('/topic/all', 'Backend\Topic\TopicController@listAll');
            Route::get('/topic/new', 'Backend\Topic\TopicController@create');
            Route::post('/topic/new', 'Backend\Topic\TopicController@postCreate');
            Route::get('/topic/{slug}/edit', 'Backend\Topic\TopicController@edit');
            Route::post('/topic/{slug}/edit', 'Backend\Topic\TopicController@postUpdate');
            Route::get('/topic/{slug}/trash', 'Backend\Topic\TopicController@Trash');
            Route::post('/topic/trash', 'Backend\Topic\TopicController@postTrash');

            Route::get('/topic/contents', 'Backend\Topic\ContentController@listAll');
            Route::get('/topic/content/all', 'Backend\Topic\ContentController@listAll');
            Route::post('/topic/content/all', 'Backend\Topic\ContentController@listAll');
            Route::get('/topic/content/new', 'Backend\Topic\ContentController@create');
            Route::post('/topic/content/new', 'Backend\Topic\ContentController@postCreate');
            Route::get('/topic/content/{slug}/edit', 'Backend\Topic\ContentController@edit');
            Route::post('/topic/content/{slug}/edit', 'Backend\Topic\ContentController@postUpdate');
            Route::get('/topic/content/{slug}/trash', 'Backend\Topic\ContentController@Trash');
            Route::post('/topic/content/trash', 'Backend\Topic\ContentController@postTrash');

            Route::get('/disease/all', 'Backend\Disease\DiseaseController@listAll');
            Route::get('/disease/new', 'Backend\Disease\DiseaseController@create');
            Route::post('/disease/new', 'Backend\Disease\DiseaseController@postCreate');
            Route::get('/disease/{slug}/edit', 'Backend\Disease\DiseaseController@edit');
            Route::post('/disease/{slug}/edit', 'Backend\Disease\DiseaseController@postUpdate');
            Route::get('/disease/{slug}/trash', 'Backend\Disease\DiseaseController@Trash');
            Route::post('/disease/trash', 'Backend\Disease\DiseaseController@postTrash');


            // events
            Route::get('/events', 'Backend\Event\EventController@home');
			Route::get('/event/new', 'Backend\Event\EventController@create');
            Route::post('/event/new', 'Backend\Event\EventController@postCreate');
            Route::get('/event/all', 'Backend\Event\EventController@listAll');
            Route::get('/event/{slug}/edit', 'Backend\Event\EventController@edit');
            Route::post('/event/{slug}/edit', 'Backend\Event\EventController@postUpdate');
            Route::get('/event/{slug}/trash', 'Backend\Event\EventController@Trash');
            Route::post('/event/trash', 'Backend\Event\EventController@postTrash');

            //jobs
            Route::get('/jobs', 'Backend\Job\JobController@home');
			Route::get('/job/new', 'Backend\Job\JobController@create');
            Route::post('/job/new', 'Backend\Job\JobController@postCreate');
            Route::get('/job/all', 'Backend\Job\JobController@listAll');
            Route::get('/job/{slug}/edit', 'Backend\Job\JobController@edit');
            Route::post('/job/{slug}/edit', 'Backend\Job\JobController@postUpdate');
            Route::get('/job/{slug}/trash', 'Backend\Job\JobController@Trash');
            Route::post('/job/trash', 'Backend\Job\JobController@postTrash');
		});
	});

	// AJAX services
	Route::group(['prefix' => 'service'], function () {
		Route::get('/slugger', function () { echo str_slug(Request::input('string')); });
		Route::get('/list/generics', function () { echo Request::input('term') ? App\Generic::orderBy('name', 'asc')->where('name', 'like', '%' . Request::input('term') . '%')->get()->toJSON() : App\Generic::orderBy('name', 'asc')->get()->toJSON(); });
		Route::get('/list/drugs', function () { echo Request::input('term') ? App\Drug::orderBy('name', 'asc')->where('name', 'like', '%' . Request::input('term') . '%')->get()->toJSON() : App\Drug::orderBy('name', 'asc')->get()->toJSON(); });
		Route::get('/list/drug-forms', function () { echo Request::input('term') ? App\DrugForm::orderBy('name', 'asc')->where('name', 'like', '%' . Request::input('term') . '%')->get()->toJSON() : App\DrugForm::orderBy('name', 'asc')->get()->toJSON(); });
		Route::get('/list/drug-manufacturers', function () { echo Request::input('term') ? App\DrugManufacturer::orderBy('name', 'asc')->where('name', 'like', '%' . Request::input('term') . '%')->get()->toJSON() : App\DrugManufacturer::orderBy('name', 'asc')->get()->toJSON(); });
		Route::get('/list/journal-categories', function () { echo Request::input('term') ? App\JournalCategory::orderBy('name', 'asc')->where('name', 'like', '%' . Request::input('term') . '%')->get()->toJSON() : App\JournalCategory::orderBy('name', 'asc')->get()->toJSON(); });
		Route::get('/list/drug-subclasses', function () {
			if (Request::input('class_id'))
				$echo = App\DrugSubclass::where('drug_class_id', Request::input('class_id'))->orderBy('name', 'asc')->get()->toJSON();
			if (Request::input('term'))
				$echo = App\DrugSubclass::where('name', 'like', '%' . Request::input('term') . '%')->orderBy('name', 'asc')->get()->toJSON();
			echo isset($echo) ? $echo : '[]';
		});

        Route::get('/list/specialist-subclasses', function () {
			$echo = App\SpecialistSubclass::where('specialists_id', Request::input('specialist_id'))
               ->orderBy('name', 'asc')
               ->get()
               ->toJSON();
			echo isset($echo) ? $echo : '[]';
		});

        Route::post('/content/add', 'Backend\Content\ContentController@save');
        Route::post('/content/search', 'Backend\Content\ContentController@search');
        Route::post('/content/view', 'Backend\Content\ContentController@view');

        Route::post('/lecture/add', 'Backend\Lecture\LectureController@save');
        Route::post('/lecture/search', 'Backend\Lecture\LectureController@search');

        Route::post('/journal/article/add', 'Backend\Article\ArticleController@save');
	});

});
