<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'service/content/add' ,
        'service/content/search' ,
        'backend/topic/all' ,
        'backend/journal/all' ,
        'service/content/view' ,
        'service/lecture/add' ,
        'service/lecture/search' ,
        'service/journal/article/add' ,
        'backend/topic/content/all' ,
        'backend/topic/content/list'
    ];
}
