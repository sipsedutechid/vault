<?php

namespace App\Http\Middleware;

use Closure;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->input('lang'))
            $request->session()->put('locale', $request->input('lang'));
        app()->setLocale($request->session()->get('locale', 'en'));

        return $next($request);
    }
}
