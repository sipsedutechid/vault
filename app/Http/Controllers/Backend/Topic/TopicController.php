<?php

namespace App\Http\Controllers\Backend\Topic;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Topic;
use App\Disease;
use App\Lecture;
use App\TopicLecture;
use App\TopicDisease;
use App\TopicContent;
use App\Content;
use App\Dosease;
use App\TopicFaq;
use App\ContentController;
use App\ContentLevel;
use App\TopicTag;
use App\Tag;

use Validator;
use File;
use Storage;

class TopicController extends Controller
{
    public function home() {
        return view('backend.topics.home', [
    		'menu' 				=> 'topics' ,
            'topic_recents'     => Topic::orderBy('created_at', 'desc')->take(5)->get() ,
            'topic_count'       => Topic::count() ,
            'content_count'     => Content::count() ,
            'lecture_count'     => Lecture::count() ,
            'disease_count'     => Disease::count()
        ]);
    }

    public function listAll(Request $request) {
        if ($request->ajax()) {

            $topic = Topic::where( 'id' , '<', $request->last_id )
                    ->orderBy('id' , 'desc')
                    ->take(10)
                    ->get();

            $last_topic = Topic::orderBy('id' , 'asc')
                    ->firstOrFail();

            $view = view('backend.topics.list-table', ['topics' => $topic]);

            $total_topic = count($topic);
            //($lsat_id = 0) telling the client side to hide the load more button
            //if row selected is the last row in db
            if ( $total_topic > 0 && $last_topic->id === $topic[ $total_topic - 1 ]->id )
                $last_id = 0 ;
            else
                $last_id = $topic[ $total_topic - 1 ]->id;

            return json_encode([
                'last_id' => $last_id ,
                'html'    => $view->render()
            ]);

        } elseif ( $request->s ) {
            $s = explode( ' ' , $request->s );

            $topic = Topic::where( function ($query) use ($s){
                        $query->where(function($query) use ($s) {
                            foreach ($s as $key) {
                                $query->orWhere('title' , 'LIKE' , '%' . $key . '%');
                            }
                        });
                    })
                    ->orderBy('id', 'desc')
                    ->get();

            $hide = true;
        } else {
            $topic = Topic::orderBy('id', 'desc')
                   ->take(10)
                   ->get();

            $hide = Topic::count() <= 10;
        }

        return view('backend.topics.list', [
            'menu'      => 'topics' ,
            'topics'    => $topic ,
            'hide'      => $hide
        ]);
    }

    public function create(Request $request){
        $category = ['doctor' => 'General Doctor', 'dentist' => 'Dentist'];
        $contentLevels    = ContentLevel::all();

        return view('backend.topics.form', [
    		'menu'              => 'topics' ,
            'topic'             => new Topic() ,
            'diseases'          => Disease::all() ,
            'category'          => $category ,
            'content_levels'    => $contentLevels ,
            'request'           => $request
    		]);
    }

    public function postCreate(Request $request) {
        $this->validate($request, [
			"title" => "required|string|max:255"
    	]);

        $topic_slug = str_slug( $request->title );

        $i = 1;
        while ( ( Topic::where( 'slug' , $topic_slug )->count() ) > 0 ) {
            $topic_slug = str_slug( $request->title . $i );

            $i++;
        }

        $topic = new Topic;

        $topic->title               = $request->title;
        $topic->summary             = $request->summary;
        $topic->meta_desc           = $request->meta_desc;
        $topic->preview             = $request->preview;
        $topic->slug                = $topic_slug;
        $topic->created_by          = auth()->user()->id;
        $topic->is_open_access      = $request->is_open_access ?: 0;
        $topic->is_p2kb             = $request->is_p2kb ?: 0;
        $topic->is_free_certificate = $request->is_free_certificate ?: 0;
        $topic->p2kb_url            = !empty($request->p2kb_url) ? $request->p2kb_url : null;
        $topic->release             = $request->release;
        $topic->category            = $request->category;
        $topic->post_status         = $request->post_status ?: 0;

        if ($topic->post_status === '1') {
            $topic->publish_at  = date('Y-m-d G:i:s');
        }

        if ($request->file('featured_image')) {
            $file  = ['featured_image' => $request->file('featured_image')];
            $rules = ['mimes' => 'jpeg,bmp,png,jpg'];

            $validator = Validator::make($file, $rules);
            if (!$validator->fails()) {

                if ($request->file('featured_image')->isValid()) {
                    $extension          = $request->file('featured_image')->getClientOriginalExtension();
                    $fileName           = $topic_slug.'_image.'.$extension;

                    $request->file('featured_image')->move(base_path() . '/storage/app/topic_files/image/', $fileName);
                    $topic->featured_image = $fileName;
                }
            }
        }

        $topic->save();

        $tags = explode( "," , $request->tags);

        foreach ($tags as $tag) {

            $tag_data = Tag::firstOrCreate([
                'name' => $tag ,
                'slug' => str_slug( $tag )
            ]);

            TopicTag::create([
                'topic_id'  => $tag_data->id ,
                'tag_id'    => $topic->id
            ]);
        }

        if ($request->input('lecture.*')) {
            foreach ($request->input('lecture.*') as $data_lecture) {
                $topicLecture = TopicLecture::create([
                    'topic_id'  => $topic->id ,
                    'lecture_id'=> $data_lecture['id']
                ]);
            }
        }

        if ($request->input('disease.*')) {
            foreach ($request->input('disease.*') as $data_disease) {
                $topicDisease = TopicDisease::create([
                    'topic_id'      => $topic->id ,
                    'disease_id'    => $data_disease['id']
                ]);
            }
        }

        if ($request->input('contents.*')) {
            foreach ($request->input('contents.*') as $data_content) {
                $topicDisease = TopicContent::create([
                    'topic_id'      => $topic->id ,
                    'content_id'    => $data_content['id']
                ]);
            }
        }

        if ($request->input('faqs.*')) {
            foreach ($request->input('faqs.*') as $data_faq) {
                $topicFaq = TopicFaq::create([
                    'topic_id'   => $topic->id ,
                    'question'    => $data_faq['question'] ,
                    'answer'     => $data_faq['answer']
                ]);
            }
        }

        $request->session()->flash('msg-success', trans('alerts.topic_saved'));

        return redirect('/backend/topic/' . $request->post_action);
    }

    public function edit(Request $request) {
        $category       = ['doctor' => 'General Doctor', 'dentist' => 'Dentist'];
        $topic          = Topic::where('slug', $request->slug)->with(['tags'])->first();
        $contentLevels  = ContentLevel::all();
        $tags           = '';

        foreach ($topic->tags as $tag) {
            $tags .= (empty($tags) ? '' : ',') . $tag->name;
        }

        return view('backend.topics.form', [
    		'menu'           => 'topics' ,
            'topic'          => $topic ,
            'content_levels' => $contentLevels ,
            'diseases'       => Disease::all() ,
            'category'       => $category ,
            'request'        => $request ,
            'tags'           => $tags
    		]);
    }

    public function postUpdate(Request $request) {
        $topic = Topic::where( 'slug' , $request->slug )->firstOrFail();

        $this->validate($request, [
			"title" => "required|string|max:255"
    	]);

        $i = 1;

        if ( $topic->title != $request->title ) {
            $topic_slug = str_slug( $request->title );

            while ( ( Topic::where( 'slug' , $topic_slug )->count() ) > 0 ) {
                $topic_slug = str_slug( $request->title . $i );
                $i++;
            }

            $topic->slug = $topic_slug;
        }

        $topic->title               = $request->title;
        $topic->summary             = $request->summary;
        $topic->meta_desc           = $request->meta_desc;
        $topic->preview             = $request->preview;
        $topic->is_open_access      = $request->is_open_access ?: 0;
        $topic->is_p2kb             = $request->is_p2kb ?: 0;
        $topic->is_free_certificate = $request->is_free_certificate ?: 0;
        $topic->p2kb_url            = !empty($request->p2kb_url) ? $request->p2kb_url : null;
        $topic->release             = $request->release;
        $topic->category            = $request->category;

        if ($request->post_status === '1' && $topic->post_status != $request->post_status) {
            $topic->publish_at  = date('Y-m-d G:i:s');
        } elseif ($request->post_status == NULL) {
            $topic->publish_at = null;
        }

        $topic->post_status         = $request->post_status ?: 0;

        if ($request->file('featured_image')) {
            $file  = ['featured_image' => $request->file('featured_image')];
            $rules = ['mimes' => 'jpeg,bmp,png,jpg'];

            $validator = Validator::make($file, $rules);
            if (!$validator->fails()) {

                if ($request->file('featured_image')->isValid()) {
                    $extension          = $request->file('featured_image')->getClientOriginalExtension();
                    $fileName           = $topic->slug.'_image.'.$extension;

                    File::delete(base_path('storage/app/topic_files/image/' . $topic->featured_image));
                    $request->file('featured_image')->move(base_path() . '/storage/app/topic_files/image/', $fileName);
                    $topic->featured_image = $fileName;
                }
            }
        } elseif ($request->remove_pict == '1') {
            File::delete(base_path('storage/app/topic_files/image/' . $topic->featured_image));
            $topic->featured_image = '';
        }

        $topic->save();

        $tags = explode( "," , $request->tags);

        //topic sync
        $saved_tags = [];
        $data_tag = [];

        foreach ($tags as $tag) {

            $data_tag = Tag::firstOrCreate([
                'name' => $tag ,
                'slug' => str_slug( $tag )
            ]);

            TopicTag::firstOrCreate([
                'topic_id'  => $data_tag->id ,
                'tag_id'    => $topic->id
            ]);

            $saved_tags[] = $data_tag->id;
        }

        $topic->tags()->sync($saved_tags);

        $saved_lecture = [];
        if ($request->input('lecture.*')) {
            foreach ($request->input('lecture.*') as $data_lecture) {
                if ( $data_lecture['data_status'] == 'old' )
                    array_push( $saved_lecture , $data_lecture['id']);
            }
        }

        $topic->lectures()->sync( $saved_lecture );

        if ($request->input('lecture.*')) {

            foreach ($request->input('lecture.*') as $data_lecture) {
                if ( $data_lecture['data_status'] == 'new'  ) {
                    $topicLecture = TopicLecture::create([
                        'topic_id'  => $topic->id ,
                        'lecture_id'=> $data_lecture['id']
                    ]);
                }
            }
        }

        $saved_disease = [];

        if ($request->input('disease.*')) {
            foreach ($request->input('disease.*') as $data_disease) {
                if ( $data_disease['data_status'] == 'old')
                    array_push( $saved_disease , $data_disease['id']);
            }
        }

        $topic->disease()->sync($saved_disease);

        if ($request->input('disease.*')) {
            foreach ($request->input('disease.*') as $data_disease) {
                if ( $data_disease['data_status'] == 'new' ) {
                    $topicDisease = TopicDisease::create([
                        'topic_id'      => $topic->id ,
                        'disease_id'    => $data_disease['id']
                    ]);
                }
            }
        }

        $saved_content = [];
        if ($request->input('contents.*')) {
            foreach ($request->input('contents.*') as $data_content) {
                if ( $data_content['data_status'] == 'old')
                    array_push( $saved_content , $data_content['id']);
            }
        }

        $topic->content()->sync($saved_content);

        $saved_faq = [];
        if ($request->input('faqs.*')) {
            foreach ($request->input('faqs.*') as $data_faq) {
                if ( $data_faq['data_status'] == 'old')
                    array_push( $saved_content , $data_faq['id']);
            }
        }

        TopicFaq::where('topic_id', $topic->id)->whereNotIn('id', $saved_faq)->delete();

        if ($request->input('contents.*')) {
            foreach ($request->input('contents.*') as $data_content) {
                if ( $data_content['data_status'] == 'new' ) {
                    $topicDisease = TopicContent::create([
                        'topic_id'      => $topic->id ,
                        'content_id'    => $data_content['id']
                    ]);
                }
            }
        }

        if ($request->input('faqs.*')) {
            foreach ($request->input('faqs.*') as $data_faq) {
                $topicFaq = TopicFaq::create([
                    'topic_id'  => $topic->id ,
                    'question'  => $data_faq['question'] ,
                    'answer'    => $data_faq['answer']
                ]);
            }
        }

        $request->session()->flash('msg-success', trans('alerts.topic_updated'));

        if ( $request->post_action == 'edit') {
            return redirect('/backend/topic/' . $topic->slug . '/edit');
        } else {
            if ( $request->redirect )
                return redirect( $request->redirect );
            else
                return redirect('/backend/topic/' . $request->post_action);
        }
    }

    public function trash($slug) {
    	return view('backend.topics.trash', [
    		'menu' => 'topics',
    		'topic' => Topic::where('slug', $slug)->firstOrFail()
    		]);
    }

    public function postTrash(Request $request) {
        $topic = Topic::where('slug', $request->slug)->firstOrFail();

        Topic::where('slug', $request->slug)->delete();
        TopicContent::where('topic_id', $topic->id)->delete();
        TopicDisease::where('topic_id', $topic->id)->delete();
        TopicLecture::where('topic_id', $topic->id)->delete();
        TopicFaq::where('topic_id', $topic->id)->delete();

    	$request->session()->flash('msg-success', trans('alerts.topic_trash', ['title' => $topic->title]));
    	return redirect('backend/topic/all');
    }

}
