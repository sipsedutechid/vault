<?php

namespace App\Http\Controllers\Backend\Topic;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Content;
use App\TopicContent;
use App\ContentLevel;

use Validator;
use File;
use Storage;

class ContentController extends Controller
{
    public function listAll(Request $request) {
        if ($request->ajax()) {

            $content = Content::where( 'id' , '<', $request->last_id )
                    ->orderBy('id' , 'desc')
                    ->take(10)
                    ->get();

            $last_content = Content::orderBy('id' , 'asc')
                    ->firstOrFail();

            $view = view('backend.topics.contents.list-table', ['contents' => $content]);

            $total_content = count($content);
            //($lsat_id = 0) telling the client side to hide the load more button
            //if row selected is the last row in db
            if ( $total_content > 0 && $last_content->id === $content[ $total_content - 1 ]->id )
                $last_id = 0 ;
            else
                $last_id = $content[ $total_content - 1 ]->id;

            return json_encode([
                'last_id' => $last_id ,
                'html'    => $view->render()
            ]);

        } elseif ( $request->s ) {
            $s = explode( ' ' , $request->s );

            $content = Content::where( function ($query) use ($s){
                        $query->where(function($query) use ($s) {
                            foreach ($s as $key) {
                                $query->orWhere('title' , 'LIKE' , '%' . $key . '%');
                            }
                        });
                    })
                    ->orderBy('id', 'desc')
                    ->get();

            $hide = true;
        } else {
            $content = Content::orderBy('id', 'desc')
                   ->take(10)
                   ->get();

            $hide = Content::count() <= 10;
        }

        return view('backend.topics.contents.list', [
            'menu'      => 'topics' ,
            'contents'  => $content ,
            'hide'      => $hide
        ]);
    }

    public function create(Request $request) {
        $contentLevels    = ContentLevel::all();
        $contentLevelArr  = [];

        foreach ($contentLevels as $data) {
            $contentLevelArr[$data->id] = $data->name;
        }

        return view('backend.topics.contents.form', [
    		'menu'              => 'topics' ,
            'content'           => new Content ,
            'level_option'      => $contentLevelArr ,
            'request'           => $request ,
            'appearance_option' => [
                'text'          => 'Text' ,
                'image'         => 'Image' ,
                'video'         => 'Video' ,
                'audio'         => 'Audio' ,
                'quiz'          => 'Quiz' ,
                'download'      => 'Download' ,
                'video-mobile'  => 'Video Mobile'
            ]
    		]);
    }

    public function postCreate(Request $request) {
        $status         = true;
        $title          = $request->title;
        $content_slug   = str_slug($title);

        $i = 1;
        while ( ( Content::where( 'slug' , $content_slug )->count() ) > 0 ) {
            $content_slug = str_slug( $title . $i );

            $i++;
        }

        $content = new Content();

        $content->title      = $title;
        $content->label      = $request->label;
        $content->slug       = $content_slug;
        $content->type       = $request->type;
        $content->appearance = $request->appearance;
        $content->level_id   = $request->level_id;
        $content->is_free    = $request->is_free ?: 0;
        $content->is_media_data = $request->media_data ?: 0;
        $content->url_jw     = $request->url_jw;
        $content->order      = $request->order;

        switch ( $request->type ) {
            case 'text':
                $content->data  = $request->content_desc;
                break;

            case 'embed':
                $content->data  = $request->content_embed;
                break;

            case 'link':
                $content->data  = $request->content_link;
                break;

            case 'file':
                $file = array('content_file' => $request->file('content_file'));
                $rules = array('mimes' => 'jpeg,bmp,png,jpg, pdf' );

                $validator = Validator::make($file, $rules);
                if (!$validator->fails()) {

                    if ($request->file('content_file')->isValid()) {
                        $extension          = $request->file('content_file')->getClientOriginalExtension();
                        $fileName           = $content_slug.'.'.$extension;

                        $request->file('content_file')->move(base_path() . '/storage/app/content_files', $fileName);
                        $content->data = $fileName;
                    }
                }
                break;
        }

        try {
            $content->save();
        } catch(\Exception $e){
            $status = false;
        }

        if ($status) {
            $request->session()->flash('msg-success', trans('alerts.content_updated'));
        } else {
            $request->session()->flash('msg-failed', trans('alerts.content_fail_save'));
        }

        return redirect('/backend/topic/contents/');
    }

    public function edit(Request $request) {
        $content = Content::where('slug' , $request->slug)->first();
        $contentLevels    = ContentLevel::all();
        $contentLevelArr  = [];

        foreach ($contentLevels as $data) {
            $contentLevelArr[$data->id] = $data->name;
        }

        return view('backend.topics.contents.form', [
            'menu'              => 'topics' ,
    		'content'           => $content ,
            'level_option'      => $contentLevelArr ,
            'request'           => $request ,
            'appearance_option' => [
                'text'          => 'Text' ,
                'image'         => 'Image' ,
                'video'         => 'Video' ,
                'audio'         => 'Audio' ,
                'quiz'          => 'Quiz' ,
                'download'      => 'Download' ,
                'video-mobile'  => 'Video Mobile'
            ]
    	]);
    }

    public function postUpdate(Request $request) {
        $content = Content::where( 'slug' , $request->slug )->firstOrFail();

        $status         = true;
        $title          = $request->title;
        $content_slug   = str_slug($title);

        $i = 1;
        if ($content->title != $request->title) {
            while ( ( Content::where( 'slug' , $content_slug )->count() ) > 0 ) {
                $content_slug = str_slug( $title . $i );

                $i++;
            }

            $content->slug       = $content_slug;
        }

        $content->title      = $title;
        $content->label      = $request->label;
        $content->type       = $request->type;
        $content->appearance = $request->appearance;
        $content->level_id   = $request->level_id;
        $content->is_free    = $request->is_free ?: 0;
        $content->is_media_data = $request->media_data ?: 0;
        $content->url_jw     = $request->url_jw;
        $content->order      = $request->order;

        if ($content->delete_file_content == '1') {
            $content->data  = '';
        }

        switch ( $request->type ) {
            case 'text':
                $content->data  = $request->content_desc;
                break;

            case 'embed':
                $content->data  = $request->content_embed;
                break;

            case 'link':
                $content->data  = $request->content_link;
                break;

            case 'file':
                if ($request->file('content_file')) {
                    if (File::exists(base_path() . '/storage/app/content_files' . $content->data)) {
                        File::delete(base_path().'storage/app/content_files/' . $content->data);
                        $content->data = '';
                    }

                    $extension          = $request->file('content_file')->getClientOriginalExtension();
                    $fileName           = $content_slug.'.'.$extension;

                    $request->file('content_file')->move(base_path() . '/storage/app/content_files', $fileName);
                    $content->data = $fileName;

                } elseif ($request->delete_file_content == '1') {
                    if (File::exists(base_path() . '/storage/app/content_files' . $content->data)) {
                        File::delete(base_path().'storage/app/content_files/' . $content->data);
                        $content->data = '';
                    }
                }
                break;
        }

        try {
            $content->save();
        } catch(\Exception $e){
            $status = false;
        }

        if ($status) {
            $request->session()->flash('msg-success', trans('alerts.content_updated'));
        } else {
            $request->session()->flash('msg-failed', trans('alerts.content_fail_save'));
        }

        if ( $request->post_action == 'edit') {
            return redirect('/backend/topic/content/' . $content->slug . '/edit');
        } else {
            if ( $request->redirect )
                return redirect( $request->redirect );
            else
                return redirect('/backend/topic/contents/');
        }
    }

    public function trash($slug) {
    	return view('backend.topics.contents.trash', [
    		'menu' => 'topic',
    		'content' => Content::where('slug', $slug)->firstOrFail()
    		]);
    }

    public function postTrash(Request $request) {
        $content = Content::where('slug', $request->slug)->first();

        Content::where('slug', $request->slug)->first()->delete();

        if (File::exists(base_path() . '/storage/app/content_files' . $content->data && $content->type == 'file')) {
            Storage::delete('content_files/' . $content->data);
        }

        TopicContent::where('content_id' , $content->id)->delete();

        $request->session()->flash('msg-success', trans('alerts.content_trash', ['title' => $content->title]));
    	return redirect('backend/topic/contents');
    }
}
