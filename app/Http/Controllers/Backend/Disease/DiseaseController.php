<?php

namespace App\Http\Controllers\Backend\Disease;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Disease;

class DiseaseController extends Controller
{
    public function listAll(Request $request) {
        if ($request->ajax()) {

            $disease = Disease::where( 'id' , '<', $request->last_id )
                    ->orderBy('id' , 'desc')
                    ->take(10)
                    ->get();

            $last_disease = Disease::orderBy('id' , 'asc')
                    ->firstOrFail();

            $view = view('backend.disease.list-table', ['diseases' => $disease]);

            $total_disease = count($disease);
            //($lsat_id = 0) telling the client side to hide the load more button
            //if row selected is the last row in db
            if ( $total_disease > 0 && $last_disease->id === $disease[ $total_disease - 1 ]->id )
                $last_id = 0 ;
            else
                $last_id = $disease[ $total_disease - 1 ]->id;

            return json_encode([
                'last_id' => $last_id ,
                'html'    => $view->render()
            ]);

        } elseif ( $request->s ) {
            $s = explode( ' ' , $request->s );

            $disease = Disease::where( function ($query) use ($s){
                        $query->where(function($query) use ($s) {
                            foreach ($s as $key) {
                                $query->orWhere('name' , 'LIKE' , '%' . $key . '%');
                            }
                        });
                    })
                    ->orderBy('id', 'desc')
                    ->get();

            $hide = true;
        } else {
            $disease = Disease::orderBy('id', 'desc')
                   ->take(10)
                   ->get();

            $hide = Disease::count() <= 10;
        }

        return view('backend.disease.list', [
            'menu'      => 'topics' ,
            'diseases'    => $disease ,
            'hide'      => $hide
        ]);
    }

    public function create(Request $request) {
        $disease = Disease::all();
        $parents = ['null' => '-- no parent --'];

        foreach ($disease as $data) {
            $parents[$data->id] = $data->name;
        }

        return view('backend.disease.form', [
    		'menu'    => 'topics' ,
            'disease' => new Disease() ,
            'parents' => $parents ,
            'request' => $request
    		]);
    }

    public function postCreate(Request $request) {
        $this->validate($request, [
			"name" => "required|string|max:255"
    	]);

        $disease_slug = str_slug( $request->name );

        $i = 1;
        while ( ( Disease::where( 'slug' , $disease_slug )->count() ) > 0 ) {
            $disease_slug = str_slug( $request->name . $i );

            $i++;
        }

        $disease = new Disease;

        $disease->name                = $request->name;
        $disease->slug                = $disease_slug;

        if ($request->parent_id != null && $request->parent_id > 0) {
            $disease->parent_id = $request->parent_id;
        }

        $disease->save();

        $request->session()->flash('msg-success', trans('alerts.disease_save'));

        return redirect('/backend/disease/' . $request->post_action);
    }

    public function edit(Request $request) {
        $disease    = Disease::where('slug', $request->slug)->first();
        $diseases   = Disease::all();
        $parents    = ['null' => '-- no parent --'];

        foreach ($diseases as $data) {
            if ($data->id != $disease->id) {
                $parents[$data->id] = $data->name;
            }
        }

        return view('backend.disease.form', [
    		'menu'      => 'topics' ,
            'disease'   => $disease ,
            'diseases'  => $diseases ,
            'parents'   => $parents ,
            'request'   => $request
    		]);
    }

    public function postUpdate(Request $request) {
        $this->validate($request, [
			"name" => "required|string|max:255"
    	]);

        $disease = Disease::where('slug', $request->slug)->firstOrFail();

        if ($request->name != $disease->name) {

            $disease_slug = str_slug( $request->name );

            $i = 1;

            while ( ( Disease::where( 'slug' , $disease_slug )->count() ) > 0 ) {
                $disease_slug = str_slug( $request->name . $i );

                $i++;
            }
            $disease->slug = $disease_slug;
        }

        $disease->name = $request->name;

        if ($request->parent_id != null && $request->parent_id > 0) {
            $disease->parent_id = $request->parent_id;
        } elseif ( $disease->parent_id != $request->parent_id ) {
            $disease->parent_id = $request->parent_id;
        }

        $disease->save();

        $request->session()->flash('msg-success', trans('alerts.disease_update'));

        return redirect('/backend/disease/' . $request->post_action);
    }

    public function trash($slug) {
    	return view('backend.disease.trash', [
    		'menu' => 'topics',
    		'disease' => Disease::where('slug', $slug)->firstOrFail()
    		]);
    }

    public function postTrash(Request $request) {
        $disease = Disease::where('slug', $request->slug)->firstOrFail();

        Disease::where('slug', $disease->slug)->delete();

    	$request->session()->flash('msg-success', trans('alerts.disease_trasch', ['title' => $disease->name]));

    	return redirect('backend/disease/all');
    }

}
