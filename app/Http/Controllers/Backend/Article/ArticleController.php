<?php

namespace App\Http\Controllers\Backend\Article;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Article;
use Validator;

class ArticleController extends Controller
{
    function __construct()
    {
        Article::whereNotNull('expire_date')->where('expire_date', '<', date('Y-m-d'))->delete();
    }

    public function save(Request $request)
    {
        $status         = true;
        $title          = $request->article_title;
        $article_slug   = str_slug($title);

        $i = 1;
        while ( ( Article::where( 'slug' , $article_slug )->count() ) > 0 ) {
            $article_slug = str_slug( $title . $i );

            $i++;
        }

        $article = new Article();

        $article->title         = $title;
        $article->slug          = $article_slug;
        $article->author        = $request->article_author;
        $article->desc          = $request->article_desc;
        $article->publish_date  = $request->article_publish_date;
        $article->journal_volume= $request->article_journal_volume;
        $article->expire_date   = date('Y-m-d',strtotime( date('m/d/Y') . "+1 days"));

        if ($request->file('article_file')) {
            $file  = array('article_file' => $request->file('article_file'));
            $rules = array('mimes' => 'pdf' );

            $validator = Validator::make($file, $rules);
            if (!$validator->fails()) {

                if ($request->file('article_file')->isValid()) {
                    $extension  = $request->file('article_file')->getClientOriginalExtension();
                    $fileName   = $article_slug.'.'.$extension;

                    $request->file('article_file')->move(base_path() . '/storage/app/journal_files/pdf', $fileName);
                    $article->file = $fileName;
                }
            }
        }

        try {
            $article->save();
        } catch(\Exception $e){
            $status = false;
        }

        if ($status) {
            return json_encode([
                "status" => $status ,
                "slug"   => $article->slug
            ]);
        } else {
            return json_encode([
                "status" => $status
            ]);
        }
    }
}
