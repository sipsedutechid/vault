<?php

namespace App\Http\Controllers\Backend\Doctor;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Doctor;
use App\AcademicLevel;
use App\DoctorAcademic;
use App\Clinic;
use App\Specialist;

class DoctorController extends Controller
{
    public function home() {
        return view('backend.doctors.home', [
    		'menu' 				=> 'doctors' ,
            'doctor_count' 		=> Doctor::count(),
    		'doctor_recents' 	=> Doctor::orderBy('created_at', 'desc')->take(5)->get()
    		]);
    }

    public function create()
    {
        $specialists = Specialist::orderBy('name', 'asc')->get();
    	$specialistArr = [];
    	foreach ($specialists as $specialist) {
    		$specialistArr[$specialist->id] = $specialist->name;
    	}


    	return view('backend.doctors.form', [
    		'menu'            => 'doctors',
    		'doctor'          => new Doctor ,
            'specialists'     => $specialistArr ,
            'academic_levels' => AcademicLevel::all()
    		]);
	}

    public function postCreate(Request $request) {
        $this->validate($request, [
			"name" 			             => "required|string|max:255" ,
		    "front_title"                => "string|max:255" ,
            "back_title"                 => "string|max:255" ,
            "specialist_subclass"        => "integer" ,
            "place_birth"                => "string|max:255" ,
            "date_birth"                 => "date" ,
            "gender"                     => "in:m,f",
            "email"                      => "email|max:255" ,
            "phone"                      => "string|max:255" ,
            "mobile"                     => "string|max:255" ,
            "address" 		             => "string|max:255" ,
            "academics.*.school_college" => "string|max:255" ,
            "academics.*.year"           => "integer" ,
            "academics.*.address"        => "string|max:255" ,
            "academics.*.level"          => "integer|exists:academic_levels,id"
    		]);

        $doctor_slug = str_slug( $request->name );

        $i = 1;
        while ( ( Doctor::where( 'slug' , $doctor_slug )->count() ) > 0 ) {
            $doctor_slug = str_slug( $request->name . $i );

            $i++;
        }

        $doctor = new Doctor;

        $doctor->name                       = $request->name;
        $doctor->front_title                = $request->front_title;
        $doctor->back_title                 = $request->back_title;
        $doctor->specialist_subclasses_id   = $request->specialist_subclass;
        $doctor->date_birth                 = $request->date_birth;
        $doctor->place_birth                = $request->place_birth;
        $doctor->gender                     = $request->gender;
        $doctor->email                      = $request->email;
        $doctor->phone                      = $request->phone;
        $doctor->mobile                     = $request->mobile;
        $doctor->address                    = $request->address;
        $doctor->slug                       = $doctor_slug;

        $doctor->save();

        if ($request->input('academics.*')) {
            foreach ($request->input('academics.*') as $data_academic) {
                $academic = DoctorAcademic::create([
                    'doctor_id'         => $doctor->id ,
                    'school_college'    => $data_academic['school_college'] ,
                    'year'              => $data_academic['year'] ,
                    'address'           => $data_academic['address'] ,
                    'academic_level_id'=> $data_academic['level']
                ]);
                echo  $data_academic['level'];
            }
        }

        if ($request->input('clinics.*')) {
            foreach ($request->input('clinics.*') as $data_clinic) {
                $clinic = Clinic::create([
                    'doctor_id' => $doctor->id ,
                    'name'      => $data_clinic['name'] ,
                    'address'   => $data_clinic['address'] ,
                    'city'      => $data_clinic['city'] ,
                    'telp'      => $data_clinic['telp']
                ]);
            }

        }
        // return redirect('/backend/doctors/');
    }

    public function edit(Request $request) {
        $specialists = Specialist::orderBy('name', 'asc')->get();
    	$specialistArr = [];
    	foreach ($specialists as $specialist) {
    		$specialistArr[$specialist->id] = $specialist->name;
    	}

        return view('backend.doctors.form', [
    		'menu'            => 'doctors',
    		'doctor'          => Doctor::where('slug', $request->slug)->first() ,
            'specialists'     => $specialistArr ,
            'academic_levels' => AcademicLevel::all()
    		]);
    }
}
