<?php

namespace App\Http\Controllers\Backend\Job;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

use App\Job;

class JobController extends Controller
{
    public function home(Request $request)
    {
        return view('backend.jobs.home', [
    		'menu' 				=> 'jobs' ,
            'recent_jobs'     => Job::orderBy('created_at', 'desc')->take(5)->get() ,
            'job_count'       => Job::count()
        ]);
    }

    public function listAll(Request $request) {
        if ($request->ajax()) {

            $jobs = Job::where( 'id' , '<', $request->last_id )
                    ->orderBy('id' , 'desc')
                    ->take(10)
                    ->get();

            $last_job = Job::orderBy('id' , 'asc')
                    ->firstOrFail();

            $view = view('backend.jobs.list-table', ['jobs' => $jobs]);

            $total_job = count($jobs);
            //($lsat_id = 0) telling the client side to hide the load more button
            //if row selected is the last row in db
            if ( $total_job > 0 && $last_job->id === $jobs[ $total_job - 1 ]->id )
                $last_id = 0 ;
            else
                $last_id = $jobs[ $total_job - 1 ]->id;

            return json_encode([
                'last_id' => $last_id ,
                'html'    => $view->render()
            ]);

        } elseif ( $request->s ) {
            $s = explode( ' ' , $request->s );

            $jobs = Job::where( function ($query) use ($s){
                        $query->where(function($query) use ($s) {
                            foreach ($s as $key) {
                                $query->orWhere('position' , 'LIKE' , '%' . $key . '%');
                                $query->orWhere('institution' , 'LIKE' , '%' . $key . '%');
                            }
                        });
                    })
                    ->orderBy('id', 'desc')
                    ->get();

            $loadMore = false;
        } else {
            $jobs = Job::orderBy('id', 'desc')
                   ->take(10)
                   ->get();

            $loadMore = Job::count() >= 10;
        }

        return view('backend.jobs.list', [
            'menu'      => 'jobs' ,
            'jobs'      => $jobs ,
            'loadMore'  => $loadMore
        ]);
    }

    public function create(Request $request)
    {
        return view('backend.jobs.form', [
    		'menu' => 'jobs' ,
            'job' => new Job
        ]);
    }

    public function postCreate(Request $request) {
        $this->validate($request, [
            "position" 			  => "required|string|max:255" ,
            "institution"         => "required|string|max:255" ,
            "desc"                => "string"
            ]);

        $job_slug = str_slug( $request->position );
        $matching_slug = Job::where( 'slug' , 'like', $job_slug . '%' )->orderBy('slug', 'desc')->first();
        $slug_count = str_replace($job_slug, '', $matching_slug);

        if ($slug_count) $job_slug .= $slug_count + 1;

        $jobs = new Job;

        $file = array('logo' => $request->file('logo'));
        $rules = array('mimes' => 'jpeg,bmp,png,jpg, pdf' );

        if ($request->file('logo')) {
            $validator = Validator::make($file, $rules);
            if (!$validator->fails()) {

                if ($request->file('logo')->isValid()) {
                    $extension          = $request->file('logo')->getClientOriginalExtension();
                    $fileName           = $job_slug.'.'.$extension;

                    $request->file('logo')->move(base_path() . '/storage/app/jobs', $fileName);
                    $jobs->logo = $fileName;
                }
            }
        }

        $jobs->position    = $request->position;
        $jobs->institution = $request->institution;
        $jobs->slug        = $job_slug;
        $jobs->desc        = $request->desc;
        $jobs->requirements= $request->requirements;
        $jobs->address     = $request->address;
        $jobs->phone_name  = $request->phone_name;
        $jobs->phone_number= $request->phone_number;
        $jobs->email       = $request->email;
        $jobs->website     = $request->website;
        $jobs->city        = $request->city;
        $jobs->province    = $request->province;
        $jobs->long        = $request->long;
        $jobs->lat         = $request->lat;
        $jobs->start_at    = $request->start_at;
        $jobs->end_at      = $request->end_at;

        $jobs->save();

        if ($request->post_action == 'add_other') return redirect('/backend/job/new');
        else return redirect('/backend/job/all');
    }

    public function edit(Request $request) {
        return view('backend.jobs.form', [
    		'menu'  => 'jobs' ,
            'job' => Job::where('slug', $request->slug)->first()
        ]);
    }

    public function postUpdate(Request $request) {
        $this->validate($request, [
            "position" 			             => "required|string|max:255" ,
            "institution"                  => "required|string|max:255" ,
            "description"                => "string"
            ]);

        $job = Job::where('slug', $request->slug)->firstOrFail();

        $job_slug = str_slug( $request->name );
        $matching_slug = Job::where( 'slug' , 'like', $job_slug . '%' )
            ->where( 'id', '<>', $job->id )
            ->orderBy('slug', 'desc')
            ->first();
        $slug_count = str_replace($job_slug, '', $matching_slug);

        if ($slug_count) $job_slug .= $slug_count + 1;

        if ($request->name != $job->name) $job->slug = $job_slug;

        $job->position    = $request->position;
        $job->institution = $request->institution;
        $job->desc        = $request->desc;
        $job->requirements= $request->requirements;
        $job->address     = $request->address;
        $job->phone_name  = $request->phone_name;
        $job->phone_number= $request->phone_number;
        $job->email       = $request->email;
        $job->website     = $request->website;
        $job->city        = $request->city;
        $job->province    = $request->province;
        $job->long        = $request->long;
        $job->lat         = $request->lat;
        $job->start_at    = $request->start_at;
        $job->end_at      = $request->end_at;

        $job->save();
        if ($request->post_action == 'continue') return redirect('/backend/job/' . $job->slug . '/edit');
        else return redirect('/backend/job/all');
    }

    public function trash($slug) {
    	return view('backend.jobs.trash', [
    		'menu' => 'jobs',
    		'job' => Job::where('slug', $slug)->firstOrFail()
        ]);
    }

    public function postTrash(Request $request) {
        $job = JOb::where('slug', $request->slug)->firstOrFail();
        Job::where('slug', $request->slug)->delete();

    	$request->session()->flash('msg-success', trans('alerts.job_trash', ['title' => $job->position]));
    	return redirect('backend/job/all');
    }
}
