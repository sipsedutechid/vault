<?php

namespace App\Http\Controllers\Backend\Content;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Content;
// use Request;
use Validator;

class ContentController extends Controller
{
    public function save(Request $request) {
        $status         = true;
        $title          = $request->content_title;
        $content_slug   = str_slug($title);

        $i = 1;
        while ( ( Content::where( 'slug' , $content_slug )->count() ) > 0 ) {
            $content_slug = str_slug( $title . $i );

            $i++;
        }

        $content = new Content();

        $content->title      = $title;
        $content->label      = $request->content_label;
        $content->slug       = $content_slug;
        $content->type       = $request->content_type;
        $content->appearance = $request->content_appearance;
        $content->level_id   = $request->content_level_id;
        $content->is_free    = $request->content_free ?: 0;
        $content->is_media_data = $request->media_data ?: 0;
        $content->url_jw     = $request->url_jw;
        $content->order      = $request->order;

        switch ( $request->content_type ) {
            case 'text':
                $content->data  = $request->content_desc;
                break;

            case 'embed':
                $content->data  = $request->content_embed;
                break;

            case 'link':
                $content->data  = $request->content_link;
                break;

            case 'file':
                $file = array('content_file' => $request->file('content_file'));
                $rules = array('mimes' => 'jpeg,bmp,png,jpg, pdf' );

                $validator = Validator::make($file, $rules);
                if (!$validator->fails()) {

                    if ($request->file('content_file')->isValid()) {
                        $extension          = $request->file('content_file')->getClientOriginalExtension();
                        $fileName           = $content_slug.'.'.$extension;

                        $request->file('content_file')->move(base_path() . '/storage/app/content_files', $fileName);
                        $content->data = $fileName;
                    }
                }
                break;
        }

        try {
            $content->save();
        } catch(\Exception $e){
            $status = false;
        }

        if ($status) {
            return json_encode([
                "status"  => $status ,
                "content" => $content
            ]);
        } else {
            return json_encode([
                "status" => $status
            ]);
        }
    }

    public function search(Request $request) {
        $q = explode( ' ' , $request->input( 'keyword' ) );

        $contents = Content::where(function ($query) use ($q){

                $query->where(function($query) use ($q) {
                    foreach ($q as $key) {
                        $query->orWhere('title' , 'LIKE' , '%' . $key . '%');
                    }
                });

            })
            ->get();

        return view('backend.topics.contents.list-search', [
    		'contents' => $contents
    	]);
    }

    public function view(Request $request) {
        $q = explode( ' ' , $request->input( 'keyword' ) );

        $content = Content::where('id' , $request->id)
            ->firstOrFail();

        return view('backend.topics.contents.view', [
    		'content' => $content
    	]);
    }
}
