<?php

namespace App\Http\Controllers\Backend\Drug;

use Illuminate\Http\Request;

use App\DrugManufacturer;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Image;
use Storage;

class ManufacturerController extends Controller
{
    
    public function home(Request $request)
    {
    	$appends = [];
    	$manufacturers = null;
    	if (strlen($request->s) > 0) {
    		$manufacturers = DrugManufacturer::where('name', 'like', '%' . $request->s . '%')
    			->orderBy('name', 'asc')
    			->paginate(12);
    		$appends['s'] = $request->s;
    	}
    	else {
	    	$manufacturers = DrugManufacturer::with('drugs')->orderBy('name', 'asc')->paginate(12);
	    }

    	return view('backend.drugs.manufacturers.list', [
    		'menu' => 'drugs',
    		'manufacturers' => $manufacturers,
    		'appends' => $appends
    		]);
    }

    public function edit($slug, Request $request)
    {
    	$manufacturer = DrugManufacturer::where('slug', $slug)->firstOrFail();
    	return view('backend.drugs.manufacturers.form-edit', [
    		'menu' => 'drugs',
    		'manufacturer' => $manufacturer,
    		'manufacturers' => DrugManufacturer::orderBy('name', 'asc')->get(),
    		'drugs' => $manufacturer->drugs()->orderBy('name', 'asc')->get(),
    		'marketed_drugs' => $manufacturer->marketedDrugs()->orderBy('name', 'asc')->get(),
    		]);
    }

    public function postEdit($slug, Request $request)
    {
    	$manufacturer = DrugManufacturer::where('slug', $slug)->firstOrFail();
    	if ($request->post_action == 'remove-logo') {	
    		if ($manufacturer->logo_image)
    			Storage::delete('drug-manufacturer-logos/' . $manufacturer->logo_image);
    		$manufacturer->logo_image = null;
    		$manufacturer->save();

    		$request->session()->flash('msg-success', trans('alerts.drug_manufacturer_logo_removed'));
    		return redirect('backend/drug/manufacturer/' . $manufacturer->slug . '/edit');
    	}
    	$this->validate($request, [
    		'name' => 'required|max:255',
    		'slug' => 'required|unique:drug_manufacturers,slug,' . $manufacturer->id,
    		'logo_image' => 'image|max:512',
    		'post_action' => 'required|in:all,edit,drugs'
    		]);
    	
    	$manufacturer->name = $request->name;
    	$manufacturer->slug = $request->slug;
    	if ($request->hasFile('logo_image')) {
    		if ($manufacturer->logo_image) {
    			Storage::delete('drug-manufacturer-logos/' . $manufacturer->logo_image);
    			$manufacturer->logo_image = null;
    		}
    		$file = $request->file('logo_image');
    		$filename = str_slug($file->getClientOriginalName()) . time() . '_' . str_random(6) . '.' . $file->guessExtension();
    		Image::make($file)->fit(300, 120)->save('../storage/app/drug-manufacturer-logos/' . $filename);
    		$manufacturer->logo_image = $filename;
		}
		$manufacturer->save();

		$request->session()->flash('msg-success', trans('alerts.drug_manufacturer_updated'));
		return redirect('backend/drug/manufacturer/' . $manufacturer->slug . '/edit');
    }

    public function trash($slug, Request $request)
    {
    	$manufacturer = DrugManufacturer::where('slug', $slug)->firstOrFail();
    	$this->validate($request, ['move_to' => 'required|string|exists:drug_manufacturers,slug|not_in:' . $manufacturer->slug]);

    	$moveto = DrugManufacturer::where('slug', $request->move_to)->firstOrFail();
    	$moveto->drugs()->saveMany($manufacturer->drugs);
    	$moveto->marketedDrugs()->saveMany($manufacturer->marketedDrugs);
    	$manufacturer->delete();

    	$request->session()->flash('msg-success', trans('alerts.drug_manufacturer_trash'));
    	return redirect('backend/drug/manufacturers');
    }

    public function logo($slug)
    {
    	$manufacturer = DrugManufacturer::where('slug', $slug)->firstOrFail();
    	if ($manufacturer->logo_image)
    		return Storage::get('drug-manufacturer-logos/' . $manufacturer->logo_image);

    	return;
    }
}
