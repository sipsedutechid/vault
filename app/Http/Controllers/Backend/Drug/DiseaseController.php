<?php

namespace App\Http\Controllers\Backend\Drug;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DrugDisease;

class DiseaseController extends Controller
{
    public function home()
    {
    	$diseases = DrugDisease::all();
    	return view('backend.drugs.diseases.list', [
    		'menu' 		=> 'drugs',
    		'diseases'	=> $diseases
    		]);
    }

    public function single(Request $request, $slug = null)
    {
    	return view('backend.drugs.diseases.form', [
    		'menu' 		=> 'drugs',
    		'classes'	=> \App\DrugClass::all(),
    		'disease'	=> $slug == 'new' ? new DrugDisease : DrugDisease::where('slug', $slug)->firstOrFail()
    		]);
    }

    public function delete(Request $request, $slug)
    {
    	$disease = DrugDisease::where('slug', $slug)->firstOrFail();
    	$disease->delete();

    	$request->session()->flash('msg-success', 'Drug disease deleted');
    	return redirect('/backend/drug/diseases');
    }

    public function trash()
    {
    	return view('backend.drugs.diseases.list_trash', [
    		'menu'	=> 'drugs',
    		'diseases'	=> DrugDisease::onlyTrashed()->get()
    		]);
    }

    public function restore(Request $request, $slug)
    {
    	$disease = DrugDisease::onlyTrashed()->where('slug', $slug)->firstOrFail();
    	$disease->restore();

    	$request->session()->flash('msg-success', 'Drug disease restored');
    	return redirect('/backend/drug/diseases');
    }

    public function obliterate(Request $request)
    {
    	$diseases = DrugDisease::onlyTrashed()->get();
    	foreach ($diseases as $dis) {
    		$dis->drugSubclasses()->detach();
    		$dis->forceDelete();
    	}

    	$request->session()->flash('msg-success', 'Drug disease trash emptied');
    	return redirect('/backend/drug/diseases');
    }

    public function postSingle(Request $request, $slug = null)
    {
    	if ($slug == null)
    		$disease = new DrugDisease;
    	else $disease = DrugDisease::where('slug', $slug)->firstOrFail();

    	$this->validate($request, [
    		'name'				=> 'required|max:255',
    		'slug' 				=> 'required|not_in:new|unique:drug_diseases,slug' . ($slug == null ? '' : ',' . $disease->id),
    		'drugSubclasses.*'	=> 'boolean'
    		]);

    	$disease->name = $request->name;
    	$disease->slug = $request->slug;
    	if ($slug == null)
    		$disease->created_by = auth()->user()->id;
    	$disease->save();

    	$disease->drugSubclasses()->sync($request->input('drugSubclasses.*'));
    	$request->session()->flash('msg-success', 'Drug disease saved');
    	return redirect('/backend/drug/diseases');
    }
}
