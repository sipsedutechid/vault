<?php

namespace App\Http\Controllers\Backend\Drug;

use Illuminate\Http\Request;

use App\Drug;
use App\DrugClass;
use App\DrugDisease;
use App\DrugDosage;
use App\DrugForm;
use App\DrugManufacturer;
use App\DrugPackaging;
use App\DrugSubclass;
use App\Generic;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DrugController extends Controller
{
    public function home()
    {
    	return view('backend.drugs.home', [
    		'menu' 					=> 'drugs',
    		'drug_count' 			=> Drug::count(),
    		'drug_disease_count'	=> DrugDisease::count(),
    		'drug_recents' 			=> Drug::orderBy('created_at', 'desc')->take(5)->get(),
    		'generic_count' 		=> Generic::count(),
    		'class_count' 			=> DrugClass::count(),
    		'manufacturer_count'	=> DrugManufacturer::count(),
    		]);
    }

    public function listAll(Request $request)
    {
    	$appends = [];
    	if ($request->s) {
    		$drugs = Drug::where('name', 'like', '%' . $request->s . '%')
    			->orWhereHas('drugManufacturer', function ($q) use ($request)
    			{
    				$q->where('name', 'like', '%' . $request->s . '%');
    			})
    			->orWhereHas('drugClass', function ($q) use ($request) {
    				$q->where('name', 'like', '%' . $request->s . '%');
    			})
    			->orWhereHas('drugSubclass', function ($q) use ($request) {
    				$q->where('name', 'like', '%' . $request->s . '%');
    			})
    			->orderBy('name', 'asc')->take(30)->get();
    		$appends['s'] = $request->s;
    	}
    	else $drugs = Drug::orderBy('name', 'asc')->select('slug', 'name', 'drug_manufacturer_id', 'drug_class_id', 'drug_subclass_id', 'created_by', 'created_at')->with(['createdBy'])->paginate(30);

    	if ($request->json == "true")
    		return $drugs->toJSON();
    	return view('backend.drugs.list', [
    		'menu' => 'drugs',
    		'drugs' => $drugs,
    		'appends' => $appends
    		]);
    }

    public function create()
    {
    	$classes = DrugClass::orderBy('name', 'asc')->get();
    	$classArr = [];
    	foreach ($classes as $class) {
    		$classArr[$class->id] = $class->name;
    	}

    	return view('backend.drugs.form', [
    		'menu' => 'drugs',
    		'drug' => new Drug,
    		'classes' => $classArr
    		]);
	    }

    public function postCreate(Request $request)
    {
    	$this->validate($request, [
    		"drug_manufacturer_id" 		=> "integer|exists:drug_manufacturers,id",
			"manufacturer" 				=> "required|string|max:255",
			"slug" 						=> "required|string|max:255|unique:drugs,slug",
			"name" 						=> "required|string|max:255",
			"marketer" 					=> "string|max:255",
			"administration"			=> "in:cc,scc,s/cc",
			"drug_forms.*.id" 			=> "integer|exists:drug_forms,id",
			"drug_forms.*.name" 		=> "required|string|max:255",
			"drug_generic.*.id" 		=> "integer|exists:generics,id",
			"drug_generic.*.form_index" => "integer",
			"drug_generic.*.name" 		=> "required|string|max:255",
			"drug_generic.*.unit" 		=> "string|max:255",
			"dosages.*.form_index" 		=> "integer",
			"dosages.*.type"	 		=> "string|max:255",
			"dosages.*.administration"	=> "string|max:255",
			"packagings.*.form_index" 	=> "required|integer",
			"packagings.*.name" 		=> "string|max:255",
			"packagings.*.quantity" 	=> "integer",
			"packagings.*.quantity2" 	=> "integer",
			"packagings.*.quantity3" 	=> "integer",
			"packagings.*.price" 		=> "integer",
			"post_action" 				=> "required|in:all,new,manufacturer",
			"drug_class_id" 			=> "integer|required|exists:drug_classes,id",
			"drug_subclass_id" 			=> "integer|exists:drug_subclasses,id",
			"classification"			=> "in:o,g,w,b",
			"pregnancy_category" 		=> "in:a,b,c,d,x"
    		]);

		$drug_manufacturer = DrugManufacturer::where('slug', str_slug($request->manufacturer))->first();
		if (empty($request->drug_manufacturer_id) && !$drug_manufacturer)
			$drug_manufacturer = DrugManufacturer::create(['slug' => str_slug($request->manufacturer), 'name' => $request->manufacturer, 'created_by' => auth()->user()->id]);
		$drug_marketer = null;
		if (!empty($request->marketer)) {
			$drug_marketer = DrugManufacturer::where('slug', str_slug($request->marketer))->first();
			if (empty($request->drug_marketer_id) && !$drug_marketer)
				$drug_marketer = DrugManufacturer::create(['slug' => str_slug($request->marketer), 'name' => $request->marketer, 'created_by' => auth()->user()->id]);
			$drug_marketer = $drug_marketer->id;
		}

		$drug = new Drug;
		$drug->slug = $request->slug;
		$drug->name = $request->name;
		$drug->drug_class_id = $request->drug_class_id;
		$drug->drug_subclass_id = $request->drug_subclass_id;
		$drug->drug_manufacturer_id = $drug_manufacturer->id;
		$drug->drug_marketer_id = $drug_marketer;
		$drug->indication = $request->indication;
		$drug->contraindication = $request->contraindication;
		$drug->precautions = $request->precautions;
		$drug->adverse_reactions = $request->adverse_reactions;
		$drug->interactions = $request->interactions;
		$drug->classification = $request->classification;
		$drug->administration = $request->administration;
		$drug->administration_extra = $request->administration_extra;
		$drug->pregnancy_category = $request->pregnancy_category;
		$drug->created_by = auth()->user()->id;
		$drug->save();

		$drug_forms = [];
		if ($request->input('drug_forms.*'))
			foreach ($request->input('drug_forms.*') as $index => $drug_form) {
				$form = DrugForm::where('slug', str_slug($drug_form['name']))->first();
				if (empty($drug_form['id']) && !$form)
					$form = DrugForm::create([
						'slug' => str_slug($drug_form['name']),
						'name' => $drug_form['name']
						]);
				$drug_forms[$index] = $form;
				$drug->drugForms()->attach($form->id);
			}

		if ($request->input('drug_generic.*'))
			foreach ($request->input('drug_generic.*') as $drug_generic) {
				$generic = Generic::where('slug', str_slug($drug_generic['name']))->first();
				if (empty($drug_generic['id']) && !$generic)
					$generic = Generic::create([
						'slug' => str_slug($drug_generic['name']),
						'name' => $drug_generic['name'],
						'created_by' => auth()->user()->id
						]);
				$drug->generics()->attach($generic->id, [
					'drug_form_id' => $drug_generic['form_index'] != '' ? $drug_forms[$drug_generic['form_index']]->id : null,
					'quantity' => $drug_generic['quantity'] > 0 ? $drug_generic['quantity'] : null,
					'unit' => $drug_generic['unit']
					]);
			}
		if ($request->input('dosages.*'))
			foreach ($request->input('dosages.*') as $dosage) {
				$drug_dosage = new DrugDosage([
					'administration' => $dosage['administration'],
					'name' => $dosage['type'],
					'description' => $dosage['description'],
					'drug_form_id' => $dosage['form_index'] != '' ? $drug_forms[$dosage['form_index']]->id : null
					]);
				$drug->dosages()->save($drug_dosage);
			}
		if ($request->input('packagings.*'))
			foreach ($request->input('packagings.*') as $packaging) {
				$drug_packaging = new DrugPackaging([
					'name' => $packaging['name'],
					'price' => $packaging['price'] > 0 ? $packaging['price'] : null,
					'quantity' => $packaging['quantity'],
					'quantity_2' => $packaging['quantity2'],
					'quantity_3' => $packaging['quantity3'],
					'created_by' => auth()->user()->id,
					'drug_form_id' => $drug_forms[$packaging['form_index']]->id
					]);
				$drug->packagings()->save($drug_packaging);
			}

		$request->session()->flash('msg-success', trans('alerts.drug_saved'));
		if ($request->post_action == 'manufacturer')
			return redirect('/backend/drug/manufacturer/' . $drug->drugManufacturer->slug . '/edit');
		return redirect('/backend/drug/' . $request->post_action);
    }

    public function edit($slug)
    {
    	$drug = Drug::where('slug', $slug)->firstOrFail();
    	$drugForms = $drug->drugForms;
    	$classes = DrugClass::orderBy('name', 'asc')->get();
    	$classArr = [];
    	foreach ($classes as $class) {
    		$classArr[$class->id] = $class->name;
    	}

    	$generics = [];
    	foreach ($drug->generics as $generic) {
			$generic['form_index'] = 'all';
    		foreach ($drugForms as $key => $form) {
    			if ($generic->pivot->drug_form_id == $form->id) {
    				$generic['form_index'] = $key;
    				break;
    			}
    		}
    		$generic['quantity'] = $generic->pivot->quantity;
    		$generic['unit'] = $generic->pivot->unit;
    		array_push($generics, $generic);
    	}
    	$dosages = [];
    	foreach ($drug->dosages as $dosage) {
    		$dosage['form_index'] = 'all';
    		foreach ($drugForms as $key => $form) {
    			if ($dosage->drug_form_id == $form->id) {
    				$dosage['form_index'] = $key;
    				break;
    			}
    		}
    		$dosage['type'] = $dosage->name;
    		array_push($dosages, $dosage);
    	}
    	$packagings = [];
    	foreach ($drug->packagings as $packaging) {
    		foreach ($drugForms as $key => $form) {
    			if ($packaging->drug_form_id == $form->id) {
    				$packaging['form_index'] = $key;
    				break;
    			}
    		}
    		array_push($packagings, $packaging);
    	}

    	return view('backend.drugs.form', [
    		'menu' => 'drugs',
    		'drug' => $drug,
    		'drugForms' => $drugForms,
    		'generics' => $generics,
    		'dosages' => $dosages,
    		'packagings' => $packagings,
    		'classes' => $classArr
    		]);
    }

    public function postEdit(Request $request)
    {
		$drug = Drug::findOrFail($request->id);

    	$this->validate($request, [
    		"drug_manufacturer_id" 		=> "integer|exists:drug_manufacturers,id",
			"manufacturer" 				=> "required|string|max:255",
			"slug" 						=> "required|string|max:255|unique:drugs,slug," . $drug->id,
			"name" 						=> "required|string|max:255",
			"marketer" 					=> "string|max:255",
			"administration"			=> "in:cc,scc,s/cc",
			"drug_forms.*.id" 			=> "integer|exists:drug_forms,id",
			"drug_forms.*.name" 		=> "required|string|max:255",
			"drug_generic.*.id" 		=> "integer|exists:generics,id",
			"drug_generic.*.form_index" => "integer",
			"drug_generic.*.name" 		=> "required|string|max:255",
			"drug_generic.*.unit" 		=> "string|max:255",
			"dosages.*.form_index" 		=> "integer",
			"dosages.*.type"	 		=> "string|max:255",
			"dosages.*.administration"	=> "string|max:255",
			"packagings.*.form_index" 	=> "required|integer",
			"packagings.*.name" 		=> "string|max:255",
			"packagings.*.quantity" 	=> "integer",
			"packagings.*.quantity2" 	=> "integer",
			"packagings.*.quantity3" 	=> "integer",
			"packagings.*.price" 		=> "integer",
			"post_action" 				=> "required|in:all,new,edit,manufacturer,dashboard",
			"drug_class_id" 			=> "integer|required|exists:drug_classes,id",
			"drug_subclass_id" 			=> "integer|exists:drug_subclasses,id",
			"classification"			=> "in:o,g,w,b",
			"pregnancy_category" 		=> "in:a,b,c,d,x"
    		]);

		$drug_manufacturer = DrugManufacturer::where('slug', str_slug($request->manufacturer))->first();
		if (empty($request->drug_manufacturer_id) && !$drug_manufacturer)
			$drug_manufacturer = DrugManufacturer::create([
				'slug' => str_slug($request->manufacturer),
				'name' => $request->manufacturer,
				'created_by' => auth()->user()->id
				]);
		$drug_marketer = null;
		if ($request->marketer) {
			$drug_marketer = DrugManufacturer::where('slug', str_slug($request->marketer))->first();
			if (empty($request->drug_marketer_id) && !$drug_marketer)
				$drug_marketer = DrugManufacturer::create(['slug' => str_slug($request->marketer), 'name' => $request->marketer, 'created_by' => auth()->user()->id]);
			$drug_marketer = $drug_marketer->id;
		}

		$drug->slug = $request->slug;
		$drug->name = $request->name;
		$drug->drug_class_id = $request->drug_class_id;
		$drug->drug_subclass_id = $request->drug_subclass_id;
		$drug->drug_manufacturer_id = $drug_manufacturer->id;
		$drug->drug_marketer_id = $drug_marketer;
		$drug->indication = $request->indication;
		$drug->contraindication = $request->contraindication;
		$drug->precautions = $request->precautions;
		$drug->adverse_reactions = $request->adverse_reactions;
		$drug->interactions = $request->interactions;
		$drug->classification = $request->classification;
		$drug->administration = $request->administration;
		$drug->administration_extra = $request->administration_extra;
		$drug->pregnancy_category = $request->pregnancy_category;
		$drug->save();

		// Update drug forms
		$drug_drug_forms = [];
		$drug_forms = [];
		if ($request->input('drug_forms.*'))
			foreach ($request->input('drug_forms.*') as $index => $drug_form) {
				$form = DrugForm::where('slug', str_slug($drug_form['name']))->first();
				if (empty($drug_form['id']) && !$form)
					$form = DrugForm::create([
						'slug' => str_slug($drug_form['name']),
						'name' => $drug_form['name']
						]);
				array_push($drug_drug_forms, $form->id);
				$drug_forms[$index] = $form;
			}
		$drug->drugForms()->sync($drug_drug_forms);

		// Update generics
		$drug->generics()->detach();
		if ($request->input('drug_generic.*'))
			foreach ($request->input('drug_generic.*') as $drug_generic) {
				$generic = Generic::where('slug', str_slug($drug_generic['name']))->first();
				if (empty($drug_generic['id']) && !$generic)
					$generic = Generic::create(['slug' => str_slug($drug_generic['name']), 'name' => $drug_generic['name'], 'created_by' => auth()->user()->id]);
				$drug_generic = [
					'drug_form_id' => $drug_generic['form_index'] != '' ? $drug_forms[$drug_generic['form_index']]->id : null,
					'quantity' => $drug_generic['quantity'] > 0 ? $drug_generic['quantity'] : null,
					'unit' => $drug_generic['unit']
					];
				$drug->generics()->attach($generic['id'], $drug_generic);
			}

		// Update dosages
		DrugDosage::where('drug_id', $drug->id)->delete();
		if ($request->input('dosages.*'))
			foreach ($request->input('dosages.*') as $dosage) {
				$drug_dosage = new DrugDosage([
					'administration' => $dosage['administration'],
					'name' => $dosage['type'],
					'description' => $dosage['description'],
					'drug_form_id' => $dosage['form_index'] != '' ? $drug_forms[$dosage['form_index']]->id : null
					]);
				$drug->dosages()->save($drug_dosage);
			}

		// Update packagings
		DrugPackaging::where('drug_id', $drug->id)->delete();
		if ($request->input('packagings.*'))
			foreach ($request->input('packagings.*') as $packaging) {
				$drug_packaging = new DrugPackaging([
					'name' => $packaging['name'],
					'price' => $packaging['price'] > 0 ? $packaging['price'] : null,
					'quantity' => $packaging['quantity'],
					'quantity_2' => $packaging['quantity2'],
					'quantity_3' => $packaging['quantity3'],
					'created_by' => auth()->user()->id,
					'drug_form_id' => $drug_forms[$packaging['form_index']]->id]
					);
				$drug->packagings()->save($drug_packaging);
			}

		$request->session()->flash('msg-success', trans('alerts.drug_updated'));
		if ($request->post_action == 'manufacturer')
			return redirect('/backend/drug/manufacturer/' . $drug->drugManufacturer->slug . '/edit');
		if ($request->post_action == 'edit')
			return redirect('/backend/drug/' . $drug->slug . '/edit');
		if ($request->post_action == 'dashboard')
			return redirect('/backend/drugs');
		return redirect('/backend/drug/all');
    }

    public function trash($slug)
    {
    	return view('backend.drugs.trash', [
    		'menu' => 'drugs',
    		'drug' => Drug::where('slug', $slug)->firstOrFail()
    		]);
    }

    public function postTrash(Request $request)
    {
    	Drug::where('slug', $request->slug)->delete();

    	$request->session()->flash('msg-success', trans('alerts.drug_trash'));
    	return redirect('backend/drug/all');
    }

}
