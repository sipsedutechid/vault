<?php

namespace App\Http\Controllers\Backend\Drug;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\DrugClass;
use App\DrugSubclass;
use App\Generic;

class GenericController extends Controller
{
    public function home(Request $request)
    {
    	$generics = Generic::orderBy('name', 'asc');
    	$appends = [];
    	if ($request->s) {
    		$generics = $generics->where('name', 'like', '%' . $request->s . '%');
    		$appends['s'] = $request->s;
    	}
    	return view('backend.drugs.generics.list', [
    		'menu' => 'drugs',
    		'appends'	=> $appends,
    		'generics'	=> $generics->paginate(15)
    	]);
    }

    public function getCreate()
    {
        return;
    }

    public function getEdit($slug)
    {
        $generic = Generic::where('slug', $slug)->firstOrFail();
        $drugSubclasses = $generic->drug_class_id ?
            DrugSubclass::dropdown($generic->drug_class_id) : [];
        return view('backend.drugs.generics.form', [
            'menu' => 'drugs',
            'generic' => $generic,
            'drugClasses' => DrugClass::dropdown(),
            'drugSubclasses' => $drugSubclasses,
            ]);
    }

    public function postEdit($slug, Request $request)
    {
        $generic = Generic::where('slug', $slug)->firstOrFail();
        $this->validate($request, [
            'name'              => 'required|max:255',
            'slug'              => 'required|max:255|unique:generics,slug,' . $generic->id,
            'is_generic'        => 'boolean',
            'alias'             => 'max:255',
            'parent_id'         => 'integer|exists:generics,id',
            'post_action'       => 'required|in:list,edit',
            ]);

        $generic->name          = $request->name;
        $generic->slug          = $request->slug;
        $generic->is_generic    = $request->is_generic == 1;
        $generic->alias         = $request->alias;
        $generic->description   = $request->description;
        $generic->onset         = $request->onset;
        $generic->duration      = $request->duration;
        $generic->absorption    = $request->absorption;
        $generic->distribution  = $request->distribution;
        $generic->metabolism    = $request->metabolism;
        $generic->excretion     = $request->excretion;
        $generic->parent_id     = $request->parent_id ? $request->parent_id : null;
        $generic->save();

        $generic->drugSubclasses()->sync($request->input('subclasses.*') == null ? [] : $request->input('subclasses.*'));

        $request->session()->flash('msg-success', trans('alerts.drug_generic_saved'));
        return redirect($request->post_action == 'edit' ? '/backend/drug/generic/' . $generic->slug . '/edit' : '/backend/drug/generics');
    }

    public function postMerge($slug, Request $request)
    {
        $generic = Generic::where('slug', $slug)->firstOrFail();
        $this->validate($request, ['merge_generic'	=> 'required|integer|exists:generics,id|not_in:' . $generic->id]);

        $merge_generic = Generic::findOrFail($request->merge_generic);
        foreach($generic->drugs as $drug) {
	        $merge_generic->drugs()->attach($drug->id, [
	        	'quantity' 		=> $drug->pivot->quantity,
	        	'unit' 			=> $drug->pivot->unit,
	        	'drug_form_id'	=> $drug->pivot->drug_form_id,
	        	]);
        }

        $generic->delete();

        $request->session()->flash('msg-success', trans('alerts.drug_generic_merged', ['old' => $generic->name, 'new' => $merge_generic->name]));
        return redirect('/backend/drug/generics');
    }

}
