<?php

namespace App\Http\Controllers\Backend\Drug;

use Illuminate\Http\Request;

use App\DrugClass;
use App\DrugSubclass;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ClassController extends Controller
{

    public function home()
    {
    	return view('backend.drugs.classes.list', [
    		'menu' => 'drugs',
    		'classes' => DrugClass::orderBy('name', 'asc')->paginate(15)
    		]);
    }

    public function edit($slug)
    {
    	$class = DrugClass::where('code', $slug)->firstOrFail();
    	return view('backend.drugs.classes.form-edit', [
    		'menu' => 'drugs',
    		'class' => $class,
    		'subclasses' => $class->subclasses()->orderBy('name', 'asc')->get()
    		]);
    }

    public function post(Request $request)
    {
    	$this->validate($request, [
    		'name' => 'required|max:255'
    		]);

    	$class = $request->id ? DrugClass::findOrFail($request->id) : new DrugClass;
    	$class->code = str_slug($request->name);
    	$class->name = $request->name;
    	$class->created_by = $request->id ? $class->created_by : auth()->user()->id;
    	$class->save();

    	if ($request->has('remove'))
	    	foreach ($request->input('remove.*') as $remove) {
	    		DrugSubclass::where('id', $remove)->delete();
	    	}
	    if (!empty(trim($request->subclasses))) {
	    	$subclasses = explode(PHP_EOL, $request->subclasses);
	    	foreach ($subclasses as $subclass) {
	    		if (!empty(trim($subclass)) && DrugSubclass::where('code', str_slug($subclass))->count() == 0) {
		    		$subclass_obj = new DrugSubclass(['code' => str_slug($subclass), 'name' => trim($subclass), 'created_by' => auth()->user()->id]);
		    		$class->subclasses()->save($subclass_obj);
		    	}
	    	}
	    }

    	$request->session()->flash('msg-success', 'Drug class saved successfully');
    	return redirect('backend/drug/classes');
    }

    public function trash($slug, Request $request)
    {
    	$class = DrugClass::where('code', $slug)->firstOrFail();
    	DrugSubclass::where('drug_class_id', $class->id)->delete();
    	DrugClass::where('code', $slug)->delete();

    	$request->session()->flash('msg-success', 'Drug class deleted');
    	return redirect('backend/drug/classes');
    }

}
