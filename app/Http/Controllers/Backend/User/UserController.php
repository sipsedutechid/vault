<?php

namespace App\Http\Controllers\Backend\User;

use Illuminate\Http\Request;

use App\Role;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

	public function home()
	{
		return view('backend.users.home', [
			'menu' 			=> 'users',
			'role_count'	=> Role::count(),
			'user_count'	=> User::count()
			]);
	}

    public function listAll(Request $request)
    {
    	return view('backend.users.list', [
    		'menu' => 'users',
    		'users' => User::paginate(15)
    		]);
    }

    public function create()
    {
    	return view('backend.users.form', [
    		'menu' => 'users',
            'roles'	=> Role::dropdown(),
            'user' => new User
            ]);
    }

    public function edit($login)
    {
        return view('backend.users.form', [
            'menu'  => 'users',
            'roles' => Role::dropdown(),
    		'user' 	=> User::where('login', $login)->firstOrFail()
    		]);
    }

    public function postCreate(Request $request)
    {
    	$this->validate($request, [
    		'login' 			=> 'required|max:255',
    		'name'				=> 'required|max:255',
            'password'          => 'required|min:8',
    		'role'				=> 'required|integer',
    		'confirm_password'	=> 'required|same:password'
    		]);

    	$user = new User;
    	$user->login = $request->login;
    	$user->name = $request->name;
    	$user->password = bcrypt($request->password);
    	$user->role_id = $request->role;
    	$user->save();

    	$request->session()->flash('msg-success', 'User saved successfully');
    	return redirect('backend/user/all');
    }

    public function postEdit(Request $request)
    {
    	$this->validate($request, [
    		'login' 			=> 'required|max:255',
    		'name'				=> 'required|max:255',
    		'password'			=> 'min:8',
    		'role'				=> 'required|integer',
    		'confirm_password'	=> 'required_with:password|same:password'
    		]);

    	$user = new User;
    	$user->login = $request->login;
    	$user->name = $request->name;
    	$user->role_id = $request->role;
    	if ($request->password) 
    		$user->password = bcrypt($request->password);
    	$user->save();

    	$request->session()->flash('msg-success', 'User saved successfully');
    	return redirect('backend/user/all');
    }
}
