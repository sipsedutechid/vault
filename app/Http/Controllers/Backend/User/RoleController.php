<?php

namespace App\Http\Controllers\Backend\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ability;
use App\Role;

class RoleController extends Controller
{
    public function listAll()
    {
    	return view('backend.users.roles.list', [
    		'menu'	=> 'users',
    		'roles'	=> Role::orderBy('name', 'asc')->get()
    		]);
    }

    public function create()
    {
    	
    }

    public function edit($slug)
    {
    	$role = Role::where('slug', $slug)->firstOrFail();
    	return view('backend.users.roles.form', [
    		'abilities'	=> Ability::where('parent_id', null)->get(),
    		'menu'		=> 'users',
    		'role'		=> $role,
    		]);
    }

    public function postCreate()
    {
    	
    }

    public function postEdit($slug)
    {
    	$role = Role::where('slug', $slug)->firstOrFail();
    }
}
