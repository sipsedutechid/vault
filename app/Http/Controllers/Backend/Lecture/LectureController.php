<?php

namespace App\Http\Controllers\Backend\Lecture;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lecture;
use Validator;

class LectureController extends Controller
{
    public function save(Request $request){
        $err           = '';
        $status        = true;
        $name          = $request->lecture_name;
        $lecture_slug  = str_slug($name);

        $i = 1;
        while ( ( Lecture::where( 'slug' , $lecture_slug )->count() ) > 0 ) {
            $lecture_slug = str_slug( $name . $i );

            $i++;
        }

        $lecture = new Lecture();

        $lecture->front_title   = $request->lecture_front_title;
        $lecture->name          = $name;
        $lecture->back_title    = $request->lecture_back_title;
        $lecture->slug          = $lecture_slug;
        $lecture->bio           = $request->lecture_bio;

        if ( $request->file('lecture_photo') ) {
            $file = array('lecture_photo' => $request->file('lecture_photo'));
            $rules = array('mimes' => 'jpeg,bmp,png,jpg' );

            $validator = Validator::make($file, $rules);
            if (!$validator->fails()) {

                if ($request->file('lecture_photo')->isValid()) {
                    $extension          = $request->file('lecture_photo')->getClientOriginalExtension();
                    $fileName           = $lecture_slug.'.'.$extension;

                    $request->file('lecture_photo')->move(base_path() . '/storage/app/lecture_photo', $fileName);
                    $lecture->photo = $fileName;
                }
            }
        }

        try {
            $lecture->save();
        } catch(\Exception $e){
            $status = false;
            $err    =  $e;
        }

        if ($status) {
            return json_encode([
                "status"  => $status ,
                "lecture" => $lecture ,
                "msg_error" => $err
            ]);
        } else {
            return json_encode([
                "status"    => $status ,
                "msg_error" => $err
            ]);
        }
    }

    public function search(Request $request) {
        $q = explode( ' ' , $request->input( 'keyword' ) );

        $lectures = Lecture::where(function ($query) use ($q){

                $query->where(function($query) use ($q) {
                    foreach ($q as $key) {
                        $query->orWhere('name' , 'LIKE' , '%' . $key . '%');
                    }
                });

            })
            ->get();

        return view('backend.topics.lectures.list-search', [
    		'lectures' => $lectures
    	]);
    }
}
