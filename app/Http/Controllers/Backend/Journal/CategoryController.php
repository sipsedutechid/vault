<?php

namespace App\Http\Controllers\Backend\Journal;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\JournalCategory;

class CategoryController extends Controller
{
    public function listAll(Request $request)
    {
        if ($request->ajax()) {

            $category = JournalCategory::where( 'id' , '<', $request->last_id )
                    ->orderBy('id' , 'desc')
                    ->take(10)
                    ->get();

            $last_category = JournalCategory::orderBy('id' , 'asc')
                    ->firstOrFail();

            $view = view('backend.journals.categories.list-table', ['category' => $category]);

            $total_category = count($category);
            //($lsat_id = 0) telling the client side to hide the load more button
            //if row selected is the last row in db
            if ( $total_category > 0 && $last_category->id === $category[ $total_category - 1 ]->id )
                $last_id = 0 ;
            else
                $last_id = $category[ $total_category - 1 ]->id;

            return json_encode([
                'last_id' => $last_id ,
                'html'    => $view->render()
            ]);

        } elseif ( $request->s ) {
            $s = explode( ' ' , $request->s );

            $category = JournalCategory::where( function ($query) use ($s){
                        $query->where(function($query) use ($s) {
                            foreach ($s as $key) {
                                $query->orWhere('labels' , 'LIKE' , '%' . $key . '%');
                            }
                        });
                    })
                    ->orderBy('id', 'desc')
                    ->get();

            $hide = true;
        } else {
            $category = JournalCategory::orderBy('id', 'desc')
                   ->take(10)
                   ->get();

            $hide = JournalCategory::count() <= 10;
        }

        return view('backend.journals.categories.list', [
            'menu'      => 'journals' ,
            'categories' => $category ,
            'hide'      => $hide
        ]);
    }

    public function create(Request $request)
    {
        return view('backend.journals.categories.form', [
    		'menu'              => 'journals' ,
            'category'          => new JournalCategory ,
            'request'           => $request ,
    		]);
    }

    public function postCreate(Request $request)
    {
        $this->validate($request, [
			"labels" => "required|string|max:255"
    	]);

        $category_slug = str_slug( $request->labels );

        $i = 1;
        while ( ( JournalCategory::where( 'slug' , $category_slug )->count() ) > 0 ) {
            $category_slug = str_slug( $request->labels . $i );

            $i++;
        }

        $category = new JournalCategory;

        $category->labels       = $request->labels;
        $category->description  = $request->description;
        $category->slug         = $category_slug;
        $category->created_by   = auth()->user()->id;

        $category->save();

        $request->session()->flash('msg-success', trans('alerts.journal_category_saved'));

        return redirect('/backend/journal/categories/');
    }

    public function edit(Request $request)
    {
        $category = JournalCategory::where('slug' , $request->slug)->first();

        return view('backend.journals.categories.form', [
            'menu'        => 'journals' ,
    		'category'    => $category ,
            'request'     => $request
    	]);
    }

    public function postUpdate(Request $request)
    {
        $category = JournalCategory::where( 'slug' , $request->slug )->firstOrFail();

        $labels         = $request->labels;
        $category_slug  = str_slug($labels);

        $status = true;

        $i = 1;
        if ($category->labels != $request->labels) {
            while ( ( JournalCategory::where( 'slug' , $category_slug )->count() ) > 0 ) {
                $category_slug = str_slug( $labels . $i );

                $i++;
            }
        }

        $category->labels       = $request->labels;
        $category->description  = $request->description;
        $category->slug         = $category_slug;

        try {
            $category->save();
        } catch(\Exception $e){
            $status = false;
        }

        if ($status) {
            $request->session()->flash('msg-success', trans('alerts.journal_category_updated'));
        } else {
            $request->session()->flash('msg-failed', trans('alerts.journal_category_fail_save'));
        }

        return redirect('/backend/journal/categories/');
    }

    public function trash($slug)
    {
    	return view('backend.journals.categories.trash', [
    		'menu'        => 'journals',
    		'category'    => JournalCategory::where('slug', $slug)->firstOrFail()
    		]);
    }

    public function postTrash(Request $request)
    {
        $category = JournalCategory::where('slug', $request->slug)->first();

        JournalCategory::where('slug' , $request->slug)->delete();

        $request->session()->flash('msg-success', trans('alerts.journal_category_trash', ['title' => $category->labels]));
    	return redirect('backend/journal/categories');
    }
}
