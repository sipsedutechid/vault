<?php

namespace App\Http\Controllers\Backend\Journal;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Journal;
use App\JournalCategory;
use App\JournalFile;
use App\Article;
use App\JournalTag;
use App\Tag;

use Validator;
use File;
use Storage;

class JournalController extends Controller
{
    public function home()
    {
    	return view('backend.journals.home', [
    		'menu' 				=> 'journals',
    		'journalPublish'	=> Journal::where('post_status' , '1')->count() ,
            'journalDraft'      => Journal::where('post_status' , '0')->count() ,
            'category'          => JournalCategory::count()
    		]);
    }

    public function listAll(Request $request) {
        if ($request->ajax()) {

            $journal = Journal::where( 'id' , '<', $request->last_id )
                    ->orderBy('id' , 'desc')
                    ->take(10)
                    ->get();

            $last_journal = Journal::orderBy('id' , 'asc')
                    ->firstOrFail();

            $view = view('backend.journals.list-table', ['journals' => $journal]);

            $total_journal = count($journal);

            //($lsat_id = 0) telling the client side to hide the load more button
            //if row selected is the last row in db
            if ( $last_journal->id === $journal[ $total_journal - 1 ]->id )
                $last_id = 0 ;
            else
                $last_id = $journal[ $total_journal - 1 ]->id;

            return json_encode([
                'last_id' => $last_id ,
                'html'    => $view->render()
            ]);

        } elseif ( $request->s ) {
            $s = explode( ' ' , $request->s );

            $journal = Journal::where( function ($query) use ($s){
                        $query->where(function($query) use ($s) {
                            foreach ($s as $key) {
                                $query->orWhere('title' , 'LIKE' , '%' . $key . '%');
                            }
                        });
                    })
                    ->orderBy('id', 'desc')
                    ->get();

            $hide = true;
        } elseif ( $request->status ) {
            $status     = ($request->status == 'publish') ? '1' : '0';
            $journal    = Journal::where('post_status', $status)
                            ->orderBy('id', 'desc')
                            ->take(10)
                            ->get();
            $hide = Journal::count() <= 10;
        } else {
            $journal = Journal::orderBy('id', 'desc')
                   ->take(10)
                   ->get();

            $hide = Journal::count() <= 10;
        }

        return view('backend.journals.list', [
    		'menu'     => 'journals',
    		'journals' => $journal ,
            'hide'     => $hide
    		]);
    }

    public function create(Request $request)
    {
        $journal_cat     = JournalCategory::all();
        $journal_cat_arr = [];

        foreach ($journal_cat as $data) {
            $journal_cat_arr[$data->id] = $data->labels;
        }

    	return view('backend.journals.form', [
    		'journal'	        => new Journal ,
            'journal_category'  => $journal_cat_arr ,
            'journal_specialist'=> ['doctor' => 'General Doctor', 'dentist' => 'Dentist'] ,
    		'menu' 		        => 'journals' ,
            'request'           => $request
    		]);
    }

    public function postCreate(Request $request)
    {
        $this->validate($request, [
			"title" => "required|string|max:255"
    	]);

        $journal_slug = str_slug( $request->title );

        $i = 1;
        while ( ( Journal::where( 'slug' , $journal_slug )->count() ) > 0 ) {
            $journal_slug = str_slug( $request->title . $i );

            $i++;
        }

        $journal = new Journal;

        $journal->title                 = $request->title;
        $journal->issn                  = $request->issn;
        $journal->e_issn                = $request->e_issn;
        $journal->publisher             = $request->publisher;
        $journal->editor                = $request->editor;
        $journal->slug                  = $journal_slug;
        $journal->description           = $request->description;
        $journal->publish_date          = $request->publish_date;
        $journal->created_by            = auth()->user()->id;
        $journal->post_status           = ($request->post_action == 'save_publish') ? 1 : 0;
        $journal->journal_category_id   = $request->journal_category_id;
        $journal->is_free               = $request->is_free ?: 0;
        $journal->specialist            = $request->specialist;
        $journal->ext_url               = $request->ext_url;

        if ($request->file('image_url')) {
            $file  = ['image_url' => $request->file('image_url')];
            $rules = ['mimes' => 'jpeg,bmp,png,jpg'];

            $validator = Validator::make($file, $rules);
            if (!$validator->fails()) {

                if ($request->file('image_url')->isValid()) {
                    $extension          = $request->file('image_url')->getClientOriginalExtension();
                    $fileName           = $journal_slug.'_image.'.$extension;

                    $request->file('image_url')->move(base_path() . '/storage/app/journal_files/image', $fileName);
                    $journal->image_url = $fileName;
                }
            }
        }

        $journal->save();

        $tags = explode( "," , $request->tags);

        foreach ($tags as $tag) {

            $tag_data = Tag::firstOrCreate([
                'name' => $tag ,
                'slug' => str_slug( $tag )
            ]);

            JournalTag::create([
                'journal_id'  => $tag_data->id ,
                'tag_id'    => $journal->id
            ]);
        }

        if ($request->input('articles.*')) {
            foreach ($request->input('articles.*') as $data_article) {
                $article = Article::where( 'slug' , $data_article['slug'] )->update(['journal_id' => $journal->id , 'expire_date' => null]);
            }
        }

        $request->session()->flash('msg-success', trans('alerts.journal_saved'));

        $status = ($request->post_action == 'save_publish') ? 'publish' : 'draft';
        return redirect('/backend/journal/all?status=' . $status );
    }

    public function edit(Request $request) {
        $journal         = Journal::where( 'slug' , $request->slug )->with(['tags' , 'articles' , 'createdBy', 'category'])->firstOrFail();
        $journal_cat     = JournalCategory::all();
        $journal_cat_arr = [];
        $tags            = '';

        foreach ($journal->tags as $tag) {
            $tags .= (empty($tags) ? '' : ',') . $tag->name;
        }

        foreach ($journal_cat as $data) {
            $journal_cat_arr[$data->id] = $data->labels;
        }

    	return view('backend.journals.form', [
    		'journal'	        => $journal ,
            'journal_category'  => $journal_cat_arr ,
            'journal_specialist'=> ['doctor' => 'General Doctor', 'dentist' => 'Dentist'] ,
            'tags'              => $tags ,
    		'menu' 		        => 'journals' ,
            'request'           => $request
    		]);
    }

    public function postUpdate(Request $request) {
        $journal = Journal::where( 'slug' , $request->slug )->firstOrFail();

        $this->validate($request, [
			"title" => "required|string|max:255"
    	]);

        $i = 1;
        if ( $journal->title != $request->title ) {
            $journal_slug = str_slug( $request->title );

            while ( ( Journal::where( 'slug' , $journal_slug )->count() ) > 0 ) {
                $journal_slug = str_slug( $request->title . $i );
                $i++;
            }

            $journal->slug = $journal_slug;
        }

        $journal->title                 = $request->title;
        $journal->issn                  = $request->issn;
        $journal->e_issn                = $request->e_issn;
        $journal->publisher             = $request->publisher;
        $journal->editor                = $request->editor;
        $journal->journal_category_id   = $request->journal_category_id;
        $journal->description           = $request->description;
        $journal->is_free               = $request->is_free ?: 0;
        $journal->specialist            = $request->specialist;
        $journal->ext_url               = $request->ext_url;

        if ($request->file('image_url')) {
            $file  = ['image_url' => $request->file('image_url')];
            $rules = ['mimes' => 'jpeg,bmp,png,jpg'];

            $validator = Validator::make($file, $rules);
            if (!$validator->fails()) {

                if ($request->file('image_url')->isValid()) {
                    $extension          = $request->file('image_url')->getClientOriginalExtension();
                    $fileName           = $journal->slug.'_image.'.$extension;

                    File::delete(base_path().'storage/app/journal_files/image/' . $journal->image_url);
                    $request->file('image_url')->move(base_path() . '/storage/app/journal_files/image', $fileName);
                    $journal->image_url = $fileName;
                }
            }
        } elseif ($request->remove_pict == '1') {
            File::delete(base_path().'storage/app/journal_files/image/' . $journal->image_url);
            $journal->image_url = '';
        }

        if ($request->post_action != 'update') {
            $journal->post_status = ($request->post_action == 'update_publish') ? 1 : 0;
        }

        $journal->save();

        $tags = explode( "," , $request->tags);

        //topic sync
        $saved_tags = [];
        $data_tag = [];

        foreach ($tags as $tag) {

            $data_tag = Tag::firstOrCreate([
                'name' => $tag ,
                'slug' => str_slug( $tag )
            ]);

            JournalTag::firstOrCreate([
                'journal_id'  => $data_tag->id ,
                'tag_id'    => $journal->id
            ]);

            $saved_tags[] = $data_tag->id;
        }

        $journal->tags()->sync($saved_tags);

        $saved_articles = [];
        if ($request->input('articles.*')) {
            foreach ($request->input('articles.*') as $data_article) {
                if ( $data_article['data_status'] == 'old' )
                    array_push( $saved_articles , $data_article['slug']);
            }
        }

        Article::where( 'journal_id' , $journal->id )->whereNotIn('slug', $saved_articles);

        if ($request->input('articles.*')) {
            foreach ($request->input('articles.*') as $data_article) {
                $article = Article::where( 'slug' , $data_article['slug'] )->update(['journal_id' => $journal->id , 'expire_date' => null]);
            }
        }

        $request->session()->flash('msg-success', trans('alerts.journal_updated'));

        if ( $request->post_action == 'edit') {
            return redirect('/backend/journal/' . $topic->slug . '/edit');
        } else {
            return redirect('/backend/journal/all');
        }
    }

    public function trash($slug) {
    	return view('backend.journals.trash', [
    		'menu' => 'journals',
    		'journal' => Journal::where('slug', $slug)->firstOrFail()
    		]);
    }

    public function postTrash(Request $request) {
        $journal = Journal::where('slug', $request->slug)->firstOrFail();
        $article = Article::where('journal_id' , $journal->id)->get();
        $arrFile = [];

        if (!empty($journal->image_url)) array_push($arrFile , 'journal_files/image/' . $journal->image_url);


        foreach ($article as $data) {
            if (!empty($data->file)) array_push( $arrFile , 'journal_files/pdf/' . $data->file );
        }

        Journal::where('slug', $request->slug)->delete();
        Article::where('journal_id', $journal->id)->delete();
        JournalTag::where('journal_id', $journal->id)->delete();
        Storage::delete($arrFile);

    	$request->session()->flash('msg-success', trans('alerts.journal_trash', ['title' => $journal->title]));
    	return redirect('backend/journal/all');
    }
}
