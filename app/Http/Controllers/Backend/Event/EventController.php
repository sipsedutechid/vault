<?php

namespace App\Http\Controllers\Backend\Event;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Event;

class EventController extends Controller
{
    function __construct()
    {

    }

    public function listAll(Request $request) {
        if ($request->ajax()) {

            $events = Event::where( 'id' , '<', $request->last_id )
                    ->orderBy('id' , 'desc')
                    ->take(10)
                    ->get();

            $last_event = Event::orderBy('id' , 'asc')
                    ->firstOrFail();

            $view = view('backend.events.list-table', ['events' => $events]);

            $total_event = count($events);
            //($lsat_id = 0) telling the client side to hide the load more button
            //if row selected is the last row in db
            if ( $total_event > 0 && $last_event->id === $events[ $total_event - 1 ]->id )
                $last_id = 0 ;
            else
                $last_id = $events[ $total_event - 1 ]->id;

            return json_encode([
                'last_id' => $last_id ,
                'html'    => $view->render()
            ]);

        } elseif ( $request->s ) {
            $s = explode( ' ' , $request->s );

            $events = Event::where( function ($query) use ($s){
                        $query->where(function($query) use ($s) {
                            foreach ($s as $key) {
                                $query->orWhere('name' , 'LIKE' , '%' . $key . '%');
                            }
                        });
                    })
                    ->orderBy('id', 'desc')
                    ->get();

            $loadMore = false;
        } else {
            $events = Event::orderBy('id', 'desc')
                   ->take(10)
                   ->get();

            $loadMore = Event::count() >= 10;
        }

        return view('backend.events.list', [
            'menu'      => 'events' ,
            'events'    => $events ,
            'loadMore'      => $loadMore
        ]);
    }

    public function home(Request $request)
    {
        return view('backend.events.home', [
    		'menu' 				=> 'events' ,
            'recent_events'     => Event::orderBy('created_at', 'desc')->take(5)->get() ,
            'event_count'       => Event::count()
        ]);
    }

    public function create(Request $request)
    {
        return view('backend.events.form', [
    		'menu' => 'events' ,
            'event' => new Event
        ]);
    }

    public function postCreate(Request $request) {
        $this->validate($request, [
            "name" 			             => "required|string|max:255" ,
            "organizer"                  => "required|string|max:255" ,
            "description"                => "string"
            ]);

        $event_slug = str_slug( $request->name );
        $matching_slug = Event::where( 'slug' , 'like', $event_slug . '%' )->orderBy('slug', 'desc')->first();
        $slug_count = str_replace($event_slug, '', $matching_slug);

        if ($slug_count) $event_slug .= $slug_count + 1;

        $event = new Event;

        $event->name            = $request->name;
        $event->organizer       = $request->organizer;
        $event->slug            = $event_slug;
        $event->desc            = $request->desc;
        $event->location        = $request->location;
        $event->long            = $request->long;
        $event->lat             = $request->lat;
        $event->phone_number    = $request->phone_number;
        $event->phone_name      = $request->phone_name;
        $event->start_at        = $request->start_at;
        $event->end_at          = $request->end_at;

        $event->save();
        
        if ($request->post_action == 'add_other') return redirect('/backend/event/new');
        else return redirect('/backend/event/all');
    }

    public function edit(Request $request) {
        return view('backend.events.form', [
    		'menu'  => 'events' ,
            'event' => Event::where('slug', $request->slug)->first()
        ]);
    }

    public function postUpdate(Request $request) {
        $this->validate($request, [
            "name" 			             => "required|string|max:255" ,
            "organizer"                  => "required|string|max:255" ,
            "description"                => "string"
            ]);

        $event = Event::where('slug', $request->slug)->firstOrFail();

        $event_slug = str_slug( $request->name );
        $matching_slug = Event::where( 'slug' , 'like', $event_slug . '%' )
            ->where( 'id', '<>',$event->id )
            ->orderBy('slug', 'desc')
            ->first();
        $slug_count = str_replace($event_slug, '', $matching_slug);

        if ($slug_count) $event_slug .= $slug_count + 1;

        if ($request->name != $event->name) $event->slug = $event_slug;

        $event->name            = $request->name;
        $event->organizer       = $request->organizer;
        $event->desc            = $request->desc;
        $event->location        = $request->location;
        $event->long            = $request->long;
        $event->lat             = $request->lat;
        $event->phone_number    = $request->phone_number;
        $event->phone_name      = $request->phone_name;
        $event->start_at        = $request->start_at;
        $event->end_at          = $request->end_at;

        $event->save();
        if ($request->post_action == 'continue') return redirect('/backend/event/' . $event->slug . '/edit');
        else return redirect('/backend/event/all');
    }

    public function trash($slug) {
    	return view('backend.events.trash', [
    		'menu' => 'events',
    		'event' => Event::where('slug', $slug)->firstOrFail()
        ]);
    }

    public function postTrash(Request $request) {
        $event = Event::where('slug', $request->slug)->firstOrFail();
        Event::where('slug', $request->slug)->delete();

    	$request->session()->flash('msg-success', trans('alerts.event_trash', ['name' => $event->name]));
    	return redirect('backend/event/all');
    }
}
