<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Drug;

class DrugController extends Controller
{
    public function search(Request $request) {

        $drug = Drug::orderBy('name');

        if ($class = $request->class) 
            $drug = $drug->whereHas('drugClass', function ($query) use ($class)
                {
                    $query->where('code', $class);
                });
        if ($manufacturer = $request->manufacturer) 
            $drug = $drug->whereHas('drugManufacturer', function ($query) use ($manufacturer)
                {
                    $query->where('slug', $manufacturer);
                });
        if ($request->q) {
            $q = explode(' ' , $request->input( 'q' ));
            $drug = $drug->where(function ($query) use ($q) {
                $query->where(function ($query) use ($q) {
                    foreach ($q as $key) {
                        $query->orWhere('name' , 'LIKE' , '%' . $key . '%');
                    }
                });
            });
            if (!$request->manufacturer && !$request->class)
                $drug = $drug->orWhereHas('drugManufacturer', function($query) use ($q) {
                        $query->where(function ($query) use ($q) {
                            foreach ($q as $key) {
                                $query->orWhere('name' , 'LIKE' , '%' . $key . '%');
                            }
                        });
                    })->orWhereHas('drugClass', function($query) use ($q) {
                        $query->where(function ($query) use ($q) {
                            foreach ($q as $key) {
                                $query->orWhere('name' , 'LIKE' , '%' . $key . '%');
                            }
                        });
                    })
                    ->orWhereHas('drugSubclass', function($query) use ($q) {
                        $query->where(function ($query) use ($q) {
                            foreach ($q as $key) {
                                $query->orWhere('name' , 'LIKE' , '%' . $key . '%');
                            }
                        });
                    });
        }

        return $drug->paginate($request->limit ?: 30)
            ->appends(['q' => $request->q, 'class' => $request->class, 'manufacturer' => $request->manufacturer])
            ->toJson();
    }

    public function getList(Request $request, $option)
    {
        if ($option == 'alpha') {
            return Drug::orderBy('name')
                ->paginate(30)
                ->toJson();
        }

        abort(404);
    }

    public function getSingle(Request $request, $slug)
    {
        $drug = Drug::where('slug', $slug)
            ->with(['dosages', 'packagings'])
            ->firstOrFail();
        $drug->generics = $drug->drugGenerics()->with(['generic', 'drugForm'])->get();
        $moreDrugs = $drug
            ->drugManufacturer
            ->drugs()
            ->select(['name', 'slug', 'drug_class_id', 'drug_subclass_id'])
            ->orderBy('name')
            ->get();
        $return = ['drug' => $drug, 'more_drugs' => $moreDrugs];
        return json_encode($return);

    }

    public function getClassShort()
    {
        $classes = \App\DrugClass::orderBy('name')
            ->withCount(['drugs'])
            ->get();
        foreach ($classes as $class) {
            $class->drugs = $class->drugs()->select(['slug', 'name', 'drug_manufacturer_id'])->orderBy('name')->take(5)->get();
        }
        return $classes;
    }

}
