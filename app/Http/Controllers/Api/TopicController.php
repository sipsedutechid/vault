<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Topic;

class TopicController extends Controller
{
    public function getList(Request $request, $option) {
        if ( $option == 'recent' ) {
            if ( $request->last_id ) {
                $topic = Topic::where( 'id' , $request->last_id )->where('post_status', '1')->first();

                $topics = Topic::where( 'id' , '<' , $topic->id )
                    ->where('post_status', '1')
                    ->orderBy( 'release' , 'asc' )
                    ->take($request->limit ?: 10)
                    ->get();
            } else {
                $topics = Topic::orderBy( 'release' , 'asc' )
                    ->where('post_status', '1')
                    ->take($request->limit ?: 10)
                    ->get();
            }

            $last_topic = Topic::orderBy('release' , 'asc')
                    ->where('post_status', '1')
                    ->take(1)
                    ->get();

            $total_topic = count($topics);

            //($lsat_id = 0) telling the client side to hide the load more button
            //if row selected is the last row in db
            if ( $last_topic[0]->id === $topics[ $total_topic - 1 ]->id )
                $last_id = 0 ;
            else
                $last_id = $topics[ $total_topic - 1 ]->id;

            return json_encode(['last_id' => $last_id, 'topics' => $topics->toArray()]);

        } elseif ( $option == 'doctor' || $option == 'dentist' ) {

            $topics = Topic::where( 'category' , $option )
                ->where('post_status', 1)
                ->orderBy( 'release' , 'asc' )
                ->take(12)
                ->get();

            return json_encode($topics->toArray());

        } elseif ( $option == 'backnumber' ) {
            $last_topic = Topic::where('post_status', 1)
                ->orderBy( 'release' , 'asc' )
                ->skip(12)
                ->take(1)
                ->get();

            if (isset($last_topic[0])) {
                $topic_result = Topic::where( 'release' , '<' , $last_topic[0]->release )
                    ->where('post_status', 1)
                    ->orderBy( 'release' , 'asc' )
                    ->take(12)
                    ->get()
                    ->toArray();
            } else {
                $topic_result = [];
            }

            return json_encode($topic_result);

        }

        abort(404);
    }

    public function getByDisease(Request $request, $disease) {
        return Topic::whereHas('disease', function ($query) use ($disease)  {
                        $query->where('slug', $disease);
                    })->where('post_status', 1)->get()->toJson();
    }

    public function getSingle( $slug ) {
        return Topic::where( 'slug' , $slug )
            ->where('post_status', 1)
            ->firstOrFail()
            ->toJson();
    }

    public function getLatest(){
        $doctor = Topic::with(['tags'])
            ->where('category', 'doctor')
            ->where('post_status', 1)
            ->with(['tags'])
            ->orderBy('release', 'desc')
            ->take(4)
            ->get();

        $dentist = Topic::with(['tags'])
            ->where('category', 'dentist')
            ->where('post_status', 1)
            ->orderBy('release', 'desc')
            ->take(4)
            ->get();

        return json_encode([
            'doctor' => $doctor ,
            'dentist'=> $dentist
        ]);
    }
}
