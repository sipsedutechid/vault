<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\UserFcm;

class FcmController extends Controller
{
    public function save(Request $request)
    {
        $status = true;

        $userfcm = new UserFcm();

        $userfcm->token = $request->token;

        try {
            $userfcm->save();
        } catch(\Exception $e){
            $status = false;
        }

        if ($status) {
            return json_encode([
                "status"  => $status
            ]);
        } else {
            return json_encode([
                "status" => $status
            ]);
        }
    }
}
