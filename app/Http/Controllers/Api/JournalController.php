<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Journal;
use App\Article;
use App\JournalCategory;

class JournalController extends Controller
{
    public function getList(Request $request) {
        $journal = Journal::with(['category'])->where('post_status', 1);

        if ($request->abjad != null) {
            $journal->where('title', 'like', $request->abjad . '%');
        }

        if ($request->keyword != null) {
            $journal->where('title', 'like', '%' . $request->keyword . '%');
            $journal->orWhere('issn', 'like', '%' . $request->keyword . '%');
            $journal->orWhere('e_issn', 'like', '%' . $request->keyword . '%');
            $journal->orWhere('publisher', 'like', '%' . $request->keyword . '%');
            $journal->orWhere('editor', 'like', '%' . $request->keyword . '%');
            $journal->orWhereHas('articles', function ($query) use ($request){
                $query->where('author', 'like', '%' . $request->keyword . '%');
                $query->orWhere('title', 'like', '%' . $request->keyword . '%');
            });
        }

        if ($request->category != null) {
            $journal->whereHas('category', function ($query) use ($request){
                $query->where('slug', $request->category);
            });
        }

        if ($request->specialist != null) {
            $journal->where('specialist', $request->specialist);
        }

        $journal->orderBy('title');

        return $journal->paginate($request->limit)->toJson();

    }

    public function getLatest( $limit ) {
        return Journal::where('post_status', '1')
                ->orderBy('title' , 'asc')
                ->take($limit)
                ->get()
                ->toJson();
    }

    public function getSingle( $slug ) {
        return Journal::where( 'slug' , $slug )
            ->with(['tags' , 'articles' , 'createdBy'])
            ->firstOrFail()
            ->toJson();
    }

    public function getArticle($slug) {
        return Article::where( 'slug' , $slug )
            ->with('journal')
            ->firstOrFail()
            ->toJson();
    }

    public function getCategory() {
        return JournalCategory::All()->toJson();
    }
}
