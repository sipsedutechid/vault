<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Content;

class ContentController extends Controller
{
    public function getSingle(Request $request)
    {
        $content = Content::where('slug' , $request->slug)->firstOrFail();

        return json_encode($content);
    }

    public function getFree(Request $request)
    {
        $content = Content::where('is_free', 1)->where('is_media_data', 1);

        if ($request->keyword != null) {
            $content->where('title', 'like', '%' . $request->keyword . '%');
        }

        if ($request->appearance != null) {
            $content->where('appearance',$request->appearance);
        }

        if ($request->orderby != null) {

            switch ($request->orderby) {
                case 'newest'   : $content->orderBy('created_at', 'DESC');  break;
                case 'oldest'   : $content->orderBy('created_at', 'ASC');   break;
                case 'abjadasc' : $content->orderBy('title', 'ASC');        break;
                case 'abjaddesc': $content->orderBy('title', 'DESC');       break;
            }

        }

        return $content->paginate($request->limit)->toJson();
    }


}
