<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Disease;

class DiseaseController extends Controller
{
    private function parentSort( &$avadisease, $key, &$child , $dt ){
        $parent_key  = array_search($dt['parent_id'], array_column($child, 'id'));

        if ( is_int($parent_key) ) {
            array_push( $child[$parent_key]['child'] , $dt );
            unset( $avadisease[$key] );
        } else {
            $this->parentSort( $avadisease , $key , $child['child'] , $dt );
        }

    }

    public function getList(Request $request){
        $id = 0;

        if ($request->slug) {
            $ds         = Disease::where('slug', $request->slug)->first();
            $id         = $ds ? $ds->id : 0;
            $disease    = Disease::where('parent_id', $id)->orderBy('parent_id', 'ASC')->with(['topic' => function($q){
                $q->take(5);
            }])->get()->toArray();
        } else {
            $disease    = Disease::orderBy('parent_id', 'ASC')->with(['topic' => function($q){
                $q->take(5);
            }])->get()->toArray();
        }

        $avadisease = $disease;
        $rekap      = [];

        while (sizeof($avadisease)) {
            foreach ($disease as $dt) {
                $key  = array_search($dt['id'], array_column($disease, 'id'));

                $dt['child'] = [];

                if ( $dt['parent_id'] > 0 && $dt['parent_id'] != $id ) {
                    $this->parentSort( $avadisease , $key , $rekap , $dt );
                } else {
                    array_push( $rekap , $dt );
                    unset( $avadisease[$key] );
                }
            }
        }

        return json_encode($rekap);
    }
}
