<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrugClass extends Model
{
    protected $hidden = ['id', 'created_by', 'created_at', 'updated_at'];

    public function subclasses()
    {
    	return $this->hasMany('App\DrugSubclass');
    }

    public function drugs()
    {
    	return $this->hasMany('App\Drug');
    }

    public static function dropdown()
    {
    	$classes = DrugClass::orderBy('name')->get();
    	$classesArr = [];
    	foreach ($classes as $class) {
    		$classesArr[$class->id] = $class->name;
    	}
    	return $classesArr;
    }
}
