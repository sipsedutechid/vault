<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{
    protected $connection = 'mysql_accounts';
    protected $dates = ['starts_at', 'expired_at'];
    protected $fillable = ['subscription_id', 'starts_at', 'expired_at'];
    protected $hidden = ['id', 'user_id', 'subscription_id', 'created_at', 'updated_at'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function subscription()
    {
    	return $this->belongsTo('App\Subscription');
    }
}
