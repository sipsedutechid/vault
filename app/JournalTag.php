<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JournalTag extends Model
{
    public $timestamps  = false;
    protected $table    = 'journal_tag';
    protected $fillable = ['journal_id' , 'tag_id'];
}
