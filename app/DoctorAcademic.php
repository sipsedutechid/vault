<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorAcademic extends Model
{
    protected $table = 'doctor_academic';
    protected $fillable = ['doctor_id', 'school_college', 'year', 'address', 'academic_level_id'];
    protected $with = 'academicLevel';

    public function academicLevel() {
        return $this->belongsTo('App\AcademicLevel');
    }
}
