<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $connection = 'mysql_accounts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'login', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'password', 'remember_token', 'verify_token', 'is_provider', 'role_id', 'verify_expired_at', 'last_active_at', 'verified_at', 'deactivated_at', 'created_at', 'updated_at'
    ];

    public function getCreatedAtAttribute($value)
    {
        return date(date('Ymd', strtotime($value)) == date('Ymd') ? 'H:i' : 'j M y', strtotime($value));
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }
    public function userSubscriptions()
    {
        return $this->hasMany('App\UserSubscription');
    }
    public function currentSubscription()
    {
        return $this->userSubscriptions()
            ->where('expired_at', '>', time())
            ->orderBy('expired_at')
            ->first();
    }
}
