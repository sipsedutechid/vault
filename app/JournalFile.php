<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JournalFile extends Model
{
    public $timestamps  = false;
    protected $fillable = ['journal_id', 'file_name'];
}
