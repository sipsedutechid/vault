<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrugManufacturer extends Model
{
    protected $fillable = ['slug', 'name', 'created_by'];
    protected $hidden = ['id', 'created_by', 'created_at', 'updated_at'];

    public function drugs()
    {
    	return $this->hasMany('App\Drug');
    }

    public function marketedDrugs()
    {
    	return $this->hasMany('App\Drug', 'drug_marketer_id');
    }

    public function createdBy()
    {
    	return $this->belongsTo('App\User', 'created_by');
    }
}
