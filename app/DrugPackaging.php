<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrugPackaging extends Model
{
    protected $table = 'drug_packaging';
    protected $fillable = ['name', 'quantity', 'quantity_2', 'quantity_3', 'price', 'drug_form_id', 'created_by'];
    protected $hidden = ['id', 'created_by', 'created_at', 'updated_at', 'drug_id', 'drug_form_id'];
    protected $with = ['drugForm'];
    protected $appends = ['price_label'];

    public function drugForm()
    {
    	return $this->belongsTo('App\DrugForm');
    }

    public function getPriceLabelAttribute()
    {
        return number_format($this->price, 0, ',', '.');
    }
}
