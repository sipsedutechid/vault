<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    public function getCreatedAtAttribute($value) {
        return date('Ymd', strtotime($value)) == date('Ymd') ? date('H:i', strtotime($value)) : date('j M y', strtotime($value));
    }
}
