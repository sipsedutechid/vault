<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DrugDisease extends Model
{

	use SoftDeletes;
	protected $appends = ['deleted_at_label'];
	protected $dates = ['deleted_at'];
	protected $with = ['drugSubclasses'];
	
    public function drugSubclasses()
    {
    	return $this->belongsToMany('App\DrugSubclass');
    }

    public function getDeletedAtLabelAttribute()
    {
    	if ($this->deleted_at == null) return $this->deleted_at;
    	return date(date('Ymd', strtotime($this->deleted_at)) == date('Ymd') ? 'H:i' : 'j M Y', strtotime($this->deleted_at));
    }
}
