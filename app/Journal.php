<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    // Relations

	public function articles()
	{
        return $this->hasMany('App\Article');
    }

	public function createdBy()
	{
		return $this->belongsTo('App\User', 'created_by');
	}

	public function category()
	{
		return $this->belongsTo('App\JournalCategory', 'journal_category_id');
	}

    // Mutators

    public function getCreatedAtAttribute($value)
    {
    	return date(date('Ymd') == date('Ymd', strtotime($value)) ? 'H:i' : 'j M y', strtotime($value));
    }

    // Custom methods

    public static function latest($count = 15)
    {
    	return Journal::orderBy('created_at', 'desc')->take($count)->get();
    }

	public function tags() {
        return $this->belongsToMany('App\Tag' , 'journal_tag');
    }
}
