<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Generic extends Model
{

    use SoftDeletes;
    protected $fillable = ['slug', 'name', 'created_by'];
    protected $hidden = [
        'id',
        'parent_id',
        'created_by',
        'created_at',
        'updated_at',
        'deleted_at',
        ];
    protected $dates = ['deleted_at'];

    public function drugs()
    {
    	return $this->belongsToMany('App\Drug')->withPivot('quantity', 'unit');
    }

    public function drugGenerics()
    {
        return $this->hasMany('App\DrugGeneric');
    }

    public function parentGeneric()
    {
        return $this->belongsTo('App\Generic', 'parent_id');
    }

    public function drugSubclasses()
    {
        return $this->belongsToMany('App\DrugSubclass');
    }

    public function derivatives()
    {
        return $this->hasMany('App\Generic', 'parent_id', 'id');
    }

    public function createdBy()
    {
    	return $this->belongsTo('App\User', 'created_by');
    }

    public function getCreatedAtAttribute($value)
    {
    	return date('Ymd') == date('Ymd', strtotime($value)) ? date('H:i', strtotime($value)) : date('j M y', strtotime($value));
    }
}
