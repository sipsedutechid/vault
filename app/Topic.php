<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $connection = 'mysql';
    public $timestamps = false;
    public $with = ['createdBy' , 'lectures', 'content', 'faq'];

    public function createdBy() {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function lectures() {
        return $this->belongsToMany('App\Lecture', 'topic_lecture');
    }

    public function content() {
        return $this->belongsToMany('App\Content', 'topic_content')->orderBy('order', 'ASC');
    }

    public function disease() {
        return $this->belongsToMany('App\Disease', 'topic_disease');
    }

    public function faq() {
        return $this->hasMany('App\TopicFaq');
    }

    public function tags() {
        return $this->belongsToMany('App\Tag' , 'topic_tag');
    }

    public function getCreatedAtAttribute($value) {
        return date('Ymd', strtotime($value)) == date('Ymd') ? date('H:i', strtotime($value)) : date('j M y', strtotime($value));
    }

    public function sponsor() {
        return $this->morphMany( 'App\SponsorContent', 'content', 'content_type', 'content_id' );
    }

}
