<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ability extends Model
{

    protected $connection = 'mysql_accounts';

    public function roles()
    {
    	return $this->belongsToMany('App\Role');
    }
    
}
