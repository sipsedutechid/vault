<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopicTag extends Model
{
    public $timestamps  = false;
    protected $table    = 'topic_tag';
    protected $fillable = ['topic_id' , 'tag_id'];
}
