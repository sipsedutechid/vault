<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrugSubclass extends Model
{
    protected $fillable = ['code', 'name', 'created_by'];
    protected $hidden = ['drug_class_id', 'created_by', 'created_at', 'updated_at'];

    public function drugs()
    {
        return $this->hasMany('App\Drug');
    }

    public static function dropdown($drugClass = null)
    {
    	if ($drugClass == null)
    		$subclasses = DrugSubclass::all();
    	else $subclasses = DrugSubclass::where('drug_class_id', $drugClass)->get();

    	$subclassArr = [];
    	foreach ($subclasses as $subclass) {
    		$subclassArr[$subclass['id']] = $subclass['name'];
    	}
    	return $subclassArr;
    }
}
