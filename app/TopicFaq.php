<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopicFaq extends Model
{
    protected $fillable = ['topic_id', 'question' , 'answer'];
    public $timestamps  = false;
}
