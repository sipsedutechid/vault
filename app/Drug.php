<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drug extends Model
{

    protected $with = ['drugManufacturer', 'drugMarketer', 'drugClass', 'drugSubclass'];
    protected $hidden = [
        'id', 'drug_class_id', 'drug_subclass_id', 'drug_manufacturer_id', 'drug_marketer_id', 'updated_at', 'created_by'
    ];

    public function drugManufacturer()
    {
        return $this->belongsTo('App\DrugManufacturer');
    }

    public function drugMarketer()
    {
        return $this->belongsTo('App\DrugManufacturer', 'drug_marketer_id');
    }

    public function drugClass()
    {
        return $this->belongsTo('App\DrugClass');
    }

    public function drugSubclass()
    {
        return $this->belongsTo('App\DrugSubclass');
    }

    public function drugGenerics()
    {
        return $this->hasMany('App\DrugGeneric');
    }
    public function generics()
    {
        return $this->belongsToMany('App\Generic')->withPivot('drug_form_id', 'quantity', 'unit');
    }

    public function drugForms()
    {
    	return $this->belongsToMany('App\DrugForm', 'drug_drug_forms');
    }

    public function dosages()
    {
    	return $this->hasMany('App\DrugDosage');
    }

    public function packagings()
    {
    	return $this->hasMany('App\DrugPackaging');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function getCreatedAtAttribute($value)
    {
        return date('Ymd', strtotime($value)) == date('Ymd') ? date('H:i', strtotime($value)) : date('j M y', strtotime($value));
    }
}
