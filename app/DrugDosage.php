<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrugDosage extends Model
{
    protected $fillable = ['name', 'type', 'description', 'drug_id', 'drug_form_id'];
    protected $hidden = [
    	'id',
    	'drug_form_id',
    	'drug_id',
    	'created_at',
    	'updated_at',
    	];
    protected $with = ['drugForm'];

    public function drugForm()
    {
    	return $this->belongsTo('App\DrugForm');
    }
}
