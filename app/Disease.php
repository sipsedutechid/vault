<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disease extends Model
{
    public $with = ['topic' , 'parent'];
    public $timestamps  = false;

    public function topic() {
        return $this->belongsToMany('App\Topic', 'topic_disease');
    }

    public function parent() {
        return $this->belongsTo('App\Disease', 'parent_id' , 'id');
    }
}
