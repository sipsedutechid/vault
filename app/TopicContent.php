<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopicContent extends Model
{
    protected $table    = 'topic_content';
    protected $fillable = ['topic_id' , 'content_id'];
    public $timestamps  = false;
}
