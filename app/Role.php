<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    protected $connection = 'mysql_accounts';

	public function abilities()
	{
		return $this->belongsToMany('App\Ability', 'role_abilities');
	}
    public function users()
    {
    	return $this->hasMany('App\User');
    }

}
