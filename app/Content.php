<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    //
    use SoftDeletes;
    protected $fillable = ['title', 'slug'];
    protected $dates = ['deleted_at'];

    public function level() {
        return $this->belongsTo('App\ContentLevel', 'level_id');
    }
}
