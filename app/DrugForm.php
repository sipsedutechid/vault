<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrugForm extends Model
{
    protected $fillable = ['slug', 'name'];
    protected $hidden = ['id', 'created_at', 'updated_at'];
}
