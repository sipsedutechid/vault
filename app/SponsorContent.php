<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SponsorContent extends Model
{
    use SoftDeletes;

    protected $table = 'sponsor_content';

    public function content()
    {
        return $this->morphTo(null, 'content_type', 'content_id');
    }
}
