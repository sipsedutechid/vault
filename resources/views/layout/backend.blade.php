<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<title>GakkenVAULT Backend | @yield('title')</title>

		<link rel="stylesheet" href="{{ asset('libs/bootstrap/css/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('assets/css/default.css') }}" media="screen" />
		<script type="text/javascript" src="{{ asset('libs/jquery-ui/external/jquery/jquery.js') }}"></script>
		<script type="text/javascript" src="{{ asset('libs/bootstrap/js/bootstrap.min.js') }}"></script>

		@yield('headscript')
	</head>

	<body>

		<div class="sidebar">
			<div class="logo">GakkenVAULT</div>

			<ul class="nav nav-pills nav-stacked">
				<li class="@if($menu == 'dashboard') active @endif"><a href="/backend"><b class="glyphicon glyphicon-stats"></b>&emsp;{{ trans('app.menu_dashboard') }}</a></li>
				<li class="@if($menu == 'journals') active @endif"><a href="/backend/journals"><b class="glyphicon glyphicon-book"></b>&emsp;{{ trans('app.menu_journals') }}</a></li>
				@can('general', 'vault.drugs.view')
					<li class="@if($menu == 'drugs') active @endif"><a href="/backend/drugs"><b class="glyphicon glyphicon-erase"></b>&emsp;{{ trans('app.menu_drugs') }}</a></li>
				@endcan
				<li class="@if($menu == 'doctors') active @endif"><a href="/backend/doctors"><b class="glyphicon glyphicon-plus"></b>&emsp;{{ trans('app.menu_doctors') }}</a></li>
				<li class="@if($menu == 'hospitals') active @endif"><a href="/backend/hospitals"><b class="glyphicon glyphicon-bed"></b>&emsp;{{ trans('app.menu_hospitals') }}</a></li>
				<li class="@if($menu == 'topics') active @endif"><a href="/backend/topics"><b class="glyphicon glyphicon-education"></b>&emsp;{{ trans('app.menu_topics') }}</a></li>
				<li class="@if($menu == 'events') active @endif"><a href="/backend/events"><b class="glyphicon glyphicon-calendar"></b>&emsp;{{ trans('app.menu_events') }}</a></li>
				<li class="@if($menu == 'jobs') active @endif"><a href="/backend/jobs"><b class="glyphicon glyphicon-briefcase"></b>&emsp;{{ trans('app.menu_jobs') }}</a></li>
				{{-- <li class="@if($menu == 'users') active @endif"><a href="/backend/users"><b class="glyphicon glyphicon-user"></b>&emsp;{{ trans('app.menu_users') }}</a></li> --}}
			</ul>
			<div class="footer small text-muted">
				<p>{{ trans('app.language') }}:
					@if (App::getLocale() != 'en') <a href="?lang=en">EN</a>@else EN @endif /
					@if (App::getLocale() != 'id') <a href="?lang=id">ID</a>@else ID @endif
				</p>
				<p>
					&copy; 2016. All rights reserved.<br />
					PT. Gakken Health and Education Indonesia
				</p>
			</div>
		</div>

		<div class="main-container container-fluid">
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="container-fluid">
					<div class="navbar-left">
						@yield('nav_content')
					</div>

					<div class="navbar-right">
						<div class="navbar-text">
							<div class="dropdown">
								<a href="#" data-toggle="dropdown" class="dropdown-toggle">{{ auth()->user()->name }} <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="{{ env('ACCOUNTS_LOGOUT_URL', '/logout') }}">{{ trans('app.sign_out') }}</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			@include ('layout.alert')

			@yield ('content')
		</div>

		@yield('modals')

		@yield('endscript')

	</body>

</html>
