@if (count($errors) > 0)
	<div class="alert alert-danger">{{ $errors->first() }}</div>
@elseif (Request::session()->get('msg-success', 'undefined') != 'undefined')
	<div class="alert alert-success">{{ Request::session()->get('msg-success', 'undefined') }}</div>
@endif