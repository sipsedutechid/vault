<html lang="en-US">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
		<title>{{ trans('app.title') }} @yield('title')</title>

		<link rel="stylesheet" type="text/css" href="{{ asset('libs/bootstrap/css/bootstrap.min.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend.css') }}" media="screen" />
		<script type="text/javascript" src="{{ asset('libs/jquery-ui/external/jquery/jquery.js') }}"></script>
		<script type="text/javascript" src="{{ asset('libs/bootstrap/js/bootstrap.min.js') }}"></script>
		
		@yield ('hscripts')

	</head>

	<body>

		<div class="navbar navbar-default navbar-static-top">
			<div class="container">
				<div class="navbar-header">
					<div class="navbar-brand">
						<h1>Gakken <strong>Directory</strong></h1>
					</div>
				</div>
				<div class="navbar-right">
					<div class="navbar-text">
						<a href="https://gakken-idn.id/magento/index.php/customer/account/login">Log In</a>&ensp;or
					</div>
					<a href="https://gakken-idn.id/magento/index.php/customer/account/create/" class="navbar-btn btn btn-success">Sign Up</a>
				</div>
			</div>
		</div>

		@yield ('content')

		<section class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<p><img src="{!! asset('assets/images/logo-full-web65-mono-light.png') !!}" /></p>
						<p>&copy; 2016. PT. GAKKEN HEALTH AND EDUCATION INDONESIA.<br />All rights reserved.</p>
					</div>
				</div>
			</div>
		</section>

		@yield ('fscripts')

	</body>

</html>