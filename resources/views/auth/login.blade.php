<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<title>GakkenVAULT Backend | {{ trans('auth.sign_in') }}</title>

		<link rel="stylesheet" href="{{ asset('libs/bootstrap/css/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('assets/css/default.css') }}" media="screen" />
		<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
		<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="{{ asset('libs/bootstrap/js/bootstrap.min.js') }}"></script>

	</head>
	<body>
		
		<div class="container" style="margin-top: 60px;">
			<div class="row">
				<div class="col-lg-4 col-md-6">
					<h1>{{ trans('auth.sign_in') }}</h1>
					<p style="margin-bottom:15px;"><strong>GakkenVAULT Backend Area</strong><br />{{ trans('auth.pre_warning') }}</p>

					@if ($errors->first())
						<div class="alert alert-danger">{{ $errors->first() }}</div>
					@endif

					{!! Form::open(['url' => 'backend/sign-in']) !!}

						{!! Form::text('email', null, ['class' => 'form-control seamless', 'placeholder' => trans('auth.username'), 'autofocus' => 'autofocus']) !!}
						{!! Form::password('password', ['class' => 'form-control seamless', 'placeholder' => trans('auth.password')]) !!}

						<div class="form-group">
							<div class="checkbox">
								<label><input type="checkbox" name="remember_me" /> {{ trans('auth.remember_me') }}</label>
							</div>
						</div>

						<div class="form-group" style="margin-top: 30px;">
							<button type="submit" class="btn btn-primary">{{ trans('auth.sign_in') }}</button>
						</div>

						<div style="margin-top:45px;">
							<p>{{ trans('app.language') }}: 
								@if (App::getLocale() != 'en') <a href="?lang=en">EN</a>@else EN @endif / 
								@if (App::getLocale() != 'id') <a href="?lang=id">ID</a>@else ID @endif
							</p>
							<p class="small text-muted">&copy; 2015. All rights reserved.<br />PT. GAKKEN HEALTH AND EDUCATION INDONESIA</p>
						</div>

					{!! Form::close() !!}
				</div>
			</div>
		</div>

	</body>

</html>