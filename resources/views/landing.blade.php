@extends ('layout.frontend')

@section ('content')

	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-4 col-xs-6">
					<a href="/drugs" class="thumbnail thumbnail-menu">
						<img src="{{ asset('assets/images/menu-drugs.png') }}" width="150" />
						<div class="caption text-center">
							<div class="h2">Drugs</div>
							<p>
								Explore from commonly-used to special drugs and all its details
							</p>
						</div>
					</a>
				</div>
				<div class="col-md-3 col-sm-4 col-xs-6">
					<div class="thumbnail thumbnail-menu disabled">
						<img src="{{ asset('assets/images/menu-journals.png') }}" width="150" />
						<div class="caption text-center">
							<div class="h2">Journals</div>
							<p>
								Read and download all the latest medical journals and articles
							</p>
							<em><div class="h4 coming-soon">Coming Soon!</div></em>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-4 col-xs-6">
					<div class="thumbnail thumbnail-menu disabled">
						<img src="{{ asset('assets/images/menu-doctors.png') }}" width="150" />
						<div class="caption text-center">
							<div class="h2">Doctors</div>
							<p>
								Find experienced doctors across south and west Sulawesi
							</p>
							<em><div class="h4 coming-soon">Coming Soon!</div></em>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-4 col-xs-6">
					<div class="thumbnail thumbnail-menu disabled">
						<img src="{{ asset('assets/images/menu-hospitals.png') }}" width="150" />
						<div class="caption text-center">
							<div class="h2">Hospitals</div>
							<p>
								Look for medical facilities across south and west Sulawesi
							</p>
							<em><div class="h4 coming-soon">Coming Soon!</div></em>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</section>

@endsection