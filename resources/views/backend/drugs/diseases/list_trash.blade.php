@extends('layout.backend')

@section('title')
	Drug Diseases (Trash)
@stop

@section('nav_content')
	<div class="navbar-text navbar-back">
		<a href="/backend/drug/diseases" title="Back to Drug Diseases List">
			<b class="glyphicon glyphicon-chevron-left"></b>
		</a>
	</div>
	<div class="navbar-text navbar-title">Drug Diseases (Trash)</div>
	<a href="#" class="btn navbar-btn" data-toggle="modal" data-target="#deleteModal"><b class="glyphicon glyphicon-trash"></b>&ensp;Empty Trash</a>
@stop

@section('content')
	
	@if (count($diseases))
		
		<div class="panel panel-default">
			<table class="table table-hover">
				<thead>
					<tr><th>Disease</th><th width="200">Trashed on</th><th width="200"></th></tr>
				</thead>
				<tbody>
					@foreach ($diseases as $dis)
						<tr>
							<td>{{ $dis->name }}</td>
							<td>{{ $dis->deleted_at_label }}</td>
							<td><a href="/backend/drug/disease/{{ $dis->slug }}/restore">Restore</a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>

	@endif

@stop

@section('modals')
	<div class="modal fade" id="deleteModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					@if (count($diseases))
						{!! Form::open(['url' => '/backend/drug/disease/trash', 'method' => 'delete']) !!}
						<h2>Empty Trash</h2>
						<p>This action will permanently erase all drug diseases and its mappings. Are you sure you want to empty trash? This action cannot be undone.</p>

						<div class="text-right">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-danger">Empty Trash</button>
						</div>
						{!! Form::close() !!}
					@else
						<h2>Trash is Empty</h2>
						<p>There is currently nothing to be deleted</p>
						<div class="text-right">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>
@stop