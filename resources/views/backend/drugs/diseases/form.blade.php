@extends('layout.backend')

@section('title')
	Drug Disease
@stop

@section('nav_content')
	<div class="navbar-text navbar-back">
		<a href="/backend/drug/diseases" title="Back to Drug Diseases List">
			<b class="glyphicon glyphicon-chevron-left"></b>
		</a>
	</div>
	<div class="navbar-text navbar-title">Drug Disease</div>
@stop

@section('content')

	<div class="row">
		<div class="col-lg-6">
				
			{!! Form::model($disease, ['url' => '/backend/drug/disease/' . $disease->slug, 'class' => 'form-horizontal', 'id' => 'mainForm']) !!}
				{!! Form::hidden('slug', $disease->slug) !!}
				<div class="form-group">
					<label class="control-label col-sm-3">Name</label>
					<div class="col-sm-9">
						{!! Form::text('name', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) !!}
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3">Mapped Subclasses</label>
					<div class="col-sm-9">
						@foreach ($classes as $class)
							@if (count($class->subclasses))
								<div class="h5"><a href="#" data-toggle="subclass">{{ $class->name }}</a></div>
								<ul class="list-unstyled hidden" style="margin-left:15px;margin-bottom:15px;">
									@foreach ($class->subclasses as $dsc)
										<li>
											<label>{!! Form::checkbox('drugSubclasses[]', $dsc->id, null) !!} {{ $dsc->name }}</label>
										</li>
									@endforeach
								</ul>
							@endif
						@endforeach
					</div>
				
				</div>

				<div>
					<button type="submit" class="btn btn-primary">Save</button>
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Delete Disease</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>

@stop

@section('modals')
	
	<div class="modal fade" id="deleteModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					{!! Form::open(['url' => '/backend/drug/disease/' . $disease->slug, 'method' => 'delete']) !!}
					<h2>Confirm Delete</h2>
					<p>
						Are you sure to delete the drug disease {{ $disease->name }}?
					</p>
					<div class="text-right">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-danger">Confirm Delete</button>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@stop

@section('endscript')
	<script type="text/javascript">
		$(document).ready(function () {
			$("[name=name]").blur(function () {
				$.get('/service/slugger', {string: $(this).val()})
					.done(function (r) {
						$("[name=slug]").val(r);
					});
			});
			$("[data-toggle=subclass]").on('click', function (e) {
				e.preventDefault();
				$(this).parent().next('.list-unstyled').toggleClass('hidden');
				$(this).blur();
			});
		});

	</script>
@stop