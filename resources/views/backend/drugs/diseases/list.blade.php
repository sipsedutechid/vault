@extends('layout.backend')

@section('title')
	Drug Diseases
@stop

@section('nav_content')
	<div class="navbar-text navbar-title">Drug Diseases</div>
	<a href="/backend/drug/disease/new" class="btn navbar-btn"><b class="glyphicon glyphicon-plus"></b>&ensp;Add New</a>
	<a href="/backend/drug/disease/trash" class="btn navbar-btn"><b class="glyphicon glyphicon-trash"></b>&ensp;Trashed</a>
@stop

@section('content')
	
	@if (count($diseases))
		
		<div class="panel panel-default">
			<table class="table table-hover">
				<thead>
					<tr><th>Disease</th><th>Mapped to</th></tr>
				</thead>
				<tbody>
					@foreach ($diseases as $dis)
						<tr>
							<td><a href="/backend/drug/disease/{!! $dis->slug !!}">{{ $dis->name }}</a></td>
							<td>
								@if (count($dis->drugSubclasses))
									@foreach($dis->drugSubclasses as $index => $sc)
										{{ $sc->name }}{{ isset($dis->drugSubclasses[$index+1]) ? "; " : '' }}
									@endforeach
								@else
									N/A
								@endif
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>


	@endif

@stop