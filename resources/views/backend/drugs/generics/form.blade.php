@extends('layout.backend')

@section('title') {{ trans('drugs.generic.form.' . ($generic->id ? 'edit' : 'add')) }} @endsection

@section('headscript')

	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.structure.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.theme.min.css') }}">

@endsection

@section('nav_content')

	<div class="navbar-text navbar-back">
		<a href="{!! Request::input('redirect') ? Request::input('redirect') : '/backend/drug/generics' !!}" title="Back to Generics List"><b class="glyphicon glyphicon-chevron-left"></b></a>
	</div>
	<div class="navbar-text navbar-title">{{ trans('drugs.generic.form.' . ($generic->id ? 'edit' : 'add')) }}</div>

@endsection

@section('content')

	{!! Form::model($generic, ['url' => '/backend/drug/generic/' . ($generic->id ? $generic->slug . '/edit' : 'add'), 'class' => 'form-horizontal']) !!}
	{!! Form::hidden('slug') !!}
	<div class="row">
		<div class="col-sm-8">
			<div class="form-group">
				<div class="col-xs-12">
					<div class="input-group">
						{!! Form::text('name', null, ['class' => 'form-control input-lg', 'placeholder' => trans('drugs.generic.name'), 'autofocus' => 'autofocus']) !!}
						<div class="input-group-btn">
							<label class="btn btn-default btn-lg">
								{!! Form::checkbox('is_generic', 1, null, ['autocomplete' => 'off']) !!} {{ trans('drugs.generic.form.is_generic') }}
						  	</label>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading"><div class="panel-title h4">{{ trans('drugs.generic.form.section_mechanism') }}</div></div>
				<div class="panel-body">
					<div class="form-group">
						<label class="control-label col-sm-2">{{ trans('drugs.generic.form.alias') }}</label>
						<div class="col-sm-10">
							{!! Form::text('alias', null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2">{{ trans('drugs.generic.form.description') }}</label>
						<div class="col-sm-10">
							{!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => 3]) !!}
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2">{{ trans('drugs.generic.form.onset') }}</label>
						<div class="col-sm-10">
							{!! Form::textarea('onset', null, ['class' => 'form-control', 'rows' => 3]) !!}
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2">{{ trans('drugs.generic.form.duration') }}</label>
						<div class="col-sm-10">
							{!! Form::textarea('duration', null, ['class' => 'form-control', 'rows' => 3]) !!}
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading"><div class="panel-title h4">{{ trans('drugs.generic.form.section_pkinetics') }}</div></div>
				<div class="panel-body">
					<div class="form-group">
						<label class="control-label col-sm-2">{{ trans('drugs.generic.form.absorption') }}</label>
						<div class="col-sm-10">
							{!! Form::textarea('absorption', null, ['class' => 'form-control', 'rows' => 3]) !!}
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2">{{ trans('drugs.generic.form.distribution') }}</label>
						<div class="col-sm-10">
							{!! Form::textarea('distribution', null, ['class' => 'form-control', 'rows' => 3]) !!}
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2">{{ trans('drugs.generic.form.metabolism') }}</label>
						<div class="col-sm-10">
							{!! Form::textarea('metabolism', null, ['class' => 'form-control', 'rows' => 3]) !!}
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2">{{ trans('drugs.generic.form.excretion') }}</label>
						<div class="col-sm-10">
							{!! Form::textarea('excretion', null, ['class' => 'form-control', 'rows' => 3]) !!}
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<div class="col-xs-12">
					@if ($generic->id)
						<button type="submit" name="post_action" value="list" class="btn btn-primary btn-block">{{ trans('drugs.generic.form.update') }}</button>
						<button type="submit" name="post_action" value="edit" class="btn btn-default btn-block">{{ trans('drugs.generic.form.update_and_continue') }}</button>
						<button type="button" id="mergeBtn" data-toggle="modal" data-target="#mergeModal" class="btn btn-default btn-block">{{ trans('drugs.generic.form.merge') }}</button>
					@endif
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading"><div class="panel-title h4">{{ trans('drugs.generic.form.section_details') }}</div></div>
				<div class="panel-body">
					<div class="form-group">
						{!! Form::hidden('parent_id') !!}
						<label class="control-label col-sm-4">{{ trans('drugs.generic.form.parent') }}</label>
						<div class="col-sm-8">
							{!! Form::text('parent', $generic->parent_id ? $generic->parentGeneric->name : null, ['class' => 'form-control', 'data-target' => 'parent_id']) !!}
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-default" id="drugClass">
				<div class="panel-heading"><div class="panel-title h4">{{ trans('drugs.generic.form.section_subclasses') }}</div></div>
				<div class="panel-body">

					<input type="hidden" name="drug_subclass_id_temp" />
					<div class="input-group">
						<input type="text" name="drug_subclass" class="form-control" data-target="drug_subclass_id_temp" placeholder="{{ trans('drugs.generic.form.search_subclasses') }}" />
						<span class="input-group-btn">
							<button type="button" id="addSubclassBtn" class="btn btn-default" disabled="disabled">{{ trans('drugs.generic.form.add_subclass') }}</button>
						</span>
					</div>

				</div>
				<table class="table table-condensed" id="subclassesList">
					<thead><tr><th colspan="2">{{ trans('drugs.generic.form.subclasses_list') }}</th></tr></thead>
					<tbody>
						@if ($generic->drugSubclasses->count() == 0)
							<tr><td colspan="2">{{ trans('drugs.generic.form.subclasses_empty') }}</td></tr>
						@endif
					</tbody>
				</table>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading"><div class="panel-title h4">{{ trans('drugs.generic.form.drugs_list') }}</div></div>
				<table class="table table-condensed table-hover">
					<thead>
						<tr>
							<th>{{ trans('drugs.list_brand_name') }}</th>
							<th>{{ trans('drugs.generic.form.drugs_list_quantity') }}</th>
						</tr>
					</thead>
					<tbody>
						@foreach($generic->drugGenerics as $drugGeneric)
							<tr>
								<td>{{ $drugGeneric->drug->name }} {{ $drugGeneric->drugForm ? '(' . $drugGeneric->drugForm->name . ')' : '' }}</td>
								<td>
									@if($drugGeneric->quantity) 
										{{ $drugGeneric->quantity }} {{ $drugGeneric->unit }}
									@else {{ trans('drugs.form_null') }} 
									@endif
								</td>
							</tr>
						@endforeach
					</tbody>

				</table>
			</div>
		</div>

	</div>

	{!! Form::close() !!}

@endsection

@section('modals')
	<div class="modal fade" id="mergeModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					{!! Form::open(['url' => '/backend/drug/generic/' . $generic->slug . '/merge']) !!}
						<h3>{{ trans('drugs.generic.form.merge') }}</h3>
						<p>{{ trans('drugs.generic.form.merge_help') }}</p>

						{!! Form::hidden('merge_generic') !!}
						<div class="form-group">
							{!! Form::text('merge_generic_name', null, ['placeholder' => trans('drugs.generic.list.search'), 'class' => 'form-control', 'data-target' => 'merge_generic']) !!}
						</div>
						<div class="form-group text-right">
							<a class="btn" data-dismiss="modal">{{ trans('drugs.generic.form.merge_cancel') }}</a>
							<button type="submit" class="btn btn-primary">{{ trans('drugs.generic.form.merge_save') }}</button>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection

@section('endscript')

	<script type="text/javascript" src="{{ asset('libs/jquery-ui/jquery-ui.min.js') }}"></script>
	<script type="text/javascript">
		var subclasses = [
			@foreach($generic->drugSubclasses as $subclass)
				{id: {{ $subclass->id}}, name: "{{ $subclass->name }}" },
			@endforeach
		];

		$(document).ready(function () {
			@if (!$generic->is_generic)
				$("#drugClass").addClass('hidden');
			@endif
			@if ($generic->drugSubclasses->count() > 0)
				refreshSubclasses();
			@endif
			$("[name=name]").blur(function (e) {
				$.get('/service/slugger', {string: $(this).val()}).done(function (r) {
					$("[name=slug]").val(r);
				});
			});
			$("[name=is_generic]").on('change', function () {
				$("#drugClass").toggleClass('hidden', !$(this).is(":checked"));
			});

			$("[name=parent], [name=merge_generic_name]").autocomplete({
				source: '/service/list/generics',
				minLength: 2,
				response: function (e, ui) {
					$.each(ui.content, function (index, item) {
						item.value = item.id;
						item.label = item.name;
					});
				},
				focus: function (e, ui) {
					$(this).val(ui.item.label);
					return false;
				},
				select: function (e, ui) {
					$("[name=" + $(this).data('target') + "]").val(ui.item.value);
					$(this).val(ui.item.label);
					return false;
				}
			}).on('input change paste', function (e) {
				$("[name=" + $(this).data('target') + "]").val('');
			});

			$("[name=drug_subclass]").autocomplete({
				source: '/service/list/drug-subclasses',
				minLength: 2,
				response: function (e, ui) {
					$.each(ui.content, function (index, item) {
						item.value = item.id;
						item.label = item.name;
					});
				},
				focus: function (e, ui) {
					$(this).val(ui.item.label);
					return false;
				},
				select: function (e, ui) {
					$("[name=" + $(this).data('target') + "]").val(ui.item.value);
					$(this).val(ui.item.label);
					$("#addSubclassBtn").removeAttr('disabled').focus();
					return false;
				}
			}).on('input change paste', function (e) {
				$("[name=" + $(this).data('target') + "]").val('');
				$("#addSubclassBtn").attr('disabled', 'disabled');
			});

			$("#addSubclassBtn").click(function () {
				subclasses.push({id: $("[name=drug_subclass_id_temp]").val(), name: $("[name=drug_subclass]").val()});
				$("[name=drug_subclass], [name=drug_subclass_id_temp]").val('');
				refreshSubclasses();
				$(this).attr('disabled', 'disabled');
				$("[name=drug_subclass]").focus();
			});
			$("#subclassesList").delegate('[data-action=remove]', 'click', function (e) {
				e.preventDefault();
				for (var i = subclasses.length - 1; i >= 0; i--) {
					if (subclasses[i].id == $(this).data('index'))
						subclasses.splice(i, 1);
				}
				refreshSubclasses();
			});

		});

		function refreshSubclasses() {
			$("#subclassesList tbody").html('');
			if (subclasses.length)
				for (var i = subclasses.length - 1; i >= 0; i--) {
					var row = $("<tr />")
						.append($("<td />")
							.text(subclasses[i].name)
							)
						.append($("<td />")
							.css({width: 50})
							.append($("<input />")
								.attr('name', 'subclasses[]')
								.attr('type', 'hidden')
								.val(subclasses[i].id)
								)
							.append($("<a />")
								.attr('href', '#')
								.attr('data-action', 'remove')
								.attr('data-index', subclasses[i].id)
								.append($("<b />").addClass('glyphicon glyphicon-remove'))
								)
							);
					$("#subclassesList tbody").append(row);
				}
			else $("#subclassesList tbody").append($("<tr />").append($("<td />").attr('colspan', 2).text('{{ trans('drugs.generic.form.subclasses_empty') }}')));
		}
	</script>

@endsection