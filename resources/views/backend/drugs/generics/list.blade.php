@extends('layout.backend')

@section('title') {{ trans('drugs.generic.list.title') }} @endsection

@section('nav_content')

	<div class="navbar-text navbar-back">
		<a href="/backend/drugs" title="{{ trans('drugs.back_dashboard') }}"><b class="glyphicon glyphicon-chevron-left"></b></a>
	</div>
	<div class="navbar-text navbar-title">{{ trans('drugs.generic.list.title') }}</div>
	{!! Form::open(['url' => 'backend/drug/generics', 'class' => 'navbar-form navbar-left','method' => 'get']) !!}
		<a href="/backend/drug/generic/new" class="btn btn-primary hidden">{{ trans('drugs.add') }}</a>
		<div class="input-group">
			{!! Form::text('s', Request::input('s'), ['class' => 'form-control', 'placeholder' => trans('drugs.generic.list.search'), 'style' => 'width:300px;']) !!}
			<span class="input-group-btn">
				<button type="submit" class="btn btn-default"><b class="glyphicon glyphicon-search"></b></button>
				@if (Request::input('s'))
					<a href="/backend/drug/generics" class="btn btn-default">{{ trans('drugs.list_search_clear') }}</a>
				@endif
			</span>
		</div>
	{!! Form::close() !!}
	

@endsection

@section('content')

	<div class="pre-table">
		{!! Form::open(['url' => 'backend/drug/bulk-action', 'class' => 'form-inline pull-right hidden']) !!}
			{{ trans('drugs.bulk_action') }}&ensp;
			<div class="input-group">
				{!! Form::select('action', ['trash' => trans('drugs.list_trash'), 'merge' => trans('drugs.generic.list_merge')], null, ['class' => 'form-control input-sm']) !!}
				<span class="input-group-btn">
					<button type="submit" class="btn btn-sm btn-default">{{ trans('drugs.list_go') }}</button>
				</span>
			</div>
		{!! Form::close() !!}
	</div>
	<div class="panel panel-default">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>{{ trans('drugs.generic.name') }}</th>
					<th>{{ trans('drugs.generic.subclass') }}</th>
					<th style="width: 150px;">{{ trans('drugs.generic.derivatives') }}</th>
					<th style="width: 150px;">{{ trans('drugs.generic.drug_count') }}</th>
					<th style="width: 150px;">{{ trans('drugs.generic.created') }}</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($generics as $generic)
					<tr>
						<td>
							<a href="/backend/drug/generic/{{ $generic->slug }}/edit?redirect={!! urlencode(Request::fullUrl()) !!}">{{ $generic->name }}</a>
							@if ($generic->is_generic)
								<span class="label label-success">{{ trans('drugs.generic.true_generic') }}</span>
							@endif
							&ensp;
							<span class="actions">
								<a href="/backend/drug/generic/{{ $generic->slug }}/trash" class="text-danger">{{ trans('drugs.trash') }}</a>
							</span>
						</td>
						<td>
							@if ($generic->drugSubclasses->count() > 0)
								@foreach($generic->drugSubclasses as $index => $subclass)
									{{ $subclass->name }} @if (isset($generic->drugSubclasses[$index+1])) / @endif
								@endforeach
							@else
								{{ trans('drugs.generic.subclass_empty') }}
							@endif
						</td>
						<td>{{ $generic->is_generic ? $generic->derivatives()->count() : "N/A" }}</td>
						<td>{{ $generic->drugs()->count() }}</td>
						<td>
							{{ $generic->created_at }}
						</td>
					</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th>{{ trans('drugs.generic.name') }}</th>
					<th>{{ trans('drugs.generic.subclass') }}</th>
					<th>{{ trans('drugs.generic.derivatives') }}</th>
					<th>{{ trans('drugs.generic.drug_count') }}</th>
					<th>{{ trans('drugs.generic.created') }}</th>
				</tr>
			</tfoot>
		</table>
	</div>

	{!! $generics->appends($appends)->links() !!}

@endsection