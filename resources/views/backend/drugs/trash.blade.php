@extends('layout.backend')

@section('title') Move {{ $drug->name }} to Trash @endsection

@section('content')

	<h1>Move to Trash</h1>
	{!! Form::open(['url' => 'backend/drug/trash']) !!}
		<p>
			You are about to move the drug <strong>{{ $drug->name }} ({{ $drug->drugManufacturer->name }})</strong> to trash.<br />
			Are you sure? This action cannot be undone.
		</p>
		{!! Form::hidden('slug', $drug->slug) !!}
		<a href="/backend/drug/all" class="btn btn-default">No, take me back</a>
		<button type="submit" class="btn btn-danger">Yes, move this drug to trash</button>

	{!! Form::close() !!}

@endsection