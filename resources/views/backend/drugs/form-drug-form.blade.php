<div class="panel panel-default">
	<div class="panel-heading">
		<div class="panel-title h4">
			{{ trans('drugs.form_panel_drug_forms') }}
			<a class="pull-right" data-toggle="collapse" href="#drugForm"><b class="glyphicon glyphicon-menu-down"></b></a>
		</div>
	</div>

	<div class="panel-body collapse in" id="drugForm">
		
		<!-- DRUG FORMS -->
		<div id="formFields" class="hidden">
			<input type="hidden" name="temp_form_id" />
			<div class="input-group">
				<input type="text" name="temp_form_name" class="form-control" placeholder="{{ trans('drugs.form_drug_form') }}" />
				<span class="input-group-btn">
					<button type="button" id="cancelDrugFormBtn" class="btn btn-danger btn-normal">{{ trans('drugs.form_cdp_cancel') }}</button>
					<button type="button" id="submitDrugFormBtn" class="btn btn-primary btn-normal">{{ trans('drugs.form_cdp_add') }}</button>
				</span>
			</div>
		</div>
		<button id="addFormBtn" type="button" class="btn btn-default">{{ trans('drugs.form_add_form') }}</button>
		<table id="drugForms" class="table table-hover table-bordered" style="margin-top:15px;">
			<thead><tr><th>{{ trans('drugs.form_panel_drug_forms') }}</th><th style="width:50px;"></th></tr></thead>
			<tbody></tbody>
		</table>

	</div>

</div>

<script type="text/javascript">

	var forms = [
	@if (old('drug_forms') || $drug->id)
		<?php $forms = old('drug_forms') ? old('drug_forms') : $drugForms ?>
		@foreach ($forms as $form)
			{drug_form_id: "{{ $form['id'] }}", name: "{{ $form['name']}}"},
		@endforeach
	@endif
	];
	$(document).ready(function () {
		renderFormsTable();

		$("#addFormBtn").click(function () { 
			$("#cancelDosageBtn").click();
			$("#cancelPackagingBtn").click();
			$("#formFields input").val(''); 
			$("#formFields").removeClass('hidden'); 
			$(this).addClass('hidden'); 
			$("[name=temp_form_name]").focus(); 
		});
		$("#cancelDrugFormBtn").click(function () { $("#formFields").addClass('hidden'); $("#addFormBtn").removeClass('hidden'); });

		$("#submitDrugFormBtn").click(function () { 
			if (!$("[name=temp_form_name]").val().length) return;
			forms.push({drug_form_id: $("[name=temp_form_id]").val(), name: $("[name=temp_form_name]").val()});
			$("#formFields").addClass('hidden'); $("#addFormBtn").removeClass('hidden');
			$("#addPackagingBtn").prop('disabled', '');
			renderFormsTable();
			$("#addFormBtn").focus();
		});
		$("#drugForms").on('click', '.remove-drug-form', function (e) {
			e.preventDefault();
			forms.splice($(this).data('index'), 1);
			for (var i = generics.length - 1; i >= 0; i--) {
				if (generics[i].form_index == $(this).data('index')) {
					generics.splice(i, 1);
				}
			};
			for (var i = dosages.length - 1; i >= 0; i--) {
				if (dosages[i].form_index == $(this).data('index')) {
					dosages.splice(i, 1);
				}
			};
			for (var i = packagings.length - 1; i >= 0; i--) {
				if (packagings[i].form_index == $(this).data('index')) {
					packagings.splice(i, 1);
				}
			};
			if (!forms.length) $("[name=addPackagingBtn]").prop('disabled', 'disabled');
			renderFormsTable();
			renderCompositionsTable();
			renderDosagesTable();
			renderPackagingsTable();
		});
		$("[name=temp_form_name]").autocomplete({
			source: '/service/list/drug-forms',
			response: function (e, ui) {
				$.each(ui.content, function (index, item) {
					item.value = item.id;
					item.label = item.name;
				});
			},
			focus: function (e, ui) {
				$(this).val(ui.item.label);
				return false;
			},
			select: function (e, ui) {
				$(this).val(ui.item.label);
				$("[name=temp_form_id]").val(ui.item.value);
				return false;
			}
		}).on('input change paste', function () {
			$("[name=temp_form_id]").val('');
		});
	});

	function renderFormsTable () {
		$("#drugForms").toggleClass('hidden', !forms.length).find('tbody').html('');
		$("[name=temp_generic_form]").html('<option value="all">All forms</option>');
		$("[name=temp_dosage_form]").html('<option value="all">All forms</option>');
		$("[name=temp_packaging_form]").html('');
		for (var i = 0; i <= forms.length - 1; i++) {
			$("#drugForms tbody").append($("<tr />")
				.append($('<input type="hidden" name="drug_forms[' + i + '][id]" />').val(forms[i].drug_form_id))
				.append($('<input type="hidden" name="drug_forms[' + i + '][name]" />').val(forms[i].name))
				.append($("<td />").html(forms[i].name))
				.append($("<td />").html('<a href="#" class="remove-drug-form" data-index="' + i + '"><b class="glyphicon glyphicon-remove"></b></a>'))
				);
			$("[name=temp_generic_form]").append($('<option value="' + i + '" />').text(forms[i].name));
			$("[name=temp_dosage_form]").append($('<option value="' + i + '" />').text(forms[i].name));
			$("[name=temp_packaging_form]").append($('<option value="' + i + '" />').text(forms[i].name));
		}
	}

</script>