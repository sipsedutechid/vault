@extends('layout.backend')

@section('title')
	{{ trans('drugs.list_title') }}
@endsection

@section('nav_content')

	<div class="navbar-text navbar-back">
		<a href="/backend/drugs" title="{{ trans('drugs.back_dashboard') }}"><b class="glyphicon glyphicon-chevron-left"></b></a>
	</div>
	<div class="navbar-text navbar-title">{{ trans('drugs.list_title') }}</div>
	<a href="/backend/drug/new?redirect=list" class="btn navbar-btn navbar-left"><b class="glyphicon glyphicon-plus"></b>&ensp;{{ trans('drugs.add') }}</a>
	{!! Form::open(['url' => 'backend/drug/all', 'class' => 'navbar-form navbar-left','method' => 'get']) !!}
		<div class="form-group">
			<div class="input-group">
				{!! Form::text('s', Request::input('s'), ['class' => 'form-control', 'placeholder' => trans('drugs.list_search'), 'style' => 'width:300px;']) !!}
				<span class="input-group-btn">
					<button type="submit" class="btn btn-default"><b class="glyphicon glyphicon-search"></b></button>
					@if (Request::input('s')) <a href="/backend/drug/all" class="btn btn-default">{{ trans('drugs.list_search_clear') }}</a>@endif
				</span>
			</div>
		</div>
	{!! Form::close() !!}

@endsection

@section('content')

	@if (count($drugs) > 0 || strlen(Request::input('s')) > 0)

		<div class="panel panel-default panel-table">
			<table class="table {{ count($drugs) > 0 ? "table-hover" : "" }}" id="listTable">
				<tbody>
					@if(count($drugs) == 0)
						<tr><td colspan="3" class="text-center" style="padding-top: 30px;padding-bottom: 30px;">{{ trans('drugs.list_search_empty', ['term' => Request::input('s')]) }}</td></tr>
					@else
						@foreach ($drugs as $drug)
							<tr>
								<td>
									<div>{{ $drug->name }} - <a href="/backend/drug/manufacturer/{{ $drug->drugManufacturer->slug }}/edit?redirect={!! Request::fullUrl() !!}" class="text-muted">{{ $drug->drugManufacturer->name }}</a></div>
									<a href="/backend/drug/{{ $drug->slug }}/edit">{{ trans('drugs.edit') }}</a> / <a href="/backend/drug/{{ $drug->slug }}/trash" class="text-danger">{{ trans('drugs.trash') }}</a>
								</td>
								<td style="max-width:450px;">
									<div class="ellipsis">
										{{ $drug->drugClass->name }}
										{{ $drug->drugSubclass ? '/ ' . $drug->drugSubclass->name : '' }}
									</div>
								</td>
								<td style="width: 150px;">
									<div>{{ $drug->createdBy->name }}</div>
									<div>{{ $drug->created_at }}</div>
								</td>
							</tr>

						@endforeach
					@endif
				</tbody>
			</table>
		</div>

		@if (!Request::input('s'))
			<p class="text-center">
				<button type="button" id="loadMoreBtn" class="btn btn-default">{{ trans('app.load_more') }}</button>
			</p>
		@endif

	@else

		<p>{{ trans('drugs.list_none') }} <a href="/backend/drug/new">{{ trans('drugs.add') }}</a></p>
	@endif

@endsection

@section('endscript')

	<script type="text/javascript">
		$(document).ready(function () {
			var page = 1;

			$("#loadMoreBtn").click(function () {
				$(this).attr('disabled', 'disabled');
				$.getJSON('/backend/drug/all', {json: "true", page: page + 1}).done(function (r) {
					$.each(r.data, function (i, val) {
						var row = $("<tr />");
						var detailsCol = $("<td />")
							.append($("<div />")
								.text(val.name + " - ")
								.append($("<a />").attr('href', '/backend/drug/manufacturer/' + val.drug_manufacturer.slug + '/edit?redirect-drugs').addClass('text-muted').text(val.drug_manufacturer.name))
								)
							.append($("<div />")
								.append($("<a />").attr('href', '/backend/drug/' + val.slug + '/edit').text("{{ trans('drugs.edit') }}"))
								.append(" / ")
								.append($("<a />").attr('href', '/backend/drug/' + val.slug + '/trash').addClass('text-danger').text("{{ trans('drugs.trash') }}"))
								);
						var classCol = $("<td />")
							.css({maxWidth: 450})
							.append($("<div />")
								.addClass('ellipsis')
								.text(val.drug_class.name + " / " + val.drug_subclass.name)
								);
						var createdCol = $("<td />")
							.append($("<div />").text(val.created_by.name))
							.append($("<div />").text(val.created_at));

						row.append(detailsCol).append(classCol).append(createdCol);
						$("#listTable tbody").append(row);
					});
					page = page + 1;
					$("#loadMoreBtn").removeAttr('disabled');
				});
			});
		});

	</script>
@endsection