<div class="panel panel-default">
	<div class="panel-heading">
		<div class="panel-title h4">
			{{ trans('drugs.form_panel_cdp') }}
			<a class="pull-right" data-toggle="collapse" href="#coDoPack"><b class="glyphicon glyphicon-menu-down"></b></a>
			
		</div>
	</div>
	<div class="panel-body collapse in" id="coDoPack">
		<div id="compositionFields" class="hidden">
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('drugs.form_drug_form') }}</label>
				<div class="col-sm-9">
					<select name="temp_generic_form" class="form-control">
						<option value="all">{{ trans('drugs.form_drug_form_all') }}</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('drugs.form_generic') }}</label>
				<div class="col-sm-9">
					<input type="hidden" name="temp_generic_id" />
					<input type="text" name="temp_generic_name" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('drugs.form_quantity') }}</label>
				<div class="col-sm-5">
					<div class="input-group">
						<input type="number" name="temp_generic_qty" class="form-control" style="width:40%;" />
						<input type="text" name="temp_generic_unit" class="form-control" placeholder="Unit" style="width:60%;" />
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-3">
					<button type="button" id="cancelCompositionBtn" class="btn btn-danger btn-normal">{{ trans('drugs.form_cdp_cancel') }}</button>
					<button type="button" id="submitCompositionBtn" class="btn btn-primary btn-normal">{{ trans('drugs.form_cdp_add') }}</button>
				</div>
			</div>
		</div>
		<button id="addCompositionBtn" type="button" class="btn btn-default">{{ trans('drugs.form_add_composition') }}</button>
		<table class="table table-bordered" id="compositions" style="margin-top:15px;">
			<thead><tr><th>{{ trans('drugs.form_drug_form') }}</th><th>{{ trans('drugs.form_generic') }}</th><th style="width:100px;">{{ trans('drugs.form_quantity') }}</th><th style="width:50px;"></tr></thead>
			<tbody></tbody>
		</table>

		<hr />
		<!-- DOSAGE -->
		<div id="dosageFields" class="hidden">
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('drugs.form_drug_form') }}</label>
				<div class="col-sm-9">
					<select name="temp_dosage_form" class="form-control">
						<option value="all">{{ trans('drugs.form_drug_form_all') }}</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('drugs.form_administration') }}</label>
				<div class="col-sm-9">
					<input type="text" name="temp_dosage_administration" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('drugs.form_type') }}</label>
				<div class="col-sm-9">
					<input type="text" name="temp_dosage_type" class="form-control" />
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('drugs.form_description') }}</label>
				<div class="col-sm-9">
					<textarea name="temp_dosage_description" class="form-control" rows="3"></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-3">
					<button type="button" id="cancelDosageBtn" class="btn btn-danger btn-normal">{{ trans('drugs.form_cdp_cancel') }}</button>
					<button type="button" id="submitDosageBtn" class="btn btn-primary btn-normal">{{ trans('drugs.form_cdp_add') }}</button>
				</div>
			</div>
		</div>
		<button id="addDosageBtn" type="button" class="btn btn-default">{{ trans('drugs.form_add_dosage') }}</button>
		<table id="dosages" class="table table-hover table-bordered" style="margin-top:15px;">
			<thead><tr><th>{{ trans('drugs.form_drug_form') }}</th><th>{{ trans('drugs.form_administration') }}</th><th>{{ trans('drugs.form_type') }}</th><th>{{ trans('drugs.form_description') }}</th><th style="width:50px;"></th></tr></thead>
			<tbody></tbody>	
		</table>

		<hr />
		<!-- PACKAGING -->
		<div id="packagingFields" class="hidden">
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('drugs.form_drug_form') }}</label>
				<div class="col-sm-9">
					<select name="temp_packaging_form" class="form-control">
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('drugs.form_name') }}</label>
				<div class="col-sm-9">
					<input type="text" name="temp_packaging_name" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('drugs.form_quantity') }}</label>
				<div class="col-sm-5">
					<div class="input-group">
						<input type="number" name="temp_packaging_qty" class="form-control" placeholder="Q1" style="width:33.33333%" />
						<input type="number" name="temp_packaging_qty2" class="form-control" placeholder="Q2" style="width:33.33333%" />
						<input type="number" name="temp_packaging_qty3" class="form-control" placeholder="Q3" style="width:33.33333%" />
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('drugs.form_price') }}</label>
				<div class="col-sm-3">
					<input type="number" name="temp_packaging_price" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-3">
					<button type="button" id="cancelPackagingBtn" class="btn btn-danger btn-normal">{{ trans('drugs.form_cdp_cancel') }}</button>
					<button type="button" id="submitPackagingBtn" class="btn btn-primary btn-normal">{{ trans('drugs.form_cdp_add') }}</button>
				</div>
			</div>
		</div>
		<button id="addPackagingBtn" type="button" class="btn btn-default" disabled="disabled">{{ trans('drugs.form_add_packaging') }}</button>
		<table id="packagings" class="table table-hover table-bordered" style="margin-top:15px;">
			<thead><tr><th>{{ trans('drugs.form_drug_form') }}</th><th>{{ trans('drugs.form_name') }}</th><th class="text-right">{{ trans('drugs.form_quantity') }}</th><th class="text-right">{{ trans('drugs.form_price') }}</th><th style="width:50px;"></th></tr></thead>
			<tbody></tbody>	
		</table>
	</div>
</div>

<script type="text/javascript">
	var generics = [
		@if (old('drug_generic') || $drug->id)
			<?php $generics = old('drug_generic') ? old('drug_generic') : (array) $generics ?>
			@foreach ($generics as $generic)
				{
					form_index: "{{ $generic['form_index'] || $generic['form_index'] == 0 ? $generic['form_index'] : 'all' }}", 
					generic_id: "{{ $generic['id'] }}", 
					name: "{{ $generic['name'] }}", 
					quantity: "{{ $generic['quantity'] }}", 
					unit: "{{ $generic['unit'] }}"
				},
			@endforeach
		@endif		
		];
	var dosages = [
		@if (old('dosages') || $drug->id)
			<?php $dosages = old('dosages') ? old('dosages') : (array) $dosages ?>
			@foreach ($dosages as $dosage)
				{
					form_index: "{{ $dosage['form_index'] || $dosage['form_index'] == 0 ? $dosage['form_index'] : 'all' }}", 
					type: "{{ $dosage['type'] }}", 
					description: "{{ $dosage['description'] }}", 
					administration: "{{ $dosage['administration'] }}"
				},
			@endforeach
		@endif
		];
	var packagings = [
		@if (old('packagings') || $drug->id)
			<?php $packagings = old('packagings') ? old('packagings') : (array) $packagings ?>
			@foreach ($packagings as $packaging)
				{
					form_index: "{{ $packaging['form_index'] }}", 
					name: "{{ $packaging['name'] }}", 
					quantity: "{{ $packaging['quantity'] }}", 
					quantity2: "{{ $packaging['quantity2'] }}", 
					quantity3: "{{ $packaging['quantity3'] }}", 
					price: "{{ $packaging['price'] }}"
				},
			@endforeach
		@endif
		];
	$(document).ready(function () {
		renderCompositionsTable();
		renderDosagesTable();
		renderPackagingsTable();
		@if (isset($drugForms))
			@if (count($drugForms) > 0)
				$("#addPackagingBtn").prop('disabled', '');
			@endif
		@endif

		$("#addCompositionBtn").click(function () {
			$("#cancelDosageBtn").click();
			$("#cancelPackagingBtn").click();
			$("#compositionFields").removeClass('hidden').find('input').val('');
			$(this).addClass("hidden");
			$("#compositionFields [name=temp_generic_name]").focus();
		});
		$("#addDosageBtn").click(function () { 
			$("#cancelCompositionBtn").click();
			$("#cancelPackagingBtn").click();
			$("#dosageFields input").val(''); 
			$("#dosageFields textarea").val(''); 
			$("#dosageFields").removeClass('hidden'); 
			$(this).addClass('hidden'); 
			$("[name=temp_dosage_form]").focus(); 
		});
		$("#addPackagingBtn").click(function () { 
			$("#cancelCompositionBtn").click();
			$("#cancelDosageBtn").click();
			$("#packagingFields input").val(''); 
			$("#packagingFields").removeClass('hidden'); 
			$(this).addClass('hidden'); 
			$("[name=temp_packaging_form]").focus(); 
		});
		$("#cancelCompositionBtn").click(function () { $("#compositionFields").addClass('hidden'); $("#addCompositionBtn").removeClass('hidden'); });
		$("#cancelDosageBtn").click(function () { $("#dosageFields").addClass('hidden'); $("#addDosageBtn").removeClass('hidden'); });
		$("#cancelPackagingBtn").click(function () { $("#packagingFields").addClass('hidden'); $("#addPackagingBtn").removeClass('hidden'); });
		$("#submitCompositionBtn").click(function () {
			if ($("[name=temp_generic_name]").val() != '') {
				generics.push({form_index: $("[name=temp_generic_form]").val(), generic_id: $("[name=temp_generic_id]").val(), name: $("[name=temp_generic_name]").val(), quantity: $("[name=temp_generic_qty]").val(), unit: $("[name=temp_generic_unit]").val()});
				renderCompositionsTable();
				$("#addCompositionBtn").focus();
			}
		});
		$("#submitDosageBtn").click(function () { 
			dosages.push({form_index: $("[name=temp_dosage_form]").val(), type: $("[name=temp_dosage_type]").val(), description: $("[name=temp_dosage_description]").val(), administration: $("[name=temp_dosage_administration]").val()});
			$("#dosageFields").addClass('hidden'); $("#addDosageBtn").removeClass('hidden');
			renderDosagesTable();
			$("#addDosageBtn").focus();
		});
		$("#submitPackagingBtn").click(function () { 
			packagings.push({
				form_index: $("[name=temp_packaging_form]").val(), 
				name: $("[name=temp_packaging_name]").val(), 
				quantity: $("[name=temp_packaging_qty]").val(), 
				quantity2: $("[name=temp_packaging_qty2]").val(), 
				quantity3: $("[name=temp_packaging_qty3]").val(), 
				price: $("[name=temp_packaging_price]").val()
			});
			$("#packagingFields").addClass('hidden'); $("#addPackagingBtn").removeClass('hidden');
			renderPackagingsTable();
			$("#addPackagingBtn").focus();
		});
		$("#compositions").on('click', '.remove-composition', function (e) {
			e.preventDefault();
			generics.splice($(this).data('index'), 1);
			renderCompositionsTable();
		});
		$("#dosages").on('click', '.remove-dosage', function (e) {
			e.preventDefault();
			dosages.splice($(this).data('index'), 1);
			renderDosagesTable();
		});
		$("#packagings").on('click', '.remove-packaging', function (e) {
			e.preventDefault();
			packagings.splice($(this).data('index'), 1);
			renderPackagingsTable();
		});
		$("[name=temp_generic_name]").autocomplete({
			source: '/service/list/generics',
			response: function (e, ui) {
				$.each(ui.content, function (index, item) {
					item.value = item.id;
					item.label = item.name;
				});
			},
			focus: function (e, ui) {
				$(this).val(ui.item.label);
				return false;
			},
			select: function (e, ui) {
				$(this).val(ui.item.label);
				$("[name=temp_generic_id]").val(ui.item.value);
				return false;
			}
		}).on('input change paste', function () {
			$("[name=temp_generic_id]").val('');
		});
	});

	function renderCompositionsTable () {
		$("#compositions tbody").html('');
		$("#compositions").toggleClass('hidden', !generics.length);

		for (var i = 0; i <= generics.length - 1; i++) {
			$("#compositions tbody").append($('<tr />')
				.append($('<input type="hidden" name="drug_generic[' + i + '][form_index]" />').val(generics[i].form_index != 'all' ? generics[i].form_index : ''))
				.append($('<input type="hidden" name="drug_generic[' + i + '][id]" />').val(generics[i].generic_id))
				.append($('<input type="hidden" name="drug_generic[' + i + '][name]" />').val(generics[i].name))
				.append($('<input type="hidden" name="drug_generic[' + i + '][quantity]" />').val(generics[i].quantity))
				.append($('<input type="hidden" name="drug_generic[' + i + '][unit]" />').val(generics[i].unit))
				.append($('<td />').html(generics[i].form_index != 'all' ? forms[generics[i].form_index].name : "All forms"))
				.append($('<td />').html(generics[i].name))
				.append($('<td />').html(generics[i].quantity + ' ' + generics[i].unit))
				.append($('<td />').html($('<a href="#" class="remove-composition" data-index="' + i + '"><b class="glyphicon glyphicon-remove" /></a>')))
			);
		};
		$("#cancelCompositionBtn").click();
	}

	function renderDosagesTable () {
		$("#dosages").toggleClass('hidden', !dosages.length).find('tbody').html('');
		for (var i = 0; i <= dosages.length - 1; i++) {
			$("#dosages tbody").append($("<tr />")
				.append($('<input type="hidden" name="dosages[' + i + '][form_index]" />').val(dosages[i].form_index != 'all' ? dosages[i].form_index : ''))
				.append($('<input type="hidden" name="dosages[' + i + '][type]" />').val(dosages[i].type))
				.append($('<input type="hidden" name="dosages[' + i + '][description]" />').val(dosages[i].description))
				.append($('<input type="hidden" name="dosages[' + i + '][administration]" />').val(dosages[i].administration))
				.append($("<td />").html(dosages[i].form_index != 'all' ? forms[dosages[i].form_index].name : "All forms"))
				.append($("<td />").html(dosages[i].administration))
				.append($("<td />").html(dosages[i].type))
				.append($("<td />").html(dosages[i].description))
				.append($("<td />").html('<a href="#" class="remove-dosage" data-index="' + i + '"><b class="glyphicon glyphicon-remove"></b></a>'))
				);
		}
	}

	function renderPackagingsTable () {
		$("#packagings").toggleClass('hidden', !packagings.length).find('tbody').html('');
		for (var i = 0; i <= packagings.length - 1; i++) {
			$("#packagings tbody").append($("<tr />")
				.append($('<input type="hidden" name="packagings[' + i + '][form_index]" />').val(packagings[i].form_index != 'all' ? packagings[i].form_index : ''))
				.append($('<input type="hidden" name="packagings[' + i + '][name]" />').val(packagings[i].name))
				.append($('<input type="hidden" name="packagings[' + i + '][quantity]" />').val(packagings[i].quantity))
				.append($('<input type="hidden" name="packagings[' + i + '][quantity2]" />').val(packagings[i].quantity2))
				.append($('<input type="hidden" name="packagings[' + i + '][quantity3]" />').val(packagings[i].quantity3))
				.append($('<input type="hidden" name="packagings[' + i + '][price]" />').val(packagings[i].price))
				.append($("<td />").html(forms[packagings[i].form_index].name))
				.append($("<td />").html(packagings[i].name))
				.append($("<td />").addClass('text-right').html(packagings[i].quantity + (packagings[i].quantity2 ? " x " + packagings[i].quantity2 : '') + (packagings[i].quantity3 ? " x " + packagings[i].quantity3 : '')))
				.append($("<td />").addClass('text-right').html(packagings[i].price))
				.append($("<td />").html('<a href="#" class="remove-packaging" data-index="' + i + '"><b class="glyphicon glyphicon-remove"></b></a>'))
				);
		}
	}

</script>