@extends('layout.backend')

@section('title') {{ trans('drugs.manufacturer.form.edit') }} @endsection

@section('nav_content')

	<div class="navbar-text navbar-back">
		<a href="{!! Request::input('redirect') ? Request::input('redirect') : 'backend/drug/manufacturers' !!}" title="{{ trans('drugs.manufacturer.back_list') }}"><b class="glyphicon glyphicon-chevron-left"></b></a>
	</div>
	<div class="navbar-text navbar-title">{{ trans('drugs.manufacturer.form.edit') }}</div>

@endsection

@section('content')


	{!! Form::model($manufacturer, ['url' => 'backend/drug/manufacturer/' . $manufacturer->slug . '/edit', 'id' => 'mainForm', 'files' => true]) !!}

		<div class="row">
			<div class="col-lg-3 col-md-4 col-sm-6">
				<div class="form-group">
					<div class="text-center logo-image has-overlay">
						<img src="{{ $manufacturer->logo_image ? '/backend/drug/manufacturer/logo/' . $manufacturer->slug : '/media/images/placeholder.png' }}" style="width: 100%;" /> 
						@if($manufacturer->logo_image)
							<div class="overlay" style="border-radius: 5px;"></div>
							<span class="btn-remove">
								<button type="submit" name="post_action" value="remove-logo" class="btn btn-danger"><b class="glyphicon glyphicon-remove"></b> {{ trans('drugs.manufacturer.form.logo_remove') }}</button>
							</span>
						@endif
					</div>
				</div>
				<div class="form-group">
					{{ trans('drugs.manufacturer.form.logo_' . ($manufacturer->logo_image ? 'replace' : 'upload')) }}
					{!! Form::file('logo_image', ['class' => 'form-control']) !!}
					<span class="help-block small">
						{{ trans('drugs.manufacturer.form.logo_help') }}
					</span>
				</div>
				<div class="form-group">
					{!! Form::text('name', null, ['class' => 'form-control input-lg seamless', 'autofocus' => 'autofocus']) !!}
					{!! Form::hidden('slug', null) !!}
				</div>
				<div class="form-group">
					{{ trans('drugs.created_by', ['name' => $manufacturer->createdBy->name]) }}, {{ date('j M y H:i', strtotime($manufacturer->created_at)) }}
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block" name="post_action" value="{{ Request::input('redirect') ? Request::input('redirect') : 'all' }}">{{ trans('drugs.form_save') }}</button>
				</div>
				<div class="form-group">
					<a href="#" data-toggle="modal" data-target="#trashModal" class="text-danger">{{ trans('drugs.trash') }}</a>
				</div>
			</div>
			<div class="col-lg-9 col-md-8 col-sm-6">


				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="panel-title h4">{{ trans('drugs.manufacturer.form.list_drugs_title') }}</div>
					</div>
					@if (count ($drugs) > 0)
						<table class="table table-hover">
							<thead>
								<tr><th>{{ trans('drugs.list_brand_name') }}</th><th>{{ trans('drugs.list_class') }}</th><th style="min-width: 200px;">{{ trans('drugs.list_created') }}</th></tr>
							</thead>
							<tbody>
								@foreach($manufacturer->drugs as $drug)
									<tr>
										<td><a href="/backend/drug/{{ $drug->slug }}/edit?redirect=manufacturer">{{ $drug->name }}</a></td>
										<td style="max-width: 400px;">
											<div class="ellipsis">
												{{ $drug->drugClass->name }} /
												{{ $drug->drugSubclass->name }}
											</div>
										</td>
										<td>{{ $drug->createdBy->name }}, {{ $drug->created_at }}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					@else
						<div class="panel-body">
							{{ trans('drugs.manufacturer.form.list_drugs_empty') }}
						</div>
					@endif
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="panel-title h4">{{ trans('drugs.manufacturer.form.list_marketed_drugs_title') }}</div>
					</div>
					@if(count($marketed_drugs) > 0)
						<table class="table table-hover">
							<thead>
								<tr><th>{{ trans('drugs.list_brand_name') }}</th><th>{{ trans('drugs.list_manufacturer') }}</th><th>{{ trans('drugs.list_class') }}</th></tr>
							</thead>
							<tbody>
								@foreach($marketed_drugs as $drug)
									<tr>
										<td><a href="/backend/drug/{{ $drug->slug }}/edit?redirect=marketer">{{ $drug->name }}</a></td>
										<td><a href="/backend/drug/manufacturer/{{ $drug->drugManufacturer->slug }}/edit">{{ $drug->drugManufacturer->name }}</a></td>
										<td style="max-width: 400px;">
											<div class="ellipsis">
												{{ $drug->drugClass->name }} /
												{{ $drug->drugSubclass->name }}
											</div>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					@else
						<div class="panel-body">
							{{ trans('drugs.manufacturer.form.list_marketed_drugs_empty') }}
						</div>
					@endif
				</div>
			</div>
		</div>

	{!! Form::close() !!}

@endsection

@section('modals')

	<div class="modal fade" id="trashModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					{!! Form::open(['url' => 'backend/drug/manufacturer/' . $manufacturer->slug . '/trash', 'method' => 'delete']) !!}
						<div class="h3">{{ trans('drugs.manufacturer.form.trash_title') }}</div>
						<p>
							{!! trans('drugs.manufacturer.form.trash_help', ['manufacturer' => $manufacturer->name]) !!}
						</p>

						<div class="form-group">
							<select name="move_to" class="form-control">
								<option value="undefined">{{ trans('forms.please_select') }}</option>
								@foreach($manufacturers as $moveto)
									@if ($moveto->id != $manufacturer->id)
										<option value="{{ $moveto->slug }}">{{ $moveto->name }}</option>
									@endif
								@endforeach
							</select>
						</div>

						<div class="text-right">
							<a href="#" data-dismiss="modal" class="btn">Cancel</a>
							<button type="submit" class="btn btn-danger">Move to Trash</button>
						</div>

					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>

@endsection

@section('endscript')

	<script type="text/javascript">
		$(document).ready(function () {
			getSlug(this);
			$("#mainForm [name=name]").on('blur', function() { getSlug(this); });

			function getSlug(elem) {
				if ($(elem).val().length)
					$.get('/service/slugger', {string: $(elem).val()})
						.done(function(r) {
							$("#mainForm [name=slug]").val(r);
						});
			}
		});
	</script>

@endsection