@extends('layout.backend')

@section('title') {{ trans('drugs.manufacturer.list.title') }} @endsection

@section('nav_content')

	<div class="navbar-text navbar-back">
		<a href="/backend/drugs" title="{{ trans('drugs.back_dashboard') }}"><b class="glyphicon glyphicon-chevron-left"></b></a>
	</div>
	<div class="navbar-text navbar-title">{{ trans('drugs.manufacturer.list.title') }}</div>
	<a href="/backend/drug/new" class="btn navbar-btn navbar-left hidden"><b class="glyphicon glyphicon-plus"></b>&ensp;{{ trans('drugs.add') }}</a>
	{!! Form::open(['url' => 'backend/drug/manufacturers', 'class' => 'navbar-form navbar-left','method' => 'get']) !!}
		<div class="input-group">
			{!! Form::text('s', Request::input('s'), ['class' => 'form-control', 'placeholder' => trans('drugs.manufacturer.list.search'), 'style' => 'width:300px;']) !!}
			<span class="input-group-btn">
				<button type="submit" class="btn btn-default"><b class="glyphicon glyphicon-search"></b></button>
				@if (Request::input('s'))<a href="/backend/drug/manufacturers" class="btn btn-default">{{ trans('drugs.list.search_clear') }}</a>@endif
			</span>
		</div>
	{!! Form::close() !!}

@endsection

@section('content')



	<div class="pre-table" style="margin-bottom: 15px;">
		{!! Form::open(['url' => 'backend/drug/bulk-action', 'class' => 'form-inline pull-right hidden']) !!}
			Action
			<div class="input-group">
				{!! Form::select('action', ['trash' => trans('drugs.list_trash')], null, ['class' => 'form-control input-sm']) !!}
				<span class="input-group-btn">
					<button type="submit" class="btn btn-sm btn-default">{{ trans('drugs.list_go') }}</button>
				</span>
			</div>
		{!! Form::close() !!}
	</div>

	@if (count($manufacturers) > 0)
		<div class="row">
			@foreach($manufacturers as $man)
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="panel panel-default">
						@if($man->logo_image)
							<div class="panel-body h4 text-center" style="padding: 0 15px;">
								<a href="/backend/drug/manufacturer/{{ $man->slug }}/edit" data-toggle="tooltip" data-placement="top" title="{{ $man->name }}">
									<img src="/backend/drug/manufacturer/logo/{{ $man->slug }}" style="height: 49px;" />
								</a>
							</div>
						@else
							<div class="panel-body h4">
								<div class="ellipsis"><a href="/backend/drug/manufacturer/{{ $man->slug }}/edit">{{ $man->name }}</a></div>
							</div>
						@endif
						<ul class="list-group">
							<li class="list-group-item small">{{ count($man->drugs) }} {{ trans_choice('drugs.manufacturer.list.drug_count', count($man->drugs)) }}</li>
							<li class="list-group-item small">{{ count($man->marketedDrugs) }} {{ trans_choice('drugs.manufacturer.list.marketed_drug_count', count($man->marketedDrugs)) }}</li>
						</ul>
					</div>
				</div>
			@endforeach
		</div>

		{!! $manufacturers->appends($appends)->links() !!}

	@endif

@endsection

@section('endscript')

	<script type="text/javascript">
		$(document).ready(function () {
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>

@endsection