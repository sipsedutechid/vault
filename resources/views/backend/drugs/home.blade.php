@extends('layout.backend')

@section('title')
	{{ trans('drugs.title') }}
@endsection

@section('nav_content')

	<div class="navbar-text navbar-title">{{ trans('drugs.title') }}</div>
	<a href="/backend/drug/new" class="btn navbar-btn"><b class="glyphicon glyphicon-plus"></b>&ensp;{{ trans('drugs.add') }}</a>
		
@endsection

@section('content')

	<div class="row">
		<div class="col-lg-6">
				<div class="panel panel-default">
					<div class="panel-body h3">
						<a href="/backend/drug/all">
							{{ trans('drugs.home_all') }} <span class="badge pull-right">{{ $drug_count }}</span>
						</a>
					</div>
					<table class="table table-condensed">
						<thead><tr><th colspan="2">{{ trans('drugs.home_all_recent') }}</th></tr></thead>
						<tbody>
							@foreach($drug_recents as $drug)
								<tr>
									<td style="width:100px;">{{ $drug->created_at }}</td>
									<td><a href="/backend/drug/{{ $drug->slug }}/edit?redirect=dashboard">{{ $drug->name }}</a> ({{ $drug->drugManufacturer->name }})</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</a>
		</div>
		<div class="col-lg-3">
			<a href="/backend/drug/generics">
				<div class="panel panel-default">
					<div class="panel-body h4">
						{{ trans('drugs.home_all_generics') }} <span class="badge pull-right">{{ $generic_count }}</span>
					</div>
				</div>
			</a>
		</div>
		<div class="col-lg-3">
			<a href="/backend/drug/manufacturers">
				<div class="panel panel-default">
					<div class="panel-body h4">
						{{ trans('drugs.home_manufacturers') }} <span class="badge pull-right">{{ $manufacturer_count }}</span>
					</div>
				</div>
			</a>
		</div>
		<div class="col-lg-3">
			<a href="/backend/drug/classes">
				<div class="panel panel-default">
					<div class="panel-body h4">
						{{ trans('drugs.home_classes') }} <span class="badge pull-right">{{ $class_count }}</span>
					</div>
				</div>
			</a>
		</div>
		<div class="col-lg-3">
			<a href="/backend/drug/diseases">
				<div class="panel panel-default">
					<div class="panel-body h4">
						{{ trans('drugs.home_diseases') }} <span class="badge pull-right">{{ $drug_disease_count }}</span>
					</div>
				</div>
			</a>
		</div>

	</div>

@endsection