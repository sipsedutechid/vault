@extends('layout.backend')

@section('title')
	{{ trans('drugs.form_' . ($drug->id ? 'edit' : 'add')) }}
@endsection

@section('nav_content')

	<div class="navbar-text navbar-back">
		@if (Request::input('redirect') == "manufacturer" && $drug->id)
			<a href="/backend/drug/manufacturer/{{ $drug->drugManufacturer->slug }}/edit" title="{{ trans('drugs.back_manufacturer') }}">
				<b class="glyphicon glyphicon-chevron-left"></b>
			</a>
		@elseif (Request::input('redirect') == "marketer" && $drug->id && $drug->drug_marketer_id)
			<a href="/backend/drug/manufacturer/{{ $drug->drugMarketer->slug }}/edit" title="{{ trans('drugs.back_marketer') }}">
				<b class="glyphicon glyphicon-chevron-left"></b>
			</a>
		@elseif (Request::input('redirect') == "dashboard" && $drug->id)
			<a href="/backend/drugs" title="{{ trans('drugs.back_dashboard') }}">
				<b class="glyphicon glyphicon-chevron-left"></b>
			</a>
		@elseif (Request::input('redirect') == "list")
			<a href="/backend/drug/all" title="{{ trans('drugs.back_list') }}">
				<b class="glyphicon glyphicon-chevron-left"></b>
			</a>
		@else
			<a href="/backend/{{ $drug->id ? 'drug/all' : 'drugs' }}" title="{{ trans('drugs.back_' . ($drug->id ? 'list' : 'dashboard')) }}">
				<b class="glyphicon glyphicon-chevron-left"></b>
			</a>
		@endif

	</div>
	<div class="navbar-text navbar-title">
		{{ trans('drugs.form_' . ($drug->id ? 'edit' : 'add')) }}
	</div>
@endsection

@section('headscript')
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.structure.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.theme.min.css') }}">
	<script type="text/javascript" src="{{ asset('libs/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection

@section('content')

	{!! Form::model($drug, ['url' => 'backend/drug/' . ($drug->id ? $drug->slug . '/edit' : 'new'), 'class' => 'form-horizontal', 'novalidate' => 'novalidate']) !!}
		@if ($drug->id)
			{!! Form::hidden('id') !!}
		@endif
		{!! Form::hidden('slug') !!}
		<div class="row">
			<div class="col-sm-8">
				<div class="form-group">
					<div class="col-sm-5">
						{!! Form::hidden('drug_manufacturer_id') !!}
						{!! Form::text('manufacturer', $drug->id ? $drug->drugManufacturer->name : null, ['class' => 'form-control input-lg seamless', 'placeholder' => trans('drugs.form_manufacturer'), 'autofocus' => 'autofocus', 'data-target' => 'drug_manufacturer_id']) !!}
					</div>
					<div class="col-sm-7">
						{!! Form::text('name', null, ['class' => 'form-control input-lg seamless', 'placeholder' => trans('drugs.form_brand_name')]) !!}
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="panel-title h4">
							{{ trans('drugs.form_panel_details') }}
							<a class="pull-right" data-toggle="collapse" href="#drugDetails"><b class="glyphicon glyphicon-menu-down"></b></a>
						</div>
					</div>
					<div class="panel-body collapse in" id="drugDetails">

						<div class="form-group">
							<label class="control-label col-sm-3">{{ trans('drugs.form_marketer') }}</label>
							<div class="col-sm-9">
								{!! Form::hidden('drug_marketer_id') !!}
								{!! Form::text('marketer', $drug->id && $drug->drug_marketer_id ? $drug->drugMarketer->name : null, ['class' => 'form-control', 'data-target' => 'drug_marketer_id']) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-3">{{ trans('drugs.form_indication') }}</label>
							<div class="col-sm-9">
								{!! Form::textarea('indication', null, ['class' => 'form-control', 'rows' => 4]) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3">{{ trans('drugs.form_contraindication') }}</label>
							<div class="col-sm-9">
								{!! Form::textarea('contraindication', null, ['class' => 'form-control', 'rows' => 4]) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3">{{ trans('drugs.form_precautions') }}</label>
							<div class="col-sm-9">
								{!! Form::textarea('precautions', null, ['class' => 'form-control', 'rows' => 4]) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3">{{ trans('drugs.form_adverse_reactions') }}</label>
							<div class="col-sm-9">
								{!! Form::textarea('adverse_reactions', null, ['class' => 'form-control', 'rows' => 4]) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3">{{ trans('drugs.form_interactions') }}</label>
							<div class="col-sm-9">
								{!! Form::textarea('interactions', null, ['class' => 'form-control', 'rows' => 4]) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3">{{ trans('drugs.form_administration') }}</label>
							<div class="col-sm-9">
								{!! Form::select('administration', ['cc' => trans('drugs.form_administration_cc'), 'scc' => trans('drugs.form_administration_scc'), 's/cc' => trans('drugs.form_administration_s_cc')], null, ['class' => 'form-control', 'placeholder' => trans('drugs.form_administration_null')]) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3">{{ trans('drugs.form_administration_extra') }}</label>
							<div class="col-sm-9">
								{!! Form::textarea('administration_extra', null, ['class' => 'form-control', 'rows' => 4]) !!}
							</div>
						</div>
					</div>
				</div>

				@include('backend.drugs.form-drug-form')
				@include('backend.drugs.form-composition-dosage-packaging')

				
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<div class="col-xs-12">
						@if (Request::input('redirect') == "manufacturer" && $drug->id)
							<button type="submit" name="post_action" value="manufacturer" class="btn btn-primary btn-block">{{ trans('drugs.form_' . ($drug->id ? 'update' : 'save')) }}</button>
						@elseif (Request::input('redirect') == "dashboard" && $drug->id)
							<button type="submit" name="post_action" value="dashboard" class="btn btn-primary btn-block">{{ trans('drugs.form_' . ($drug->id ? 'update' : 'save')) }}</button>
						@else
							<button type="submit" name="post_action" value="all" class="btn btn-primary btn-block">{{ trans('drugs.form_' . ($drug->id ? 'update' : 'save')) }}</button>
						@endif
						@if (!$drug->id)
							<button type="submit" name="post_action" value="new" class="btn btn-default btn-block">{{ trans('drugs.form_save_and_add') }}</button>
						@else
							<button type="submit" name="post_action" value="edit" class="btn btn-default btn-block">{{ trans('drugs.form_update_and_continue') }}</button>

						@endif
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading"><div class="panel-title h4">{{ trans('drugs.form_panel_class') }}</div></div>
					<div class="panel-body">
						<div class="form-group">
							<label class="control-label col-sm-4">{{ trans('drugs.form_class') }}</label>
							<div class="col-sm-8">
								{!! Form::select('drug_class_id', $classes, null, ['placeholder' => trans('drugs.form_select'), 'class' => 'form-control']) !!}
							</div>
						</div>
						<div class="form-group hidden" id="subclassContainer">
							<label class="control-label col-sm-4">{{ trans('drugs.form_subclass') }}</label>
							<div class="col-sm-8">
								<select name="drug_subclass_id" class="form-control">
								</select>
							</div>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading"><div class="panel-title h4">{{ trans('drugs.form_panel_classification') }}</div></div>
					<div class="panel-body">

						<div class="form-group">
							<label class="control-label col-sm-4">{{ trans('drugs.form_distribution') }}</label>
							<div class="col-sm-8">
								{!! Form::select('classification', ['o' => trans('drugs.form_distribution_o'), 'g' => trans('drugs.form_distribution_g'), 'w' => trans('drugs.form_distribution_w'), 'b' => trans('drugs.form_distribution_b')], null, ['placeholder' => trans('drugs.form_null'), 'class' => 'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4">{{ trans('drugs.form_pregnancy') }}</label>
							<div class="col-sm-8">
								{!! Form::select('pregnancy_category', ['a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D', 'x' => 'X'], null, ['placeholder' => trans('drugs.form_null'), 'class' => 'form-control']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	{!! Form::close() !!}

@endsection

@section('endscript')
	<script type="text/javascript">
		$(document).ready(function () {
			@if(old('drug_class_id'))
				renderSubclass($("[name=drug_class_id]"){!! empty(old('drug_subclass_id')) ? '' : ', "' . old('drug_subclass_id') . '"' !!});
			@elseif($drug->id)
				renderSubclass($("[name=drug_class_id]"){!! empty($drug->drug_subclass_id) ? '' : ', "' . $drug->drug_subclass_id . '"' !!});
			@endif
			$("[name=drug_class_id]").change(function () {
				renderSubclass($(this));
			});
			$("[name=manufacturer], [name=name]").blur(function () {
				if ($("[name=manufacturer]").val().length && $("[name=name]").val().length) {
					$.get('/service/slugger', {string: $("[name=manufacturer]").val() + ' ' + $("[name=name]").val()})
						.done(function (r) {
							$("[name=slug]").val(r);
						});
				}
			});
			$("[name=manufacturer], [name=marketer]").autocomplete({
				source: '/service/list/drug-manufacturers',
				response: function (e, ui) {
					$.each(ui.content, function (index, item) {
						item.value = item.id;
						item.label = item.name;
					});
				},
				focus: function (e, ui) {
					$(this).val(ui.item.label);
					return false;
				},
				select: function (e, ui) {
					$("[name=" + $(this).data('target') + "]").val(ui.item.value);
					$(this).val(ui.item.label);
					return false;
				}
			}).on('input change paste', function () {
				$("[name=" + $(this).data('target') + "]").val('');
			});
		});

		function renderSubclass (classElem, selection) {
			if (classElem.val() != '') {
				classElem.find('option[value=]').remove();
				$.getJSON('/service/list/drug-subclasses', {class_id: classElem.val()})
					.done(function (r) {
						var count = 0;
						$("[name=drug_subclass_id]").html('');
						$.each(r, function (index, item) {
							$("[name=drug_subclass_id]").append($("<option />").val(item.id).html(item.name));
							count++;
						});
						$("#subclassContainer").toggleClass('hidden', count == 0);
						if (selection != undefined)
							$("[name=drug_subclass_id]").val(selection);
					});
			}
		}
	</script>

@endsection