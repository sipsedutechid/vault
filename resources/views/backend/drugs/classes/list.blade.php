@extends('layout.backend')

@section('title')
	Drug Classes
@endsection

@section('nav_content')
	
	<div class="navbar-text navbar-back">
		<a href="/backend/drugs" title="{{ trans('drugs.back_dashboard') }}"><b class="glyphicon glyphicon-chevron-left"></b></a>
	</div>
	<div class="navbar-text navbar-title">Drug Classes</div>

@endsection


@section('content')

	<div class="panel panel-default">
		<table class="table table-hover">
			<thead><tr><th>Class Name</th><th style="width:150px;">Subclass Count</th><th style="width:150px;">Drug Count</th></tr></thead>
			<tbody>
				@if (count ($classes) > 0)
					@foreach($classes as $class)
						<tr>
							<td>
								@if ($class->subclasses()->count() > 0)
									<a href="#" data-toggle="details" data-code="{{ $class->code }}">{{ $class->name }}</a>
								@else
									{{ $class->name }}
								@endif
							</td>
							<td>{{ $class->subclasses()->count() }}</td>
							<td>{{ $class->drugs()->count() }}</td>
						</tr>
						@if ($class->subclasses()->count() > 0)
							@foreach ($class->subclasses as $subclass)
								<tr data-parent="{{ $class->code }}"><td colspan="2" style="border-top: none;">&emsp;{{ $subclass->name }}</td><td style="border-top: none;">{{ $subclass->drugs->count() }}</td></tr>
							@endforeach
						@else

						@endif
					</tbody>
					@endforeach
				@else
					<tr><td colspan="3" class="text-center">No drug classes found</td></tr>
				@endif
			<tfoot><tr><th>Class Name</th><th>Subclass Count</th><th style="width:150px;">Drug Count</th></tr></tfoot>
		</table>
	</div>
	{!! $classes->render() !!}


@endsection

@section('endscript')

	<script type="text/javascript">
		$(document).ready(function () {
			$("[data-parent]").hide();
			$("a[data-toggle=details]").on('click', function (e) {
				e.preventDefault();
				$("[data-parent]").hide();
				$("tr[data-parent=" + $(this).data('code') + "]").toggle();
				$(this).blur();
			});
		});
	</script>
	
@endsection