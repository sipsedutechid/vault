{!! Form::open(['url' => '/backend/drug/class']) !!}
	{!! Form::hidden('slug') !!}
	<h3>Add New Class</h3>

	{{ $errors->first() }}
	<div class="form-group">
		<label for="name">Class Name</label>
		{!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
	</div>
	<div class="form-group">
		<label for="subclasses">Subclasses</label>
		{!! Form::textarea('subclasses', old('subclasses'), ['class' => 'form-control', 'rows' => 5]) !!}
		<div class="help-block small">
			Separate each subclass with new lines
		</div>
	</div>
	<button type="submit" class="btn btn-block btn-primary">Save Class</button>

{!! Form::close() !!}