@extends('layout.backend')

@section('title')
	Edit Drug Class
@endsection

@section('content')

	<p>
		<a href="/backend/drug/classes">&laquo; Back to Drug Classes List</a>
	</p>

	<div class="h1">Edit Drug Class</div>
	<div class="row" style="margin-top:30px;">
		<div class="col-sm-8">
			{!! Form::open(['url' => '/backend/drug/class/' . $class->code . '/edit', 'class' => 'form-horizontal']) !!}
			{!! Form::hidden('id', $class->id) !!}
			<div class="form-group">
				<label class="control-label col-sm-4">Class Name</label>
				<div class="col-sm-8">
					{!! Form::text('name', old('name') ? old('name') : $class->name, ['class' => 'form-control', 'autofocus' => 'autofocus']) !!}
				</div>
				
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Remove Subclasses</label>
				<div class="col-sm-8">
					@if (!count($subclasses) > 0)
						<p class="form-control-static">There are no subclasses assigned to this class yet.</p>
					@else
						@foreach($subclasses as $sub)
						<div class="checkbox">
							<label>{!! Form::checkbox('remove[]', $sub->id, old('remove[]')) !!} {{ $sub->name }}</label>
						</div>
						@endforeach
					@endif
				</div>
				
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Add Subclasses</label>
				<div class="col-sm-8">
					{!! Form::textarea('subclasses', null, ['class' => 'form-control', 'rows' => 5]) !!}
					<div class="help-block small">
						Separate each subclass with new lines
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-8 col-sm-offset-4">
					<button class="btn btn-primary" type="submit">Save Changes</button>
				</label>
			</div>

			{!! Form::close() !!}
			
		</div>
	</div>

@endsection