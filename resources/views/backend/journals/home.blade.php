@extends('layout.backend')

@section('title') {{ trans('journals.home.title') }} @endsection

@section('nav_content')
	<div class="navbar-text navbar-title">{{ trans('journals.home.title') }}</div>
	<a href="/backend/journal/new" class="btn navbar-btn">
		<b class="glyphicon glyphicon-plus"></b>
		{{ trans('journals.actions.add') }}
	</a>
@endsection

@section('content')

	<h1>{{ trans('journals.title') }}</h1>
	<p>{{ trans('journals.explanation') }}</p>
	<a href="/backend/journal/new" class="btn btn-primary">{{ trans('journals.actions.add') }}</a>

	<div class="row" style="margin-top:45px;">
		<div class="col-lg-3">
			<a href="/backend/journal/all?status=publish">
				<div class="panel panel-default">
					<div class="panel-body h4">
						{{ trans('journals.posted_journals') }} <span class="badge pull-right">{{ $journalPublish }}</span>
					</div>
				</div>
			</a>
		</div>
		<div class="col-lg-3">
			<a href="/backend/journal/all?status=draft">
				<div class="panel panel-default">
					<div class="panel-body h4">
						{{ trans('journals.draft') }} <span class="badge pull-right">{{ $journalDraft }}</span>
					</div>
				</div>
			</a>
		</div>

		<div class="col-lg-3">
			<a href="/backend/journal/categories">
				<div class="panel panel-default">
					<div class="panel-body h4">
						{{ trans('journals.form.category') }} <span class="badge pull-right">{{ $category }}</span>
					</div>
				</div>
			</a>
		</div>

	</div>

@endsection
