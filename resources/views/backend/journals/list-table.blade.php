@foreach ($journals as $journal)
    <tr>
        <td>
            <div>{{ $journal->title }} </div>
            <a href="/backend/journal/{{ $journal->slug }}/edit">{{ trans('journals.edit') }}</a> / <a href="/backend/journal/{{ $journal->slug }}/trash" class="text-danger">{{ trans('journals.trash') }}</a>
        </td>
        <td style="max-width:450px;">
            <div class="ellipsis">
                {{ $journal->publisher }}
            </div>
        </td>
        <td style="width: 150px;">
            <div>{{ $journal->createdBy->name }}</div>
            <div>{{ $journal->created_at }}</div>
        </td>
    </tr>
@endforeach
