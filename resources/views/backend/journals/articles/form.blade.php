<script src="{{ asset('assets/js/dynamicForm.js') }}" charset="utf-8"></script>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title h4">
            {{ trans('articles.form.detail_title') }}
            <a class="pull-right" data-toggle="collapse" href="#articleJournal"><b class="glyphicon glyphicon-menu-down"></b></a>
        </div>
    </div>
    <div class="panel-body collapse in" id="articleJournal">
		<button id="addArticleButton" type="button" class="btn btn-primary" data-toggle='modal' data-target='#createArticleModal'>{{ trans('articles.form.new') }}</button>

		<table class="table table-bordered hidden" id="articles" style="margin-top:15px;">
			<thead>
                <tr>
                    <th>{{ trans('articles.form.title') }}</th>
                    <th>{{ trans('articles.form.author') }}</th>
                    <th style="width:50px;"></th>
                </tr>
            </thead>
			<tbody></tbody>
		</table>
    </div>
</div>

@section ('modals')
<div class="modal fade" id="createArticleModal" tabindex="-1" role="dialog" aria-labelledby="articleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="articleModalLabel">{{ trans('articles.modal_title') }}</h4>
            </div>
            <div class="modal-body">
                <form id="article-form" enctype="multipart/form-data" method="post" >
                    <div class="form-group">
                        <label class="control-label">{{ trans('articles.form.title') }}</label>
                        <input type="text" class="form-control" name="article_title">
                    </div>
                    <div class="form-group">
                        <textarea name="article_desc" id="article-desc"></textarea>
                    </div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('articles.form.author') }}</label>
                        <textarea name="article_author" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('articles.form.publish_date') }}</label>
                        <input type="date" name="article_publish_date" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('articles.form.volume') }}</label>
                        <input type="number" name="article_journal_volume" min="1" class="form-control" value="1">
                    </div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('articles.form.file') }}</label>
                        <input type="file" name="article_file">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('topics.form_def_cancel') }}</button>
                <button type="button" id="modal-btn-save" name="button" class="btn btn-primary">
                    <span id="article-modal-save"> {{ trans('articles.form.save') }} </span>
                    <span id="article-modal-saving" style="display:none;"> <i class='fa fa-circle-o-notch fa-spin'></i> {{ trans('articles.form.saving') }}  </span>
                    <span id="article-modal-success-saving" style="display:none"> {{ trans('articles.form.success_saving') }} </span>
                    <span id="article-modal-failed-saving" style="display:none"> {{ trans('articles.form.failed_saving') }} </span>
                </button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    tinymce.init({ selector:'textarea#article-desc' });
    var btnModalSave    = document.getElementById('modal-btn-save') ,
        artModalSave    = document.getElementById('article-modal-save') ,
        artModalSaving  = document.getElementById('article-modal-saving') ,
        artModalSuccess = document.getElementById('article-modal-success-saving') ,
        artModalFailed  = document.getElementById('article-modal-failed-saving');

    btnModalSave.onclick = function(){
        this.setAttribute('disabled', 'true');
        artModalSave.style.display = 'none';
        artModalSaving.style.display = 'block';
        requestData();
    }

    function requestData(){
        var formArticle  = document.querySelector('form#article-form');
        var ed           = tinyMCE.get('article-desc');
        var article_desc = ed.getContent();
        var formData     = new FormData(formArticle);

        formData.append( 'article_desc' , article_desc );

        var myInit = {
            method  : 'post' ,
            body    : formData
        };

        fetch( "{{ $app->make('url')->to('/service/journal/article/add') }}", myInit )
            .then(function(response) {
                return response.json();
            })
            .then(function(data) {
                artModalSaving.style.display    = 'none';
                btnModalSave.classList.remove('btn-primary');

                if (data.status) {
                    artModalSuccess.style.display = 'block';

                    btnModalSave.classList.add('btn-success');

                    setTimeout(function () {
                        resultArticle.setResult([
                            {
                                tag     : 'input',
                                key     : 'title' ,
                                val     : formArticle.elements.namedItem("article_title").value ,
                                label   : formArticle.elements.namedItem("article_title").value
                            } ,
                            {
                                tag     : 'input' ,
                                key     : 'author' ,
                                val     : formArticle.elements.namedItem("article_author").value ,
                                label   : formArticle.elements.namedItem("article_author").value
                            } ,
                            {
                                tag     : 'input' ,
                                key     : 'data_status' ,
                                val     : "new" ,
                                parent  : 'title' ,
                                splitter: '' ,
                                hide    : true
                            } ,
                            {
                                tag     : 'input' ,
                                key     : 'slug' ,
                                val     :  data.slug ,
                                parent  : 'title' ,
                                splitter: '' ,
                                hide    : true
                            }
                        ]);

                        $('#createArticleModal').modal('hide');
                        resetFormModalArticle();
                    }, 1000);
                } else {
                    artModalFailed.style.display = 'block';

                    btnModalSave.classList.add('btn-danger');

                    setTimeout(function () {
                        resetFormModalArticle();
                    }, 1000);
                }
            }).catch(function(err) {
            	console.log(err);
            });

        return false;

    }

    function resetFormModalArticle(){
        setTimeout(function () {
            document.querySelector('form#article-form').reset();

            btnModalSave.classList.remove('btn-danger');
            btnModalSave.classList.remove('btn-success');

            btnModalSave.classList.add('btn-primary');

            artModalSave.style.display      = 'block';
            artModalFailed.style.display    = 'none';
            artModalSuccess.style.display   = 'none';
            artModalSaving.style.display    = 'none';

            btnModalSave.removeAttribute('disabled');
        }, 500);
    }

</script>

<script type="text/javascript">
    var resultArticle = new formDynamicGakken('articles');

    var articles = [
        @if ($journal->id)
    		@foreach ($journal->articles as $article)
    			[
                    { tag : 'input' , key : 'title' ,       val : "{{ $article->title }}" ,     label : "{{ $article->title }}" } ,
                    { tag : 'input' , key : 'author' ,      val : "{{ $article->author }}" ,    label : "{{ $article->author }}" } ,
                    { tag : 'input' , key : 'data_status' , val : "old" ,                  parent : 'title' , splitter : '' , hide : true } ,
                    { tag : 'input' , key : 'slug' ,        val : "{{ $article->slug }}" , parent : 'title' , splitter : '' , hide : true }
                ] ,
    		@endforeach
    	@endif
    ];

    resultArticle.setElement({
        'table_result' : '#articles'
    });

    //handling modal choosing article;
    articles.forEach( function( data) {
        resultArticle.setResult(data);
    });

</script>
@endsection
