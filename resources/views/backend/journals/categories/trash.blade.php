@extends('layout.backend')

@section('title') Move {{ $category->labels }} to Trash @endsection

@section('content')

	<h1>Move to Trash</h1>
	{!! Form::open(['url' => 'backend/journal/category/trash']) !!}
		<p>
			You are about to move the category <strong>{{ $category->labels }}</strong> to trash.<br />
			Are you sure? This action cannot be undone.
		</p>
		{!! Form::hidden('slug', $category->slug) !!}
		<a href="/backend/journal/categories" class="btn btn-default">No, take me back</a>
		<button type="submit" class="btn btn-danger">Yes, move this category to trash</button>

	{!! Form::close() !!}

@endsection
