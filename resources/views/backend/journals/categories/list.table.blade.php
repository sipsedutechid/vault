@foreach ($categories as $category)
    <tr>
        <td>
            <div>{{ $category->labels }}</div>
            <a href="/backend/journal/category/{{ $category->slug }}/edit">{{ trans('journals.edit') }}</a> / <a href="/backend/journal/category/{{ $category->slug }}/trash" class="text-danger">{{ trans('journals.trash') }}</a>
        </td>
        <td style="max-width:450px;">
            <div class="ellipsis" style="text-transform: capitalize;">
                {{ $category->type }}
            </div>
        </td>
    </tr>
@endforeach
