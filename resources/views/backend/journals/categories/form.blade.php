@extends('layout.backend')

@section('title')
    {{ trans('journals.category.form_' . ($category->id ? 'edit' : 'add')) }}
@endsection

@section('headscript')
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.structure.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.theme.min.css') }}">
	<script type="text/javascript" src="{{ asset('libs/jquery-ui/jquery-ui.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}" media="screen" charset="utf-8">
@endsection

@section('content')

	<a href="/backend/journal/categories" title="Journals">
		<b class="glyphicon glyphicon-chevron-left"></b> {{ trans('journals.category.back_to_dashboard') }}
    </a>

    <h1> {{ trans('journals.category.actions_' . ($category->id ? 'edit' : 'add')) }}</h1>

    {!! Form::model($category, ['url' => 'backend/journal/category/' . ($category->id ? $category->slug . '/edit' : 'new') . ($request->redirect ? '/?redirect=' . urlencode($request->redirect) : ''), 'class' => 'form-horizontal', 'novalidate' => 'novalidate', 'id' => 'formJournal']) !!}
		@if ($category->id)
			{!! Form::hidden('id', null, ['readonly' => 'true']) !!}
			{!! Form::hidden('issn', null, ['readonly' => 'true']) !!}
		@endif

		<div class="row">
			<div class="col-sm-8">
				<div class="form-group">
                    <label class="control-label col-sm-3">
                        {!! trans('journals.category.form_label') !!}
                    </label>
					<div class="col-sm-9">
						{!! Form::text('labels', null, ['class' => 'form-control']) !!}
					</div>
				</div>

                <div class="form-group">
                    <label class="control-label col-sm-3">
                        {!! trans('journals.category.form_description') !!}
                    </label>
                    <div class="col-sm-9">
                        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                    </div>
                </div>

            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <div class="col-xs-12">
                        @if (!$category->id)
                            <button type="submit" name="post_action" value="save_publish" class="btn btn-primary btn-block">{{ trans('journals.form.save') }}</button>
                        @else
                            <button type="submit" name="post_action" value="update" class="btn btn-primary btn-block">{{ trans('journals.form.update') }}</button>
                        @endif
                    </div>
                </div>

            </div>
        </div>

	{!! Form::close() !!}

@endsection
