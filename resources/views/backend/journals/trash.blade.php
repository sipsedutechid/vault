@extends('layout.backend')

@section('title') Move {{ $journal->name }} to Trash @endsection

@section('content')

	<h1>Move to Trash</h1>
	{!! Form::open(['url' => 'backend/journal/trash']) !!}
		<p>
			You are about to move the journal <strong>{{ $journal->title }}</strong> to trash.<br />
			Are you sure? This action cannot be undone.
		</p>
		{!! Form::hidden('slug', $journal->slug) !!}
		<a href="/backend/journal/all" class="btn btn-default">No, take me back</a>
		<button type="submit" class="btn btn-danger">Yes, move this journal to trash</button>

	{!! Form::close() !!}

@endsection
