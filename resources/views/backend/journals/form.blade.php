@extends('layout.backend')

@section('title')
    {{ trans('journals.form_' . ($journal->id ? 'edit' : 'add')) }}
@endsection

@section('headscript')
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.structure.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.theme.min.css') }}">
	<script type="text/javascript" src="{{ asset('libs/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/js/tinymce/tinymce.min.js') }}" charset="utf-8"></script>
    <script type="text/javascript">
        tinymce.init({ selector:'textarea#description' });
    </script>
	<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}" media="screen" charset="utf-8">
@endsection

@section('content')

	<a href="/backend/journals" title="Journals">
		<b class="glyphicon glyphicon-chevron-left"></b> {{ trans('journals.back_to_dashboard') }}
    </a>

    <h1> {{ trans('journals.actions.' . ($journal->id ? 'edit' : 'add')) }}</h1>

    {!! Form::model($journal, ['files' => true, 'url' => 'backend/journal/' . ($journal->id ? $journal->slug . '/edit' : 'new') . ($request->redirect ? '/?redirect=' . urlencode($request->redirect) : ''), 'class' => 'form-horizontal', 'novalidate' => 'novalidate', 'id' => 'formJournal']) !!}
		@if ($journal->id)
			{!! Form::hidden('id', null, ['readonly' => 'true']) !!}
			{!! Form::hidden('issn', null, ['readonly' => 'true']) !!}
		@endif

		<div class="row">
			<div class="col-sm-8">
				<div class="form-group">
					<div class="col-sm-6">
						{!! Form::text('title', null, ['class' => 'form-control input-lg seamless', 'placeholder' => trans('journals.form.title')]) !!}
					</div>
                    <div class="col-sm-3">
						{!! Form::text('issn', null, ['class' => 'form-control input-lg seamless', 'placeholder' => trans('journals.form.issn')]) !!}
					</div>
                    <div class="col-sm-3">
						{!! Form::text('e_issn', null, ['class' => 'form-control input-lg seamless', 'placeholder' => trans('journals.form.e_issn')]) !!}
					</div>
				</div>

                <div class="form-group">

                    <div class="col-sm-12">
                        {!! Form::textarea('description', null, ['id' => 'description']) !!}
                    </div>
                </div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="panel-title h4">
							{!! trans('journals.form.detail') !!}
							<a class="pull-right" data-toggle="collapse" href="#journalDetail"><b class="glyphicon glyphicon-menu-down"></b></a>
						</div>
					</div>

                    <div class="panel-body collapse in" id="journalDetail">
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{ trans('journals.form.publisher') }}</label>
                            <div class="col-sm-9">
                                {!! Form::text('publisher', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3">{{ trans('journals.form.editor') }}</label>
                            <div class="col-sm-9">
                                {!! Form::text('editor', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3">{{ trans('journals.form.publish_date') }}</label>
                            <div class="col-sm-9">
                                {!! Form::date('publish_date', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3">{{ trans('journals.form.url') }}</label>
                            <div class="col-sm-9">
                                {!! Form::text('ext_url', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
					<div class="panel-heading">
						<div class="panel-title h4">
							{!! trans('journals.form.tags') !!}
							<a class="pull-right" data-toggle="collapse" href="#journalTags"><b class="glyphicon glyphicon-menu-down"></b></a>
						</div>
					</div>

                    <div class="panel-body collapse in" id="JournalTags">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <textarea name="tags" class="form-control"><?= $journal->id ? $tags : '' ?></textarea>
                                <h5>{{ trans('journals.tags_instruction') }}</h5>
                            </div>
                        </div>
                    </div>
                </div>

                @include('backend.journals.articles.form')

            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <div class="col-xs-12">
                        @if (!$journal->id)
                            <button type="submit" name="post_action" value="save_publish" class="btn btn-primary btn-block">{{ trans('journals.form.save_publish') }}</button>
                            <button type="submit" name="post_action" value="save_draft" class="btn btn-default btn-block">{{ trans('journals.form.save_draft') }}</button>
                        @else
                            <button type="submit" name="post_action" value="update" class="btn btn-primary btn-block">{{ trans('journals.form.update') }}</button>
                            <button type="submit" name="post_action" value="update_publish" class="btn btn-default btn-block">{{ trans('journals.form.update_publish') }}</button>
                            <button type="submit" name="post_action" value="update_draft" class="btn btn-default btn-block">{{ trans('journals.form.update_draft') }}</button>
                        @endif
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading"><div class="panel-title h4">{{ trans('journals.form.additional') }}</div></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="control-label col-sm-4">
                                {!! trans('journals.form.category') !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('journal_category_id', $journal_category, null, [ 'class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="control-label col-sm-4">
                                {!! trans('journals.form.specialist') !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('specialist', $journal_specialist, null, [ 'class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
							<label class="control-label col-sm-4"></label>
							<div class="col-sm-8">
								<div class="well" style="margin-bottom:0;max-height:200px;overflow-y:auto;padding-top:10px;">
									<div class="checkbox">
										<label>
                                            <?php
                                                if ($journal->id) $free = $journal->is_free ?: false;
                                                else $free = false;
                                            ?>
			                                {!! Form::checkbox('is_free', 1, $free) !!} {{ trans('journals.form.free') }}
										</label>
									</div>
								</div>
							</div>
						</div>

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading"><div class="panel-title h4">{{ trans('journals.form.cover') }}</div></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                    {!! Form::file('image_url', ['class' => 'hide' , 'id' => 'image_url']) !!}
                                    <input type="checkbox" name="remove_pict" class='hide' value="1">
                                    <div class="logo-image has-overlay col-sm-12 <?= ($journal->id && !empty($journal->image_url)) ? '' : 'hide' ?>" id='preview_div'>
                                        <img src="<?= !empty($journal->image_url) ? url('journal/files/image/') . '/' . $journal->image_url : '' ?>" style="width: 100%;" id="image_view">
        								<div class="overlay" style="border-radius: 5px;"></div>
        					            <span class="btn-remove text-center">
        						            <button type="button" id='remove_image_btn' class="btn btn-danger"><b class="glyphicon glyphicon-remove"></b> Remove Cover</button>
        					            </span>
                                    </div>

                                    <div class="logo-image has-overlay col-sm-12 <?= empty($journal->image_url) ? '' : 'hide' ?>" id='browse_div'>
                                        <img src="/media/images/placeholder.png" style="width: 100%;">
                                        <span class="btn-remove text-center">
                                            <label for='image_url' class="btn btn-success">
                                                Browse Photo
                                            </label>
                                        </span>
                                    </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

	{!! Form::close() !!}

    <script type="text/javascript">
        var image_url   = document.getElementById('image_url') ,
            image_view  = document.getElementById('image_view') ,
            browse_div  = document.getElementById('browse_div') ,
            preview_div = document.getElementById('preview_div') ,
            remove_btn  = document.getElementById('remove_image_btn') ,
            remove_pict = document.getElementsByName('remove_pict')[0];

        image_url.onchange = function( e ) {
            if (this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    image_view.setAttribute('src', e.target.result);
                    browse_div.classList.add('hide');
                    preview_div.classList.remove('hide');
                }

                reader.readAsDataURL(this.files[0]);
            }
        };

        remove_btn.onclick = function( e ) {
            image_view.setAttribute('src','');
            browse_div.classList.remove('hide');
            preview_div.classList.add('hide');

            image_url.value     = '';
            remove_pict.checked = true;
        }
    </script>

@endsection
