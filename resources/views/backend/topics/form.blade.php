@extends('layout.backend')

@section('title')
	{{ trans('topics.form_' . ($topic->id ? 'edit' : 'add')) }}
@endsection

@section('nav_content')

	<div class="navbar-text navbar-back">

		<a href="/backend/topics" title="Topics">
			<b class="glyphicon glyphicon-chevron-left"></b>
		</a>

	</div>
	<div class="navbar-text navbar-title">
		{{ trans('topics.form_' . ($topic->id ? 'edit' : 'add')) }}
	</div>
@endsection

@section('headscript')
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.structure.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.theme.min.css') }}">
	<script type="text/javascript" src="{{ asset('libs/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/js/tinymce/tinymce.min.js') }}" charset="utf-8"></script>
    <script type="text/javascript">
        tinymce.init({ selector:'textarea#summary' });
    </script>
	<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}" media="screen" charset="utf-8">
	<link rel="stylesheet" href="{{ asset('assets/css/content-list.css') }}" media="screen" charset="utf-8">
@endsection

@section('content')

	{!! Form::model($topic, ['files'=> true , 'url' => 'backend/topic/' . ($topic->id ? $topic->slug . '/edit' : 'new') . ($request->redirect ? '/?redirect=' . urlencode($request->redirect) : ''), 'class' => 'form-horizontal', 'novalidate' => 'novalidate', 'id' => 'formTopic']) !!}
		@if ($topic->id)
			{!! Form::hidden('id') !!}
			{!! Form::hidden('slug') !!}
		@endif
		<div class="row">
			<div class="col-sm-8">
				<div class="form-group">
					<div class="col-sm-12">
						{!! Form::text('title', null, ['class' => 'form-control input-lg seamless', 'placeholder' => trans('topics.form_title')]) !!}
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="panel-title h4">
							{!! trans('topics.form_detail') !!}
							<a class="pull-right" data-toggle="collapse" href="#topicDetail"><b class="glyphicon glyphicon-menu-down"></b></a>
						</div>
					</div>
					<div class="panel-body collapse in" id="topicDetail">

                        <div class="form-group">
							<label class="control-label col-sm-2">{{ trans('topics.form_summary') }}</label>
							<div class="col-sm-10">
                                {!! Form::textarea('summary', null, ['id' => 'summary']) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-2">{{ trans('topics.form_overview') }}</label>
							<div class="col-sm-10">
                                {!! Form::textarea('meta_desc', null, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-2">{{ trans('topics.form_release') }}</label>
							<div class="col-sm-10">
								{!! Form::number('release', null, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-2">{{ trans('topics.form_preview') }}</label>
							<div class="col-sm-10">
                                {!! Form::url('preview', null, ['class' => 'form-control', 'placeholder' => 'Youtube link...']) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-2">{{ trans('topics.form_tags')}}</label>
							<div class="col-sm-10">
								<textarea name="tags" class='form-control' placeholder="Tags here...">{!! ($topic->id ? $tags : null ) !!}</textarea>
								{!! trans('topics.tags_suggestion') !!}
							</div>
						</div>

					</div>
				</div>

                @include('backend.topics.lectures.form-lecture')
				@include('backend.topics.contents.form-content')
				@include('backend.topics.form-faq')

			</div>

            <div class="col-sm-4">
				<div class="form-group">
					<div class="col-xs-12">
						@if (!$topic->id)
							<button type="submit" name="post_action" value="all" class="btn btn-primary btn-block">{{ trans('topics.form_def_save') }}</button>
							<button type="submit" name="post_action" value="new" class="btn btn-default btn-block">{{ trans('topics.form_def_save_add_other') }}</button>
						@else
							<button type="submit" name="post_action" value="all" class="btn btn-primary btn-block">{{ trans('topics.form_def_update') }}</button>
							<button type="submit" name="post_action" value="edit" class="btn btn-default btn-block">{{ trans('topics.form_def_update_continue') }}</button>
						@endif
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading"><div class="panel-title h4">{{ trans('topics.form_access') }}</div></div>
					<div class="panel-body">
						<div class="form-group">
							<label class="control-label col-sm-2">{{ trans('topics.form_publish') }}</label>
							<div class="col-sm-10">
								<?php $post_status = $topic->id ? $topic->post_status == 1 ?: false : false; ?>
								{!! Form::checkbox('post_status', 1, $post_status) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-2"></label>
							<div class="col-sm-10">
								<div class="well" style="margin-bottom:0;max-height:200px;overflow-y:auto;padding-top:10px;">
									<div class="checkbox">
										<label>
											<?php $open_access = $topic->id ? $topic->is_open_access == 1 ?: false : false; ?>
			                                {!! Form::checkbox('is_open_access', 1, $open_access) !!} {{ trans('topics.form_open_access') }}
										</label>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-2"></label>
							<div class="col-sm-10">
								<div class="well" style="margin-bottom:0;max-height:200px;overflow-y:auto;padding-top:10px;">
									<div class="checkbox">
										<label>
											<?php $p2kb = $topic->id ? $topic->is_p2kb == 1 ?: false : false; ?>
											{!! Form::checkbox('is_p2kb', 1, $p2kb) !!} {{ trans('topics.form_is_p2kb') }}
										</label>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group <?= $p2kb ? '' : 'hide' ?>" id="formP2KBURL">
							<label class="control-label col-sm-2">{{ trans('topics.form_p2kb_url') }}</label>
							<div class="col-sm-10">
								{!! Form::text('p2kb_url', null, [ 'class' => 'form-control']) !!}
							</div>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading"><div class="panel-title h4">{{ trans('topics.form_certificate') }}</div></div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-12">
								<div class="well" style="margin-bottom:0;max-height:200px;overflow-y:auto;padding-top:10px;">
									<div class="checkbox">
										<label>
											<?php $certificatefree = $topic->id ? $topic->is_free_certificate == 1 ?: false : false; ?>
											{!! Form::checkbox('is_free_certificate', 1, $certificatefree) !!} {{ trans('topics.form_is_free_certificate') }}
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading"><div class="panel-title h4">{{ trans('topics.form_category') }}</div></div>
					<div class="panel-body">
						<div class="form-group">
							<center>
								<div class="col-sm-12">
									{!! Form::select('category', $category, null, [ 'class' => 'form-control', 'style' => 'width: 70%;']) !!}
								</div>
							</center>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
                    <div class="panel-heading"><div class="panel-title h4">{{ trans('topics.form_featured_image') }}</div></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                {!! Form::file('featured_image', ['class' => 'hide' , 'id' => 'featured_image']) !!}
                                <input type="checkbox" name="remove_pict" class='hide' value="1">
                                <div class="logo-image has-overlay col-sm-12 <?= ($topic->id && !empty($topic->featured_image)) ? '' : 'hide' ?>" id='preview_div'>
                                    <img src="<?= !empty($topic->featured_image) ? url('topic/files') . '/' . $topic->featured_image : '' ?>" style="width: 100%;" id="image_view">
    								<div class="overlay" style="border-radius: 5px;"></div>
    					            <span class="btn-remove text-center">
    						            <button type="button" id='remove_image_btn' class="btn btn-danger"><b class="glyphicon glyphicon-remove"></b> Remove Cover</button>
    					            </span>
                                </div>

                                <div class="logo-image has-overlay col-sm-12 <?= empty($topic->featured_image) ? '' : 'hide' ?>" id='browse_div'>
                                    <img src="/media/images/placeholder.png" style="width: 100%;">
                                    <span class="btn-remove text-center">
                                        <label for='featured_image' class="btn btn-success">
                                            Browse Photo
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

				@include('backend.topics.form-disease')

			</div>
		</div>

	{!! Form::close() !!}

	<script type="text/javascript">

		var image_url   = document.getElementById('featured_image') ,
            image_view  = document.getElementById('image_view') ,
            browse_div  = document.getElementById('browse_div') ,
            preview_div = document.getElementById('preview_div') ,
            remove_btn  = document.getElementById('remove_image_btn') ,
            remove_pict = document.getElementsByName('remove_pict')[0];

        image_url.onchange = function( e ) {
            if (this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    image_view.setAttribute('src', e.target.result);
                    browse_div.classList.add('hide');
                    preview_div.classList.remove('hide');
                }

                reader.readAsDataURL(this.files[0]);
            }
        };

        remove_btn.onclick = function( e ) {
            image_view.setAttribute('src','');
            browse_div.classList.remove('hide');
            preview_div.classList.add('hide');

            image_url.value     = '';
            remove_pict.checked = true;
        }


		var formP2KBURL = document.getElementById('formP2KBURL') ,
			isP2KB		= document.getElementsByName('is_p2kb')[0];

		isP2KB.onclick = function(e){
			formP2KBURL.classList.toggle('hide');
		}
	</script>

@endsection
