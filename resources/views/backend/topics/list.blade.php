@extends('layout.backend')

@section('title')
	{{ trans('topics.list_title') }}
@endsection

@section('nav_content')

	<div class="navbar-text navbar-back">
		<a href="/backend/topics" title="{{ trans('topics.back_dashboard') }}">
            <b class="glyphicon glyphicon-chevron-left"></b>
        </a>
	</div>

    <div class="navbar-text navbar-title">{{ trans('topics.list_title') }}</div>

    <a href="/backend/topic/new?redirect=list" class="btn navbar-btn navbar-left">
        <b class="glyphicon glyphicon-plus"></b>&ensp;{{ trans('topics.add') }}
    </a>
	{!! Form::open(['url' => 'backend/topic/all', 'class' => 'navbar-form navbar-left','method' => 'get']) !!}
		<div class="form-group">
			<div class="input-group">
				{!! Form::text('s', Request::input('s'), ['class' => 'form-control', 'placeholder' => trans('topics.list_search'), 'style' => 'width:300px;']) !!}
				<span class="input-group-btn">
					<button type="submit" class="btn btn-default"><b class="glyphicon glyphicon-search"></b></button>
					@if (Request::input('s'))
                        <a href="/backend/topic/all" class="btn btn-default">{{ trans('topics.list_search_clear') }}</a>
                    @endif
				</span>
			</div>
		</div>
	{!! Form::close() !!}

@endsection

@section('content')
	<?php $lastId = 0; ?>
	@if (count($topics) > 0 || strlen(Request::input('s')) > 0)

		<div class="panel panel-default panel-table">
			<table class="table {{ count($topics) > 0 ? "table-hover" : "" }}" id="listTable">
				<tbody>
					@if(count($topics) == 0)
						<tr><td colspan="3" class="text-center" style="padding-top: 30px;padding-bottom: 30px;">{{ trans('topics.list_search_empty', ['term' => Request::input('s')]) }}</td></tr>
					@else
						@foreach ($topics as $topic)
							<tr>
								<td>
									<div>{{ $topic->title }} - {{ $topic->category }} </div>
									<a href="/backend/topic/{{ $topic->slug }}/edit">{{ trans('topics.edit') }}</a> / <a href="/backend/topic/{{ $topic->slug }}/trash" class="text-danger">{{ trans('topics.trash') }}</a>
								</td>
								<td style="max-width:450px;">
									<div class="ellipsis">
										{{ $topic->is_open_access ? 'Public' : 'Suscribe' }}
									</div>
								</td>
								<td style="width: 150px;">
									<div>{{ $topic->createdBy->name }}</div>
									<div>{{ $topic->created_at }}</div>
								</td>
							</tr>
                            <?php $lastId = $topic->id; ?>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>

		@if (!Request::input('s'))
            @if( !$hide )
    			<p class="text-center">
    				<button type="button" id="loadMoreBtn" class="btn btn-default">{{ trans('app.load_more') }}</button>
    			</p>
            @endif
		@endif

	@else

		<p>{{ trans('topics.list_none') }} <a href="/backend/topic/new">{{ trans('topics.add') }}</a></p>
	@endif

@endsection

@section('endscript')

	<script type="text/javascript">
		var loadMoreBtn = document.getElementById('loadMoreBtn') ,
            listTable   = document.getElementById('listTable') ,
            bodyTable   = listTable.querySelector('tbody');

        var formData  = new FormData() ,
            myHeaders = new Headers();

        var lastId    = "{{ $lastId }}";

        myHeaders.append("X-Requested-With", "XMLHttpRequest");

        var myInit = {
            method  : 'post' ,
            headers : myHeaders ,
            body    : formData
        };

        loadMoreBtn.onclick = function( e ){
            formData.append( 'last_id' , lastId );

            var xmlhttp = new XMLHttpRequest();

            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var data = JSON.parse( xmlhttp.responseText );

                    lastId = (data.last_id > 0) ? data.last_id : 0;

                    bodyTable.insertAdjacentHTML( 'beforeend' , data.html );

                    if (lastId == 0)
                        loadMoreBtn.classList.add('hidden');

					loadMoreBtn.removeAttribute('disabled');
               }
            };
            xmlhttp.open("POST", "{{ $app->make('url')->to('/backend/topic/all') }}", true);
            xmlhttp.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            xmlhttp.send(formData);

			loadMoreBtn.setAttribute('disabled' , true);
        }
	</script>
@endsection
