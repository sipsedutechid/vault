@extends('layout.backend')

@section('title')
	{{ trans('topics.title') }}
@endsection

@section('nav_content')

	<div class="navbar-text navbar-title">{{ trans('topics.title') }}</div>
	<a href="/backend/topic/new" class="btn navbar-btn"><b class="glyphicon glyphicon-plus"></b>&ensp;{{ trans('topics.add') }}</a>

@endsection

@section('content')

	<div class="row">
		<div class="col-lg-6">
				<div class="panel panel-default">
					<div class="panel-body h3">
						<a href="/backend/topic/all">
							{{ trans('topics.home_all') }} <span class="badge pull-right">{{ $topic_count }}</span>
						</a>
					</div>
					<table class="table table-condensed">
						<thead><tr><th colspan="2">{{ trans('topics.home_all_recent') }}</th></tr></thead>
						<tbody>
							@foreach($topic_recents as $topic)
								<tr>
									<td style="width:100px;">{{ $topic->created_at }}</td>
									<td><a href="/backend/topic/{{ $topic->slug }}/edit?redirect={{ urlencode('backend/topics') }}">{{ $topic->title }}</a></td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</a>
		</div>
		<div class="col-lg-3">
			<a href="/backend/topic/contents">
				<div class="panel panel-default">
					<div class="panel-body h4">
							{{ trans('topics.home_all_content') }} <span class="badge pull-right">{{ $content_count }}</span>
					</div>
				</div>
			</a>
		</div>
		<div class="col-lg-3">
			<a href="/backend/topic/lectures">
				<div class="panel panel-default">
					<div class="panel-body h4">
							{{ trans('topics.home_all_lecture') }} <span class="badge pull-right">{{ $lecture_count }}</span>
					</div>
				</div>
			</a>
		</div>
		<div class="col-lg-3">
			<a href="/backend/disease/all?ref=topics">
				<div class="panel panel-default">
					<div class="panel-body h4">
							{{ trans('topics.home_all_disease') }} <span class="badge pull-right">{{ $disease_count }}</span>
					</div>
				</div>
			</a>
		</div>
	</div>

@endsection
