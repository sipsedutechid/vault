@extends('layout.backend')

@section('title') Move {{ $topic->name }} to Trash @endsection

@section('content')

	<h1>Move to Trash</h1>
	{!! Form::open(['url' => 'backend/topic/trash']) !!}
		<p>
			You are about to move the topic <strong>{{ $topic->title }} ({{ $topic->level->name }})</strong> to trash.<br />
			Are you sure? This action cannot be undone.
		</p>
		{!! Form::hidden('slug', $topic->slug) !!}
		<a href="/backend/topic/all" class="btn btn-default">No, take me back</a>
		<button type="submit" class="btn btn-danger">Yes, move this topic to trash</button>

	{!! Form::close() !!}

@endsection
