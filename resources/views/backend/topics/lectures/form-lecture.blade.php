<script src="{{ asset('assets/js/dynamicForm.js') }}" charset="utf-8"></script>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title h4">
            {{ trans('topics.form_lecture') }}
            <a class="pull-right" data-toggle="collapse" href="#topicLecture"><b class="glyphicon glyphicon-menu-down"></b></a>
        </div>
    </div>
    <div class="panel-body collapse in" id="topicLecture">
        {{-- <div id="lectureFields" class="hidden">
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('topics.form_lecture_name') }}</label>
                <div class="col-sm-9">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <input type="hidden" name="lecture_id">
                            <input type="hidden" name="lecture_data_status" value="new">
                            <input type="text" name="lecture_front_title" class="form-control input-lg seamless" placeholder="{{ trans('topics.form_lecture_front_title') }}" />
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="lecture_name" class="form-control input-lg seamless" placeholder="{{ trans('topics.form_lecture_name') }}" />
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="lecture_back_title" class="form-control input-lg seamless" placeholder="{{ trans('topics.form_lecture_back_title') }}" />
                        </div>

                    </div>
                </div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('topics.form_lecture_bio') }}</label>
				<div class="col-sm-9">
					<textarea name="lecture_bio" class='form-control' rows="6"></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-3">
					<button type="button" id="cancelLectureBtn" class="btn btn-danger btn-normal">{{ trans('topics.form_def_cancel') }}</button>
					<button type="button" id="submitLectureBtn" class="btn btn-primary btn-normal">{{ trans('topics.form_def_add') }}</button>
				</div>
			</div>
		</div> --}}
        {{-- <button id="addLectureButton" type="button" class="btn btn-default">{{ trans('topics.form_add_lecture') }}</button> --}}
        <button id="addLectureButton" type="button" class="btn btn-primary" data-toggle='modal' data-target='#newLectureModal'>{{ trans('topics.form_new_lecture') }}</button>
        <button id="chooseLectureButton" type="button" class="btn btn-default" data-toggle='modal' data-target='#chooseLectureModal'>{{ trans('topics.form_choose_lecture') }}</button>
		{{-- <button id="addLectureButton" type="button" class="btn btn-default">{{ trans('topics.form_add_lecture') }}</button> --}}
		<table class="table table-bordered hidden" id="lectures" style="margin-top:15px;">
			<thead>
                <tr>
                    <th>{{ trans('topics.form_lecture_name') }}</th>
                    <th style="width:50px;"></th>
                </tr>
            </thead>
			<tbody></tbody>
		</table>
    </div>
</div>

@section('modals')
    @@parent
    <div class="modal fade" id="newLectureModal" tabindex="-1" role="dialog" aria-labelledby="newLectureModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="newLectureModalLabel">{{ trans('topics.form_lecture_modal_title') }}</h4>
                </div>
                <div class="modal-body">
                    <form id="lecture-form" enctype="multipart/form-data" method="post" >
                        <div class="form-group">
                            <div class="input-group">
                                <input type="hidden" name="lecture_id">
                                <input type="hidden" name="lecture_data_status" value="new">
                                <input type="text" name="lecture_front_title" class="form-control input-lg seamless" style='width: 20%' placeholder="{{ trans('topics.form_lecture_front_title') }}" />
                                <input type="text" name="lecture_name" class="form-control input-lg seamless" style='width: 58%; margin-left: 1%; margin-right: 1%;' placeholder="{{ trans('topics.form_lecture_name') }}" />
                                <input type="text" name="lecture_back_title" class="form-control input-lg seamless" style='width: 20%' placeholder="{{ trans('topics.form_lecture_back_title') }}" />
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <label class="control-label">{{ trans('topics.form_lecture_bio') }}</label>
                            <textarea name="lecture_bio" class='form-control' rows="6"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">{{ trans('topics.form_lecture_photo') }}</label>
                            <input type="file" name="lecture_photo">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('topics.form_def_cancel') }}</button>
                    <button type="button" id="save-lecture-button" name="button" class="btn btn-primary">
                        <span id="lecture-modal-save"> {{ trans('topics.form_def_save') }} </span>
                        <span id="lecture-modal-saving" style="display:none;"> <i class='fa fa-circle-o-notch fa-spin'></i> {{ trans('topics.form_def_saving') }}  </span>
                        <span id="lecture-modal-success-saving" style="display:none"> {{ trans('topics.form_def_success_saving') }} </span>
                        <span id="lecture-modal-failed-saving" style="display:none"> {{ trans('topics.form_def_failed_saving') }} </span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="chooseLectureModal" tabindex="-1" role="dialog" aria-labelledby="chooseLectureLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="chooseLectureLabel">{{ trans('topics.form_lecture_modal_choose') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="">
                        <form id="search-lecture-form" enctype="multipart/form-data" method="post" >
                            <div class="input-group">
                                <input type="text" name='lecture_search_keyword' class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                    <button type='button' class="btn btn-default" type="button" id="search-lecture-btn">Go!</button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <div id="list-lecture-found">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('topics.form_def_cancel') }}</button>
                    <button type="button" id="btn-insert-lecture" name="button" class="btn btn-primary">
                        {{ trans('topics.form_lecture_insert') }}
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var btnSaveLecture  = document.getElementById('save-lecture-button') ,
            ltrModalSave    = document.getElementById('lecture-modal-save') ,
            ltrModalSaving  = document.getElementById('lecture-modal-saving') ,
            ltrModalSuccess = document.getElementById('lecture-modal-success-saving') ,
            ltrModalFailed  = document.getElementById('lecture-modal-failed-saving');

        btnSaveLecture.onclick = function(){
            this.setAttribute('disabled', 'true');
            ltrModalSave.style.display = 'none';
            ltrModalSaving.style.display = 'block';
            postDatalecture();
        }

        function postDatalecture(){
            var formData = new FormData(document.querySelector('form#lecture-form'));

            var myInit = {
                method: 'post' ,
                body: formData
            };

            fetch( "{{ $app->make('url')->to('/service/lecture/add') }}", myInit )
                .then(function(response) {
                    return response.json();
                })
                .then(function(data) {
                    ctnModalSaving.style.display    = 'none';
                    btnModalSave.classList.remove('btn-primary');

                    if (data.status) {
                        ltrModalSuccess.style.display = 'block';

                        btnSaveLecture.classList.add('btn-success');

                        resultLecture.setResult([
                            {tag: 'input',      key: 'id',          val: data.lecture.id ,          hide: true } ,
                            {tag: 'input',      key: 'front_title', val: data.lecture.front_title , hide: true , splitter: '' , parent: 'id'} ,
                            {tag: 'input',      key: 'name',        val: data.lecture.name ,        hide: true , splitter: '' , parent: 'id'} ,
                            {tag: 'input',      key: 'back_title',  val: data.lecture.back_title ,  hide: true , splitter: '' , parent: 'id'} ,
                            {tag: 'input',      key: 'full_name',   val: data.lecture.front_title + ' ' + data.lecture.name + ' ' + data.lecture.back_title , label: data.lecture.front_title + ' ' + data.lecture.name + ' ' + data.lecture.back_title , parent: 'id' , splitter: '' } ,
                            {tag: 'input',      key: 'data_status', val: "new",                     parent: 'id' , splitter: '' , hide: true}
                        ]);

                        setTimeout(function () {
                            $('#newLectureModal').modal('hide');
                            resetFormModalLecture();
                        }, 1000);
                    } else {
                        ltrModalFailed.style.display = 'block';

                        btnSaveLecture.classList.add('btn-danger');

                        setTimeout(function () {
                            resetFormModalLecture();
                        }, 1000);
                    }
                }).catch(function(err) {
                	console.log(err);
                });
        }

        function resetFormModalLecture(){
            setTimeout(function () {
                btnSaveLecture.classList.remove('btn-danger');
                btnSaveLecture.classList.remove('btn-success');

                btnSaveLecture.classList.add('btn-primary');

                ltrModalSave.style.display      = 'block';
                ltrModalFailed.style.display    = 'none';
                ltrModalSuccess.style.display   = 'none';
                ltrModalSaving.style.display    = 'none';

                document.querySelector('form#lecture-form').reset();

                btnSaveLecture.removeAttribute('disabled');
            }, 500);
        }

        var resultLecture = new formDynamicGakken('lecture');

        var lectures = [
            @if ( old('lecture') || $topic->id )
                <?php
                    $lectures       = old('lecture') ? json_decode(json_encode(old('lecture'))) : $topic->lectures;
                    $current_status = old('lecture') ? 'new' : 'old';
                ?>
        		@foreach ($lectures as $lecture)
        			[
                        {
                            tag : 'input',
                            key : 'id',
                            val : "{{ $lecture->id }}" ,
                            hide: true
                        } ,
                        {
                            tag     : 'input',
                            key     : 'front_title',
                            val     : "{{ $lecture->front_title }}" ,
                            hide    : true ,
                            parent  : 'id' ,
                            splitter: ''
                        } ,
                        {
                            tag     : 'input',
                            key     : 'name',
                            val     : "{{ $lecture->name }}" ,
                            hide    : true ,
                            parent  : 'id' ,
                            splitter: ''
                        } ,
                        {
                            tag     : 'input',
                            key     : 'back_title',
                            val     : "{{ $lecture->back_title }}" ,
                            hide    : true ,
                            parent  : 'id' ,
                            splitter: ''
                        } ,
                        {
                            tag  : 'input',
                            key  : 'full_name',
                            val  : "{{ $lecture->front_title . ' ' . $lecture->name . ' ' . $lecture->back_title }}" ,
                            label: "{{ $lecture->front_title . ' ' . $lecture->name . $lecture->back_title }}" , parent: 'id' , splitter: ''
                        } ,
                        {
                            tag     : 'input',
                            key     : 'data_status',
                            val     : "{{ $current_status }}",
                            parent  : 'id' ,
                            splitter: '' ,
                            hide    : true
                        }
                    ] ,
        		@endforeach
        	@endif
        ];

        resultLecture.setElement({
            table_result: '#lectures'
        });

        lectures.forEach( function( data) {
            resultLecture.setResult(data);
        });
    </script>

    <script type="text/javascript">
        //handling modal choosing lecture;
        var btnSrcLecture       = document.getElementById('search-lecture-btn') ,
            btnInsertLecture    = document.getElementById('btn-insert-lecture') ,
            listLectureFound    = document.getElementById('list-lecture-found') ,
            srcLectureKeyword   = document.getElementsByName('lecture_search_keyword')[0] ,
            formLecture         = document.querySelector('form#lecture-form') ,
            formSearchLecture   = document.querySelector('form#search-lecture-form') ,
            chooseBtnLecture    = document.getElementById('chooseLectureButton');

        chooseBtnLecture.addEventListener('click', function(){
            listLectureFound.innerHTML  = '';
            srcLectureKeyword.value     = '';
        });

        var searchLecture = function ( e ) {
            if ( srcLectureKeyword.value.length < 4 ) return false;

            var formData = new FormData();

            formData.append( 'keyword' , srcLectureKeyword.value );

            var myInit = {
                method  : 'post' ,
                body    : formData
            };

            fetch( "{{ $app->make('url')->to('/service/lecture/search') }}", myInit )
                .then(function(response) {
                    return response.text();
                })
                .then(function(data) {
                    listLectureFound.innerHTML = data;
                }).catch(function(err) {
                    console.log(err);
                });

            return false;
        }

        btnSrcLecture.onclick = searchLecture;
        formSearchLecture.onsubmit  = searchLecture;

        btnInsertLecture.onclick = function() {
            var lectures = document.getElementsByName('lecture');

            $('#chooseLectureModal').modal('hide');

            for (var index in lectures) {
                if (lectures.hasOwnProperty(index)) {
                    if ( lectures[index].checked ) {
                        var frontTitle  = document.getElementsByName('lecture_r_front_title[' + lectures[index].value + ']')[0] ,
                            name        = document.getElementsByName('lecture_r_name[' + lectures[index].value + ']')[0] ,
                            backTitle   = document.getElementsByName('lecture_r_back_title[' + lectures[index].value + ']')[0];

                        resultLecture.setResult([
                            { tag: 'input', key : 'full_name' ,  val : frontTitle.value + ' ' + name.value + ' ' + backTitle.value ,         label: frontTitle.value + ' ' + name.value + ' ' + backTitle.value } ,
                            { tag: 'input', key : 'front_title', val : frontTitle.value , hide: true , splitter: '' , parent: 'full_name'} ,
                            { tag: 'input', key : 'name',        val : name.value ,        hide: true , splitter: '' , parent: 'full_name'} ,
                            { tag: 'input', key : 'back_title',  val : backTitle.value ,  hide: true , splitter: '' , parent: 'full_name'} ,
                            { tag: 'input', key : 'id' ,         val : lectures[index].value , label: lectures[index].value , hide: true, parent: 'full_name'} ,
                            { tag: 'input', key : 'data_status', val : "new",                 parent: 'full_name' , splitter: '' , hide: true}
                        ]);
                    }
                }
            }
        }
    </script>
@endsection
