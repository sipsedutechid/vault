<div class="row space15px">
    @foreach( $lectures as $lecture )

        <div class="col-sm-6 col-md-4">
            <input type="checkbox" id='lecture_{{ $lecture->id }}' name="lecture" class='content-box' style='display: none;' value="{{ $lecture->id }}">
            <label for="lecture_{{ $lecture->id }}" class="content-list thumbnail">
                <i class="fa fa-check-circle floating-checked fa-2x"></i>
                @if( $lecture->photo )
                    <img src="{{ url('lecture/photo/' . $lecture->photo) }}" alt="" />
                @endif
                <div class="caption">
                    <center> <h3>{{ $lecture->front_title . ' ' . $lecture->name . ' ' . $lecture->back_title }}</h3> </center>
                </div>
                <input type="hidden" name="lecture_r_front_title[{{ $lecture->id }}]" value="{{ $lecture->front_title }}" readonly="true">
                <input type="hidden" name="lecture_r_name[{{ $lecture->id }}]" value="{{ $lecture->name }}" readonly="true">
                <input type="hidden" name="lecture_r_back_title[{{ $lecture->id }}]" value="{{ $lecture->back_title }}" readonly="true">
                <textarea name="lecture_r_bio[{{ $lecture->bio }}]" class='hidden' rows="8" cols="40" readonly="true"></textarea>
            </label>
        </div>

    @endforeach
</div>
