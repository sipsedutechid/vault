<div class="list-group space15px">
    @foreach( $contents as $content )
        <input type="checkbox" id='content_{{ $content->id }}' name="content" class='content-box' style='display: none;' value="{{ $content->id }}">
        <label for="content_{{ $content->id }}" class="content-list">
            <i class="fa fa-check-circle floating-checked fa-2x"></i>
            <h4 class="list-group-item-heading">{{ $content->title }}</h4>
            <input type="hidden" name="content_r_title[{{ $content->id }}]" value="{{ $content->title }}">
            <input type="hidden" name="content_r_appearance[{{ $content->id }}]" value="{{ $content->appearance }}">
            <input type="hidden" name="content_r_type[{{ $content->id }}]" value="{{ $content->type }}">
            <div>
                @if ( $content->type === 'text' )
                    {!! $content->data !!}
                @elseif ( $content->type === 'link' )
                    {{ $content->data }}
                @elseif ( $content->type === 'embed' )
                    {{ $content->data }}
                @elseif ( $content->type === 'file' )
                    <?php
                        $file_info = pathinfo($content->data);
                        switch ( $file_info['extension'] ) {
                            case 'pdf':
                                echo "<object data='" . url('content/files/' . $content->data) . "'></object>";
                                break;

                            default:
                                echo "<img src='" . url('content/files/' . $content->data) . "'>";
                                break;
                        }
                    ?>
                @endif


            </div>
        </label>
    @endforeach
</div>
