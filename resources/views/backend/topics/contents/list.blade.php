@extends('layout.backend')

@section('title')
	{{ trans('contents.list_title') }}
@endsection

@section('nav_content')

	<div class="navbar-text navbar-back">
		<a href="/backend/topics" title="{{ trans('contents.back_dashboard') }}">
            <b class="glyphicon glyphicon-chevron-left"></b>
        </a>
	</div>

    <div class="navbar-text navbar-title">{{ trans('contents.list_title') }}</div>

    <a href="/backend/topic/content/new?redirect=list" class="btn navbar-btn navbar-left">
        <b class="glyphicon glyphicon-plus"></b>&ensp;{{ trans('contents.add') }}
    </a>
	{!! Form::open(['url' => 'backend/topic/content/all', 'class' => 'navbar-form navbar-left','method' => 'get']) !!}
		<div class="form-group">
			<div class="input-group">
				{!! Form::text('s', Request::input('s'), ['class' => 'form-control', 'placeholder' => trans('topics.list_search'), 'style' => 'width:300px;']) !!}
				<span class="input-group-btn">
					<button type="submit" class="btn btn-default"><b class="glyphicon glyphicon-search"></b></button>
					@if (Request::input('s'))
                        <a href="/backend/content/all" class="btn btn-default">{{ trans('contents.list_search_clear') }}</a>
                    @endif
				</span>
			</div>
		</div>
	{!! Form::close() !!}

@endsection

@section('content')
	<?php $lastId = 0; ?>
	@if (count($contents) > 0 || strlen(Request::input('s')) > 0)

		<div class="panel panel-default panel-table">
			<table class="table {{ count($contents) > 0 ? "table-hover" : "" }}" id="listTable">
				<tbody>
					@if(count($contents) == 0)
						<tr><td colspan="2" class="text-center" style="padding-top: 30px;padding-bottom: 30px;">{{ trans('topics.list_search_empty', ['term' => Request::input('s')]) }}</td></tr>
					@else
						@foreach ($contents as $content)
							<tr>
								<td>
									<div>{{ $content->title }}</div>
									<a href="/backend/topic/content/{{ $content->slug }}/edit">{{ trans('contents.edit') }}</a> / <a href="/backend/topic/content/{{ $content->slug }}/trash" class="text-danger">{{ trans('contents.trash') }}</a>
								</td>
								<td style="max-width:450px;">
									<div class="ellipsis" style="text-transform: capitalize;">
                                        {{ $content->type }}
									</div>
								</td>
							</tr>
                            <?php $lastId = $content->id; ?>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>

		@if (!Request::input('s'))
            @if( !$hide )
    			<p class="text-center">
    				<button type="button" id="loadMoreBtn" class="btn btn-default">{{ trans('app.load_more') }}</button>
    			</p>
            @endif
		@endif

	@else

		<p>{{ trans('contents.list_none') }} <a href="/backend/contents/new">{{ trans('contents.add') }}</a></p>
	@endif

@endsection

@section('endscript')

	<script type="text/javascript">
		var loadMoreBtn = document.getElementById('loadMoreBtn') ,
            listTable   = document.getElementById('listTable') ,
            bodyTable   = listTable.querySelector('tbody');

        var formData  = new FormData() ,
            myHeaders = new Headers();

        var lastId    = "{{ $lastId }}";

        myHeaders.append("X-Requested-With", "XMLHttpRequest");

        var myInit = {
            method  : 'post' ,
            headers : myHeaders ,
            body    : formData
        };

        loadMoreBtn.onclick = function( e ){
            formData.append( 'last_id' , lastId );

            var xmlhttp = new XMLHttpRequest();

            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var data = JSON.parse( xmlhttp.responseText );

                    lastId = (data.last_id > 0) ? data.last_id : 0;

                    bodyTable.insertAdjacentHTML( 'beforeend' , data.html );

                    if (lastId == 0)
                        loadMoreBtn.classList.add('hidden');

					loadMoreBtn.removeAttribute('disabled');
               }
            };
            xmlhttp.open("POST", "{{ $app->make('url')->to('/backend/topic/content/all') }}", true);
            xmlhttp.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            xmlhttp.send(formData);

			loadMoreBtn.setAttribute('disabled' , true);
        }
	</script>
@endsection
