@foreach ($contents as $content)
    <tr>
        <td>
            <div>{{ $content->title }}</div>
            <a href="/backend/topic/content/{{ $content->slug }}/edit">{{ trans('contents.edit') }}</a> / <a href="/backend/content/{{ $content->slug }}/trash" class="text-danger">{{ trans('contents.trash') }}</a>
        </td>
        <td style="max-width:450px;">
            <div class="ellipsis">

            </div>
        </td>
    </tr>
@endforeach
