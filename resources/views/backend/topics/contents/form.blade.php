@extends('layout.backend')

@section('title')
	{{ trans('contents.form_' . ($content->id ? 'edit' : 'add')) }}
@endsection

@section('nav_content')

	<div class="navbar-text navbar-back">

		<a href="/backend/topic/contents" title="Topics">
			<b class="glyphicon glyphicon-chevron-left"></b>
		</a>

	</div>
	<div class="navbar-text navbar-title">
		{{ trans('contents.form_' . ($content->id ? 'edit' : 'add')) }}
	</div>
@endsection

@section('headscript')
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.structure.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.theme.min.css') }}">
	<script type="text/javascript" src="{{ asset('libs/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/js/tinymce/tinymce.min.js') }}" charset="utf-8"></script>
    <script type="text/javascript">
        tinymce.init({ selector:'textarea#desc' });
    </script>
	<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}" media="screen" charset="utf-8">
	<link rel="stylesheet" href="{{ asset('assets/css/content-list.css') }}" media="screen" charset="utf-8">
@endsection

@section('content')

	{!! Form::model($content, ['files'=> true , 'url' => 'backend/topic/content/' . ($content->id ? $content->slug . '/edit' : 'new') . ($request->redirect ? '/?redirect=' . urlencode($request->redirect) : ''), 'class' => 'form-horizontal', 'novalidate' => 'novalidate', 'id' => 'formTopic']) !!}
		@if ($content->id)
			{!! Form::hidden('id') !!}
			{!! Form::hidden('slug') !!}
		@endif
		<div class="row">
			<div class="col-sm-8">
				<div class="form-group">
					<div class="col-sm-8">
						{!! Form::text('title', null, ['class' => 'form-control input-lg seamless', 'placeholder' => trans('contents.form_title')]) !!}
						{!! Form::hidden('type', null, []) !!}
					</div>
					<div class="col-sm-4">
						{!! Form::text('label', null, ['class' => 'form-control input-lg seamless', 'placeholder' => trans('contents.form_label')]) !!}
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="panel-title h4">
							{!! trans('contents.form_detail') !!}
							<a class="pull-right" data-toggle="collapse" href="#topicDetail"><b class="glyphicon glyphicon-menu-down"></b></a>
						</div>
					</div>
					<div class="panel-body collapse in" id="topicDetail">

                        <div class="form-group">
							<label class="control-label col-sm-2">{{ trans('contents.form_appearance') }}</label>
							<div class="col-sm-10">
                                {!! Form::select('appearance' , $appearance_option, null, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-2">{{ trans('contents.form_level') }}</label>
							<div class="col-sm-10">
                                {!! Form::select('level_id' , $level_option, null, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-2">{{ trans('contents.order') }}</label>
							<div class="col-sm-10">
                                {!! Form::number('order' , null, ['class' => 'form-control']) !!}
							</div>
						</div>
					</div>
				</div>

                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="{{ ($content->type == 'text') ? 'active' : '' }}">
                            <a href="#text" aria-controls="text" role="tab" data-content-type='text' data-toggle="tab">{{ trans('topics.form_content_text') }}</a>
                        </li>
                        <li role="presentation" class="{{ ($content->type == 'embed') ? 'active' : '' }}">
                            <a href="#embed" aria-controls="embed" role="tab" data-content-type='embed' data-toggle="tab">{{ trans('topics.form_content_embed') }}</a></li>
                        <li role="presentation" class="{{ ($content->type == 'link') ? 'active' : '' }}">
                            <a href="#link" aria-controls="link" role="tab" data-content-type='link' data-toggle="tab">{{ trans('topics.form_content_link') }}</a>
                        </li>
                        <li role="presentation" class="{{ ($content->type == 'file') ? 'active' : '' }}">
                            <a href="#file" aria-controls="file" role="tab" data-content-type='file' data-toggle="tab">{{ trans('topics.form_content_file') }}</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane {{ ($content->type == 'text') ? 'active' : '' }}" id="text">
                            <div class="">
                                <textarea class="form-control" id="desc" name="content_desc">{!! ($content->type == 'text') ? $content->data : '' !!}</textarea>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane {{ ($content->type == 'embed') ? 'active' : '' }}" id="embed">
                            <br>
                            <div class="">
                                <textarea class="form-control" name="content_embed" placeholder="{{ trans('topics.form_content_embed' )}}">{{ ($content->type == 'embed') ? $content->data : '' }}</textarea>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane {{ ($content->type == 'link') ? 'active' : '' }}" id="link">
                            <br>
                            <div class="">
                                <input type="text" class="form-control" name="content_link" placeholder="{{ trans('topics.form_content_url') }}" value="{{ ($content->type == 'link') ? $content->data : '' }}">
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane {{ ($content->type == 'file') ? 'active' : '' }}" id="file">
                            <br>
                            <div class="">
								@if($content->type =='file' && $content->id && !empty($content->data))
									<span id="spanContentFile">{{ $content->data }} <a id='removeFileContent'> {{ trans('topics.remove') }} </a> </span>
								@endif
								<input type="hidden" name="delete_file_content" value="0">
                                <input type="file" name="content_file" placeholder="{{ trans('topics.form_content_file') }}">
                            </div>
                        </div>
                    </div>
                </div>

				<div class="panel panel-default" style="margin-top:20px;">
					<div class="panel-heading">
						<div class="panel-title h4">
							Additional
							<a class="pull-right" data-toggle="collapse" href="#additional"><b class="glyphicon glyphicon-menu-down"></b></a>
						</div>
					</div>
					<div class="panel-body collapse in" id="additional">

                        <div class="form-group">
							<label class="control-label col-sm-2">URL JWPlayer</label>
							<div class="col-sm-10">
								{!! Form::text('url_jw' , null, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="well" style="margin-bottom:10px;max-height:200px;overflow-y:auto;padding-top:10px">
							<div class="checkbox">
								<label>
									<?php $is_free = $content->id ? $content->is_free == 1 ?: false : false; ?>
									{!! Form::checkbox('is_free' , '1', $is_free) !!} Free, can be access public
								</label>
							</div>
						</div>

						<div class="well" style="margin-bottom:0;max-height:200px;overflow-y:auto;padding-top:10px;">
							<div class="checkbox">
								<label>
									<?php $mediadata = $content->id ? $content->is_media_data == 1 ?: false : false; ?>
									{!! Form::checkbox('media_data' , '1', $mediadata) !!} Can be access trhough media and data page
								</label>
							</div>
						</div>

					</div>
				</div>

			</div>

            <div class="col-sm-4">
				<div class="form-group">
					<div class="col-xs-12">
						@if (!$content->id)
							<button type="submit" name="post_action" value="all" class="btn btn-primary btn-block">{{ trans('topics.form_def_save') }}</button>
							<button type="submit" name="post_action" value="new" class="btn btn-default btn-block">{{ trans('topics.form_def_save_add_other') }}</button>
						@else
							<button type="submit" name="post_action" value="all" class="btn btn-primary btn-block">{{ trans('topics.form_def_update') }}</button>
							<button type="submit" name="post_action" value="edit" class="btn btn-default btn-block">{{ trans('topics.form_def_update_continue') }}</button>
						@endif
					</div>
				</div>

			</div>
		</div>

	{!! Form::close() !!}

	<script type="text/javascript">
		var removeFileContent 	= document.getElementById('removeFileContent') ,
			fieldDeleteFile		= document.getElementsByName('delete_file_content')[0] ,
			spanContentFile		= document.getElementById('spanContentFile');

		if (removeFileContent) {
			removeFileContent.onclick = function( e ) {
				fieldDeleteFile.value = '1';
				spanContentFile.parentNode.removeChild(spanContentFile);
			}
		}

		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

	        document.getElementsByName('type')[0].value =  $(e.target).data('content-type');

	    });
	</script>

@endsection
