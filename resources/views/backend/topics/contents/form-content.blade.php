<script src="{{ asset('assets/js/dynamicForm.js') }}" charset="utf-8"></script>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title h4">
            {{ trans('topics.form_content') }}
            <a class="pull-right" data-toggle="collapse" href="#topicContent"><b class="glyphicon glyphicon-menu-down"></b></a>
        </div>
    </div>
    <div class="panel-body collapse in" id="topicContent">
		<button id="addContentButton" type="button" class="btn btn-primary" data-toggle='modal' data-target='#createContentModal'>{{ trans('topics.form_new_content') }}</button>
        <button id="chooseContentButton" type="button" class="btn btn-default" data-toggle='modal' data-target='#chooseContentModal'>{{ trans('topics.form_choose_content') }}</button>

		<table class="table table-bordered hidden" id="contents" style="margin-top:15px;">
			<thead>
                <tr>
                    <th>{{ trans('topics.form_content_title') }}</th>
                    <th>{{ trans('topics.form_content_type') }}</th>
                    <th style="width:50px;"></th>
                </tr>
            </thead>
			<tbody></tbody>
		</table>
    </div>
</div>

@section ('modals')
<div class="modal fade" id="createContentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">{{ trans('topics.form_content_modal_title') }}</h4>
            </div>
            <div class="modal-body">
                <form id="content-form" enctype="multipart/form-data" method="post" >
                    <div class="form-group">
                        <label class="control-label">{{ trans('topics.form_content_title') }}</label>
                        <input type="text" class="form-control" name="content_title">
                        <input type="hidden" name="content_type" value="text">
                    </div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('topics.form_content_label') }}</label>
                        <input type="text" name="content_label" class="form-control">
					</div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('topics.order') }}</label>
                        <input type="number" name="order" class="form-control">
					</div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('topics.form_content_appearance') }}</label>
                        <select class="form-control" name="content_appearance">
                            <option value="text">Text</option>
                            <option value="image">Image</option>
                            <option value="video">Video</option>
                            <option value="audio">Audio</option>
                            <option value="quiz">Quiz</option>
                            <option value="download">Download</option>
                            <option value="video-mobile">Video Mobile</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('topics.form_level') }}</label>
                        <select class="form-control" name="content_level_id">
                            @foreach($content_levels as $level)
                                <option value="<?= $level->id ?>"><?= $level->name ?></option>
                            @endforeach
                        </select>
                    </div>
                    <div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#text" aria-controls="text" role="tab" data-content-type='text' data-toggle="tab">{{ trans('topics.form_content_text') }}</a>
                            </li>
                            <li role="presentation">
                                <a href="#embed" aria-controls="embed" role="tab" data-content-type='embed' data-toggle="tab">{{ trans('topics.form_content_embed') }}</a></li>
                            <li role="presentation">
                                <a href="#link" aria-controls="link" role="tab" data-content-type='link' data-toggle="tab">{{ trans('topics.form_content_link') }}</a>
                            </li>
                            <li role="presentation">
                                <a href="#file" aria-controls="file" role="tab" data-content-type='file' data-toggle="tab">{{ trans('topics.form_content_file') }}</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="text">
                                <div class="form-group">
                                    <textarea class="form-control" id="content-desc" name="content_desc"></textarea>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="embed">
                                <br>
                                <div class="form-group">
                                    <label class="control-label">{{ trans('topics.form_content_embed') }}</label>
                                    <textarea class="form-control" name="content_embed"></textarea>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="link">
                                <br>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">{{ trans('topics.form_content_url') }}</label>
                                    <input type="text" class="form-control" name="content_link">
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="file">
                                <br>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">{{ trans('topics.form_content_file') }}</label>
                                    <input type="file" name="content_file">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">URL JWPlayer</label>
                        <input type="text" class="form-control" name="url_jw">
                    </div>
                    <div class="form-group">
                        <div class="well" style="margin-bottom:0;max-height:200px;overflow-y:auto;padding-top:10px;">
							<div class="checkbox">
								<label>
							        <input name="content_free" type="checkbox" value="1"> Free, can be access public
								</label>
							</div>
						</div>
					</div>
                    <div class="form-group">
                        <div class="well" style="margin-bottom:0;max-height:200px;overflow-y:auto;padding-top:10px;">
							<div class="checkbox">
								<label>
							        <input name="media_data" type="checkbox" value="1"> Can be access in media and data page
								</label>
							</div>
						</div>
					</div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('topics.form_def_cancel') }}</button>
                <button type="button" id="modal-btn-save" name="button" class="btn btn-primary">
                    <span id="content-modal-save"> {{ trans('topics.form_def_save') }} </span>
                    <span id="content-modal-saving" style="display:none;"> <i class='fa fa-circle-o-notch fa-spin'></i> {{ trans('topics.form_def_saving') }}  </span>
                    <span id="content-modal-success-saving" style="display:none"> {{ trans('topics.form_def_success_saving') }} </span>
                    <span id="content-modal-failed-saving" style="display:none"> {{ trans('topics.form_def_failed_saving') }} </span>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="chooseContentModal" tabindex="-1" role="dialog" aria-labelledby="chooseContentLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="chooseContentLabel">{{ trans('topics.form_content_modal_choose') }}</h4>
            </div>
            <div class="modal-body">
                <div class="">
                    <form id="search-content-form" enctype="multipart/form-data" method="post" >
                        <div class="input-group">
                            <input type="text" name='content_search_keyword' class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                                <button type='button' class="btn btn-default" type="button" id="search-content-btn">Go!</button>
                            </span>
                        </div>
                    </form>
                </div>
                <div id="list-content-found">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('topics.form_def_cancel') }}</button>
                <button type="button" id="btn-insert-content" name="button" class="btn btn-primary">
                    {{ trans('topics.form_content_insert') }}
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="content-view-modal" tabindex="-1" role="dialog" aria-labelledby="content-view-label">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id='content-view-container'>
        </div>
    </div>
</div>

<script type="text/javascript">
    tinymce.init({ selector:'textarea#content-desc' });
    var btnModalSave    = document.getElementById('modal-btn-save') ,
        ctnModalSave    = document.getElementById('content-modal-save') ,
        ctnModalSaving  = document.getElementById('content-modal-saving') ,
        ctnModalSuccess = document.getElementById('content-modal-success-saving') ,
        ctnModalFailed  = document.getElementById('content-modal-failed-saving');

    btnModalSave.onclick = function(){
        this.setAttribute('disabled', 'true');
        ctnModalSave.style.display = 'none';
        ctnModalSaving.style.display = 'block';
        requestData();
    }

    function requestData(){
        var formData = new FormData(document.querySelector('form#content-form'));
        var ed = tinyMCE.get('content-desc');

        formData.append('content_desc', ed.getContent());

        var myInit = {
            method: 'post' ,
            body: formData
        };

        fetch( "{{ $app->make('url')->to('/service/content/add') }}", myInit )
            .then(function(response) {
                return response.json();
            })
            .then(function(data) {
                ctnModalSaving.style.display    = 'none';
                btnModalSave.classList.remove('btn-primary');

                if (data.status) {
                    ctnModalSuccess.style.display = 'block';

                    btnModalSave.classList.add('btn-success');

                    resultContent.setResult([
                        { tag: 'input', key : 'title' ,         val : data.content.title ,           label: data.content.title } ,
                        { tag: 'input', key : 'id' ,            val : data.content.id , hide: true, parent: 'title'} ,
                        { tag: 'input', key : 'type' ,          val : data.content.type , label: data.content.type } ,
                        { tag: 'input', key : 'appearance' ,    val : data.content.appearance , hide: true, parent: 'title'} ,
                        { tag: 'input', key : 'data_status',    val : "new", parent : 'title' , splitter : '' , hide: true}
                    ]);

                    setTimeout(function () {
                        $('#createContentModal').modal('hide');
                        resetFormModalContent();
                    }, 1000);
                } else {
                    ctnModalFailed.style.display = 'block';

                    btnModalSave.classList.add('btn-danger');

                    setTimeout(function () {
                        resetFormModalContent();
                    }, 1000);
                }
            }).catch(function(err) {
            	console.log(err);
            });
    }

    function resetFormModalContent(){
        setTimeout(function () {
            btnModalSave.classList.remove('btn-danger');
            btnModalSave.classList.remove('btn-success');

            btnModalSave.classList.add('btn-primary');

            ctnModalSave.style.display      = 'block';
            ctnModalFailed.style.display    = 'none';
            ctnModalSuccess.style.display   = 'none';
            ctnModalSaving.style.display    = 'none';

            document.querySelector('form#content-form').reset();

            btnModalSave.removeAttribute('disabled');
        }, 500);
    }

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

        document.getElementsByName('content_type')[0].value =  $(e.target).data('content-type');

    });
</script>

<script type="text/javascript">
    var resultContent = new formDynamicGakken('contents');

    var contents = [
        @if (old('contents') || $topic->id)
            <?php
                $contents       = old('contents') ? json_decode(json_encode(old('contents'))) : $topic->content;
                $current_status = old('contents') ? 'new' : 'old';
            ?>
    		@foreach ($contents as $content)
    			[
                    { tag: 'input', key : 'title' , val : "{{ $content->title }}" , label: "{{ $content->title }}" } ,
                    { tag: 'input', key : 'id' ,    val : "{{ $content->id }}" ,    label: "{{ $content->id }}", hide: true, parent: 'title'} ,
                    { tag: 'input', key : 'type' ,  val : "{{ $content->type }}" ,  label: "{{ $content->type }}" } ,
                    { tag: 'input', key : 'appearance' , val : "{{ $content->appearance }}",  hide: true, parent: 'type' } ,
                    { tag: 'input', key : 'data_status', val : "{{ $current_status }}", parent: 'title' , splitter: '' , hide: true}
                ] ,
    		@endforeach
    	@endif
    ];

    resultContent.setElement({
        'table_result' : '#contents'
    });

    var adtData = [
        { html : "<b class='fa fa-eye'></b>",  att: { style: 'cursor: pointer;' } }
    ];

    adtData[0].click = function( e , self , cell , row ){
        var id       = cell.parentNode.cells[0].querySelectorAll('input')[1].value;
        var formData = new FormData();

        formData.append( 'id' , id );

        var myInit = {
            method  : 'post' ,
            body    : formData
        };

        fetch( "{{ $app->make('url')->to('/service/content/view') }}", myInit )
            .then(function(response) {
                return response.text();
            })
            .then(function( text ) {
                $('#content-view-modal').modal('show');
                document.getElementById('content-view-container').innerHTML = text ;
            }).catch(function(err) {
                console.log(err);
            });
    }

    resultContent.setAdditonal(adtData);

    //handling modal choosing content;
    var btnSrcContent       = document.getElementById('search-content-btn') ,
        btnInsertContent    = document.getElementById('btn-insert-content') ,
        listContentFound    = document.getElementById('list-content-found') ,
        srcContentKeyword   = document.getElementsByName('content_search_keyword')[0] ,
        formContent         = document.querySelector('form#content-form') ,
        formSearchContent   = document.querySelector('form#search-content-form') ,
        chooseBtnContent    = document.getElementById('chooseContentButton');

    chooseBtnContent.addEventListener('click', function(){
        listContentFound.innerHTML  = '';
        srcContentKeyword.value     = '';
    });

    var searchContent = function ( e ) {
        if ( srcContentKeyword.value.length < 4 ) return false;

        var formData = new FormData();

        formData.append( 'keyword' , srcContentKeyword.value );

        var myInit = {
            method  : 'post' ,
            body    : formData
        };

        fetch( "{{ $app->make('url')->to('/service/content/search') }}", myInit )
            .then(function(response) {
                return response.text();
            })
            .then(function(data) {
                listContentFound.innerHTML = data;
            }).catch(function(err) {
                console.log(err);
            });

        return false;
    }

    btnSrcContent.onclick = searchContent;
    formSearchContent.onsubmit  = searchContent;

    btnInsertContent.onclick = function() {
        var contents = document.getElementsByName('content');

        $('#chooseContentModal').modal('hide');

        for (var index in contents) {
            if (contents.hasOwnProperty(index)) {
                if ( contents[index].checked ) {
                    var title       = document.getElementsByName('content_r_title[' + contents[index].value + ']')[0] ,
                        type        = document.getElementsByName('content_r_type[' + contents[index].value + ']')[0] ,
                        appearance  = document.getElementsByName('content_r_appearance[' + contents[index].value + ']')[0];

                    resultContent.setResult([
                        { tag: 'input', key : 'title' ,       val : title.value ,           label: title.value } ,
                        { tag: 'input', key : 'id' ,          val : contents[index].value , label: contents[index].value , hide: true, parent: 'title'} ,
                        { tag: 'input', key : 'type' ,        val : type.value , label: type.value } ,
                        { tag: 'input', key : 'appearance' ,  val : appearance.value ,  hide: true, parent: 'type' } ,
                        { tag: 'input', key : 'data_status',  val : "new", parent : 'title' , splitter : '' , hide: true}
                    ]);
                }
            }
        }
    }

    contents.forEach( function( data) {
        resultContent.setResult(data);
    });

</script>
@endsection
