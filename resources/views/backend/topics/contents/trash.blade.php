@extends('layout.backend')

@section('title') Move {{ $content->name }} to Trash @endsection

@section('content')

	<h1>Move to Trash</h1>
	{!! Form::open(['url' => 'backend/topic/content/trash']) !!}
		<p>
			You are about to move the content <strong>{{ $content->title }} </strong> to trash.<br />
			Are you sure? This action cannot be undone.
		</p>
		{!! Form::hidden('slug', $content->slug) !!}
		<a href="/backend/topic/contents" class="btn btn-default">No, take me back</a>
		<button type="submit" class="btn btn-danger">Yes, move this topic to trash</button>

	{!! Form::close() !!}

@endsection
