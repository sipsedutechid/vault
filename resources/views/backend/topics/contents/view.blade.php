<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="content-view-label">{{ $content->title }}</h4>
</div>
<div class="modal-body">
    @if ( $content->category === 'text' )
        {!! $content->data !!}
    @elseif ( $content->category === 'link' )
        {{ $content->data }}
    @elseif ( $content->category === 'embed' )
        {{ $content->data }}
    @elseif ( $content->category === 'file' )
        <?php
            $file_info = pathinfo($content->data);
            switch ( $file_info['extension'] ) {
                case 'pdf':
                    echo "<object data='" . url('content/files/' . $content->data) . "'></object>";
                    break;

                default:
                    echo "<img src='" . url('content/files/' . $content->data) . "'>";
                    break;
            }
        ?>
    @endif
</div>
