<script src="{{ asset('assets/js/dynamicForm.js') }}" charset="utf-8"></script>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title h4">
            {{ trans('topics.form_disease') }}
            <a class="pull-right" data-toggle="collapse" href="#topicDisease"><b class="glyphicon glyphicon-menu-down"></b></a>
        </div>
    </div>
    <div class="panel-body collapse in" id="topicDisease">
        <div id="diseaseFields" class="hidden">
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('topics.form_disease_name') }}</label>
                <div class="col-sm-9">
                    <input type="hidden" name="disease_data_status" value="new">
                    <select class="form-control" name="disease_id">
                        @foreach ($diseases as $disease)
            				<option value="{{ $disease->id }}">{{ $disease->name }}</option>
            			@endforeach
                    </select>
                </div>
			</div>
			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-3">
					<button type="button" id="cancelDiseaseBtn" class="btn btn-danger btn-normal">{{ trans('topics.form_def_cancel') }}</button>
					<button type="button" id="submitDiseaseBtn" class="btn btn-primary btn-normal">{{ trans('topics.form_def_add') }}</button>
				</div>
			</div>
		</div>
		<button id="addDiseaseButton" type="button" class="btn btn-default">{{ trans('topics.form_add_disease') }}</button>
		<table class="table table-bordered hidden" id="diseases" style="margin-top:15px;">
			<thead>
                <tr>
                    <th>{{ trans('topics.form_disease_name') }}</th>
                    <th style="width:50px;"></th>
                </tr>
            </thead>
			<tbody></tbody>
		</table>
    </div>
</div>

<script type="text/javascript">
    var formDisease = new formDynamicGakken('disease');

    var diseases = [
        @if ($topic->id)
            @foreach ($topic->disease as $disease)
                [
                    {tag: 'input', key: 'id',           val: "{{ $disease->id }}", label: "{{ $disease->name }}" } ,
                    {tag: 'input', key: 'data_status',  val: "old", parent: 'id' , splitter: '' , hide: true}
                ] ,
            @endforeach
        @endif
    ];


    formDisease.setButton({
        add     : '#addDiseaseButton' ,
        cancel  : '#cancelDiseaseBtn' ,
        submit  : '#submitDiseaseBtn'
    });

    formDisease.setElement({
        form_elm    : '#diseaseFields' ,
        table_result: '#diseases'
    });

    formDisease.setData([
        {elm: 'disease_id',          key: 'id',          tag: 'input'} ,
        {elm: 'disease_data_status', key: 'data_status', tag: 'input', parent: 'id', splitter: '', hide: true} ,
    ]);

    formDisease.run();

    diseases.forEach( function( data ) {
        formDisease.setResult(data);
    });
</script>
