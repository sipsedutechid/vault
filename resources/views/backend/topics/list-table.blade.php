@foreach ($topics as $topic)
    <tr>
        <td>
            <div>{{ $topic->title }} - {{ $topic->category }} </div>
            <a href="/backend/topic/{{ $topic->slug }}/edit">{{ trans('topics.edit') }}</a> / <a href="/backend/topic/{{ $topic->slug }}/trash" class="text-danger">{{ trans('topics.trash') }}</a>
        </td>
        <td style="max-width:450px;">
            <div class="ellipsis">
                {{ $topic->is_open_access ? 'Public' : 'Suscribe' }}
            </div>
        </td>
        <td style="width: 150px;">
            <div>{{ $topic->createdBy->name }}</div>
            <div>{{ $topic->created_at }}</div>
        </td>
    </tr>
@endforeach
