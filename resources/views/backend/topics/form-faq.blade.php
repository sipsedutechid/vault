<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title h4">
            {{ trans('topics.form_faq') }}
            <a class="pull-right" data-toggle="collapse" href="#faq"><b class="glyphicon glyphicon-menu-down"></b></a>
        </div>
    </div>
    <div class="panel-body collapse in" id="faq">
        <div id="faqFields" class="hidden">
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('topics.form_faq_question') }}</label>
				<div class="col-sm-9">
					<input type="text" name="topic_question" class="form-control" />
                    <input type="hidden" name="topic_status" value='new' class="form-control" readonly />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('topics.form_faq_answer') }}</label>
				<div class="col-sm-9">
					<textarea name="topic_answer" class="form-control"> </textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-3">
					<button type="button" id="cancelFaqBtn" class="btn btn-danger btn-normal">{{ trans('topics.form_def_cancel') }}</button>
					<button type="button" id="submitFaqBtn" class="btn btn-primary btn-normal">{{ trans('topics.form_def_save') }}</button>
				</div>
			</div>
		</div>
		<button id="addFaqBtn" type="button" class="btn btn-default">{{ trans('topics.form_def_add') }}</button>
		<table class="table table-bordered hidden" id="faqs" style="margin-top:15px;">
			<thead>
                <tr>
                    <th>{{ trans('topics.form_faq_question') }}</th>
                    <th>{{ trans('topics.form_faq_answer') }}</th>
                    <th style="width:50px;"></th>
                </tr>
            </thead>
			<tbody></tbody>
		</table>
    </div>
</div>

<script type="text/javascript">
    var resultFaq = new formDynamicGakken('faqs');

    resultFaq.setElement({
        table_result  : '#faqs' ,
        form_elm      : '#faqFields'
    });

    resultFaq.setButton({
        add    : '#addFaqBtn' ,
        cancel : '#cancelFaqBtn' ,
        submit : '#submitFaqBtn'
    });

    resultFaq.setData([
        {
            elm     : 'topic_question' ,
            key     : 'question' ,
            tag     : 'input' ,
        } ,
        {
            elm     : 'topic_answer' ,
            tag     : 'textarea' ,
            key     : 'answer'
        } ,
        {
            elm     : 'topic_status' ,
            tag     : 'input' ,
            key     : 'data_status' ,
            parent  : 'question' ,
            splitter: ''
        }
    ]);

    resultFaq.run();

    var faqs = [
        @if (old('faqs') || $topic->id)
            <?php
                $faqs           = old('faqs') ? json_decode(json_encode(old('faqs'))) : $topic->faq;
                $current_status = old('faqs') ? 'new' : 'old';
            ?>
    		@foreach ($faqs as $data_faq)
    			[
                    {
                        tag     : 'input' ,
                        key     : 'question' ,
                        val     : "{{ $data_faq->question }}" ,
                        label   : "{{ $data_faq->question }}"
                    } ,
                    {
                        tag     : 'input' ,
                        key     : 'answer' ,
                        val     : "{{ $data_faq->answer }}" ,
                        label   : "{{ $data_faq->answer }}"
                    } ,
                    {
                        tag     : 'input' ,
                        key     : 'data_status' ,
                        val     : "{{ $current_status }}" ,
                        parent  : 'question' ,
                        splitter: '' ,
                        hide    : true
                    } ,
                    @if( !old('faqs') )
                    {
                        tag     : 'input' ,
                        key     : 'id' ,
                        parent  : 'question' ,
                        splitter: '' ,
                        hide    : true
                    }
                    @endif
                ] ,
    		@endforeach
    	@endif
    ];

    faqs.forEach( function(data) {
        resultFaq.setResult(data);
    });
</script>
