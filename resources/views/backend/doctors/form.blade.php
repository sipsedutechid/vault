@extends('layout.backend')

@section('title')
	{{ trans('doctors.form_' . ($doctor->id ? 'edit' : 'add')) }}
@endsection

@section('nav_content')

	<div class="navbar-text navbar-back">

		<a href="/backend/doctors" title="Doctors">
			<b class="glyphicon glyphicon-chevron-left"></b>
		</a>

	</div>
	<div class="navbar-text navbar-title">
		{{ trans('doctors.form_' . ($doctor->id ? 'edit' : 'add')) }}
	</div>
@endsection

@section('headscript')
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.structure.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.theme.min.css') }}">
	<script type="text/javascript" src="{{ asset('libs/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection

@section('content')

	{!! Form::model($doctor, ['url' => 'backend/doctor/' . ($doctor->id ? $doctor->slug . '/edit' : 'new'), 'class' => 'form-horizontal', 'novalidate' => 'novalidate', 'id' => 'formDoctor']) !!}
		@if ($doctor->id)
			{!! Form::hidden('id') !!}
			{!! Form::hidden('slug') !!}
		@endif
		<div class="row">
			<div class="col-sm-8">
				<div class="form-group">
					<div class="col-sm-3">
						{!! Form::text('front_title', null, ['class' => 'form-control input-lg seamless', 'placeholder' => trans('doctors.form_first_title')]) !!}
					</div>
					<div class="col-sm-6">
						{!! Form::text('name', null, ['class' => 'form-control input-lg seamless', 'placeholder' => trans('doctors.form_name')]) !!}
					</div>
                    <div class="col-sm-3">
						{!! Form::text('back_title', null, ['class' => 'form-control input-lg seamless', 'placeholder' => trans('doctors.form_back_title')]) !!}
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="panel-title h4">
							{!! trans('doctors.form_detail') !!}
							<a class="pull-right" data-toggle="collapse" href="#doctorDetail"><b class="glyphicon glyphicon-menu-down"></b></a>
						</div>
					</div>
					<div class="panel-body collapse in" id="doctorDetail">

                        <div class="form-group">
							<label class="control-label col-sm-3">{{ trans('doctors.form_email') }}</label>
							<div class="col-sm-9">
                                {!! Form::email('email', null, ['class' => 'form-control']) !!}
							</div>
						</div>

                        <div class="form-group">
							<label class="control-label col-sm-3">{{ trans('doctors.form_phone') }}</label>
							<div class="col-sm-9">
                                <div class="input-group">
                                    {!! Form::number('phone', null, ['class' => 'form-control', 'placeholder' => trans('doctors.form_phone_home'), 'style' =>'width:60%']) !!}
                                    {!! Form::number('mobile', null, ['class' => 'form-control',  'placeholder' => trans('doctors.form_phone_mobile'), 'style' =>'width:40%']) !!}
                                </div>
                            </div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-3">{{ trans('doctors.form_gender') }}</label>
							<div class="col-sm-9">
                                <label class="radio-inline">
                                    {!! Form::radio('gender', 'm', ['checked' => 'true']) !!} {{ trans('doctors.form_gender_male') }}
                                </label>
                                <label class="radio-inline">
                                    {!! Form::radio('gender', 'f') !!} {{ trans('doctors.form_gender_female') }}
                                </label>
							</div>
						</div>

                        <div class="form-group">
							<label class="control-label col-sm-3">{{ trans('doctors.form_birth') }}</label>
							<div class="col-sm-9">
                                <div class="input-group">
                                    {!! Form::text('place_birth', null, ['class' => 'form-control', 'placeholder' => trans('doctors.form_birth_place'), 'style' =>'width:60%']) !!}
                                    {!! Form::date('date_birth', null, ['class' => 'form-control', 'style' =>'width:40%']) !!}
                                </div>
                            </div>
						</div>

                        <div class="form-group">
							<label class="control-label col-sm-3">{{ trans('doctors.form_address') }}</label>
							<div class="col-sm-9">
                                {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
                            </div>
						</div>

					</div>
				</div>

                @include('backend.doctors.form-academic')
				@include('backend.doctors.form-clinic')

			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<div class="col-xs-12">
						@if (!$doctor->id)
							<button type="submit" name="post_action" value="new" class="btn btn-primary btn-block">{{ trans('doctors.form_cdp_save') }}</button>
							<button type="submit" name="post_action" value="new" class="btn btn-default btn-block">{{ trans('doctors.form_cdp_save_add_other') }}</button>
						@else
							<button type="submit" name="post_action" value="edit" class="btn btn-primary btn-block">{{ trans('doctors.form_cdp_update') }}</button>
							<button type="submit" name="post_action" value="new" class="btn btn-default btn-block">{{ trans('doctors.form_cdp_update_continue') }}</button>
						@endif
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading"><div class="panel-title h4">{{ trans('doctors.form_specialist') }}</div></div>
					<div class="panel-body">
						<div class="form-group">
							<label class="control-label col-sm-4">{{ trans('doctors.form_specialist_class') }}</label>
							<div class="col-sm-8">
								{!! Form::select('specialist', $specialists, null, ['placeholder' => trans('doctors.form_select'), 'class' => 'form-control', 'form' => 'formDoctor']) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4">{{ trans('doctors.form_specialist_sub_class') }}</label>
							<div class="col-sm-8">
								{!! Form::select('specialist_subclass', [], null, ['placeholder' => trans('doctors.form_select_specialist_subclass'), 'class' => 'form-control', 'form' => 'formDoctor']) !!}
							</div>
						</div>

					</div>
				</div>

			</div>
		</div>

	{!! Form::close() !!}

	<script type="text/javascript">
		var specialistInput 		= _name( 'specialist' )[0] ,
			specialistSubClassInput = _name( 'specialist_subclass')[0];

		specialistInput.addEventListener('change', function(){

			fetch("{{ url('service/list/specialist-subclasses/?specialist_id=') }}" + this.value , {
				method	: 'GET'
			}).then(function( response ) {

				return response.json();

			}).then(function( subclasses ) {

				specialistSubClassInput.innerHTML = '';
				specialistSubClassInput.insertAdjacentHTML('beforeend', "<option> Please Select </option>");

				subclasses.forEach( function( subclass ) {
					specialistSubClassInput.insertAdjacentHTML('beforeend', "<option value='" + subclass.id  + "'> " + subclass.name + " </option>");
				});
			});

		});
	</script>

@endsection
