<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title h4">
            {{ trans('doctors.form_academic') }}
            <a class="pull-right" data-toggle="collapse" href="#doctorAcademic"><b class="glyphicon glyphicon-menu-down"></b></a>
        </div>
    </div>
    <div class="panel-body collapse in" id="doctorAcademic">
        <div id="academicFields" class="hidden">
            <div class="form-group">
				<label class="control-label col-sm-3">{{ trans('doctors.form_academic_level') }}</label>
				<div class="col-sm-9">
					<select name="academic_levels" class="form-control">
                        @foreach ($academic_levels as $academic_level)
            				<option value="{{ $academic_level->id }}">{{ $academic_level->name }}</option>
            			@endforeach
                    </select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('doctors.form_academic_school_college') }}</label>
				<div class="col-sm-9">
					<input type="text" name="academic_school_college" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('doctors.form_academic_years') }}</label>
				<div class="col-sm-4">
					<input type="number" name="academic_year" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('doctors.form_academic_address') }}</label>
				<div class="col-sm-9">
					<textarea name="academic_address" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-3">
					<button type="button" id="cancelAcademicBtn" class="btn btn-danger btn-normal">{{ trans('doctors.form_cdp_cancel') }}</button>
					<button type="button" id="submitAcademicBtn" class="btn btn-primary btn-normal">{{ trans('doctors.form_cdp_add') }}</button>
				</div>
			</div>
		</div>
		<button id="addAcademicButton" type="button" class="btn btn-default">{{ trans('doctors.form_add_academic') }}</button>
		<table class="table table-bordered hidden" id="academics" style="margin-top:15px;">
			<thead>
                <tr>
                    <th>{{ trans('doctors.form_academic_level') }}</th>
                    <th>{{ trans('doctors.form_academic_school_college') }}</th>
                    <th>{{ trans('doctors.form_academic_years') }}</th>
                    <th>{{ trans('doctors.form_academic_address') }}</th>
                    <th style="width:50px;"></th>
                </tr>
            </thead>
			<tbody></tbody>
		</table>
    </div>
</div>

<script type="text/javascript">
    function _id( id ) {
        return document.getElementById( id );
    }

    function _name( name ) {
        return document.getElementsByName( name );
    }

    function crtHideIinput( tag , name , val) {
        var elm = document.createElement(tag);

        elm.name  = name;
        elm.value = val;
        elm.type  = 'hidden';

        return elm;
    }

    function removeRow( e ) {
        var i                     = this.getAttribute('data-index') ,
            academicRemoveButton  = tableAcademics.querySelectorAll('.remove-academic');

        for (var index in academicRemoveButton) {
            if (academicRemoveButton.hasOwnProperty(index)) {
                var indexButton = parseInt(academicRemoveButton[index].getAttribute('data-index'));

                if ( indexButton > i ) academicRemoveButton[index].setAttribute( 'data-index' , (indexButton-1) );
            }
        }

        tableAcademics.deleteRow(i);

        if ( tableAcademics.rows.length <= 1) {
            tableAcademics.classList.add('hidden');
        }
    }

    function resetInputAcademics() {
        _name( 'academic_year' )[0].value           = '' ,
        _name( 'academic_address' )[0].value        = '' ,
        _name( 'academic_school_college' )[0].value = '' ;
    }

    function crtAcademics( academicIdVal , schoolCollegeVal , academicYearVal , academicAddressVal , academicLevelsVal , academicLevelText ) {
        var iRow    = tableAcademics.rows.length;
        var newRow  = tableAcademics.insertRow( iRow );

        var a = document.createElement('a') ,
            schoolCollege   = crtHideIinput( 'input' , 'academics[' + (iRow-1) + '][school_college]' , schoolCollegeVal ) ,
            academicYear    = crtHideIinput( 'input' , 'academics[' + (iRow-1) + '][year]' , academicYearVal ) ,
            academicAddress = crtHideIinput( 'input' , 'academics[' + (iRow-1) + '][address]' , academicAddressVal ) ,
            academicLevels  = crtHideIinput( 'input' , 'academics[' + (iRow-1) + '][level]' , academicLevelsVal) ,
            academicId      = crtHideIinput( 'input' , 'academics[' + (iRow-1) + '][id]' , academicIdVal);

        a.setAttribute( 'data-index' , ( iRow ) );
        a.classList.add( 'remove-academic' );
        a.innerHTML = '<b class="glyphicon glyphicon-remove"></b>';

        var cell1 = newRow.insertCell(0) ,
            cell2 = newRow.insertCell(1) ,
            cell3 = newRow.insertCell(2) ,
            cell4 = newRow.insertCell(3) ,
            cell5 = newRow.insertCell(4);

        cell1.innerHTML = academicLevelText;
        cell2.innerHTML = schoolCollegeVal;
        cell3.innerHTML = academicYearVal;
        cell4.innerHTML = academicAddressVal;

        cell1.appendChild( academicLevels );
        cell1.appendChild( academicId );
        cell2.appendChild( schoolCollege );
        cell3.appendChild( academicYear );
        cell4.appendChild( academicAddress );
        cell5.appendChild( a );

        a.onclick = removeRow;

        tableAcademics.classList.remove( 'hidden' );
    }

    var btnAdd         = _id( 'addAcademicButton' ) ,
        btnCancel      = _id( 'cancelAcademicBtn' ) ,
        btnSubmit      = _id( 'submitAcademicBtn' ) ,
        academicFields = _id( 'academicFields' ) ,
        tableAcademics = _id( 'academics' );

    var arrFormAcademic = [
        @if ($doctor->id)
    		@foreach ($doctor->academic as $academic)
    			{
                    _id            : "{{ $academic->id }}" ,
                    school_college : "{{ $academic->school_college }}" ,
                    year           : "{{ $academic->year }}" ,
                    address        : "{{ $academic->address }}" ,
                    level          : "{{ $academic->level_academics_id }}" ,
                    level_text     : "{{ $academic->academicLevel->name}}"
                } ,
    		@endforeach
    	@endif
    ];

    window.addEventListener('load', function(){
        arrFormAcademic.forEach( function(academic) {
            crtAcademics(
                academic._id ,
                academic.school_college ,
                academic.year ,
                academic.address ,
                academic.level ,
                academic.level_text
            );
        });
    });

    btnAdd.onclick = function( e ) {
        academicFields.classList.remove( 'hidden' );
        this.classList.add('hidden');
    }

    btnCancel.onclick = function( e ) {
        academicFields.classList.add( 'hidden' );
        btnAdd.classList.remove( 'hidden' );
    }

    btnSubmit.onclick =  function( e ) {
        var schoolCollegeVal   = _name( 'academic_school_college' )[0].value ,
            academicYearVal    = _name( 'academic_year' )[0].value ,
            academicAddressVal = _name( 'academic_address' )[0].value ,
            academicLevelsVal  = _name( 'academic_levels' )[0].value ,
            academicLevelText  = _name( 'academic_levels' )[0].options[ _name( 'academic_levels' )[0].selectedIndex ].text;

        academicFields.classList.add( 'hidden' );
        btnAdd.classList.remove( 'hidden' );

        crtAcademics(
            0 ,
            schoolCollegeVal ,
            academicYearVal ,
            academicAddressVal ,
            academicLevelsVal ,
            academicLevelText
        );

        resetInputAcademics();
    }
</script>
