@extends('layout.backend')

@section('title')
	{{ trans('doctors.title') }}
@endsection

@section('nav_content')

	<div class="navbar-text navbar-title">{{ trans('doctors.title') }}</div>
	<a href="/backend/doctor/new" class="btn navbar-btn"><b class="glyphicon glyphicon-plus"></b>&ensp;{{ trans('doctors.add') }}</a>

@endsection

@section('content')

	<div class="row">
		<div class="col-lg-6">
				<div class="panel panel-default">
					<div class="panel-body h3">
						<a href="/backend/drug/all">
							{{ trans('doctors.home_all') }} <span class="badge pull-right">{{ $doctor_count }}</span>
						</a>
					</div>
					<table class="table table-condensed">
						<thead><tr><th colspan="2">{{ trans('doctors.home_all_recent') }}</th></tr></thead>
						<tbody>
							@foreach($doctor_recents as $doctor)
								<tr>
									<td style="width:100px;">{{ $doctor->created_at }}</td>
									<td><a href="/backend/doctor/{{ $doctor->slug }}/edit?redirect={{ urlencode('backend/doctors') }}">{{ $doctor->front_title . ' ' . $doctor->name . ' ' . $doctor->back_title }}</a></td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</a>
		</div>

	</div>

@endsection
