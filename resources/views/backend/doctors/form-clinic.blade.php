<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title h4">
            {{ trans('doctors.form_clinic') }}
            <a class="pull-right" data-toggle="collapse" href="#doctorClinic"><b class="glyphicon glyphicon-menu-down"></b></a>
        </div>
    </div>
    <div class="panel-body collapse in" id="doctorClinic">
        <div id="clinicFields" class="hidden">
            <div class="form-group">
				<label class="control-label col-sm-3">{{ trans('doctors.form_clinic_name') }}</label>
                <div class="col-sm-9">
					<input type="text" name="clinic_name" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('doctors.form_clinic_address') }}</label>
				<div class="col-sm-9">
					<textarea name="clinic_address" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('doctors.form_clinic_city') }}</label>
				<div class="col-sm-4">
					<input type="text" name="clinic_city" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('doctors.form_clinic_phone') }}</label>
				<div class="col-sm-9">
					<input type="text" name="clinic_phone" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-3">
					<button type="button" id="cancelClinicBtn" class="btn btn-danger btn-normal">{{ trans('doctors.form_cdp_cancel') }}</button>
					<button type="button" id="submitClinicBtn" class="btn btn-primary btn-normal">{{ trans('doctors.form_cdp_add') }}</button>
				</div>
			</div>
		</div>
		<button id="addClinicButton" type="button" class="btn btn-default">{{ trans('doctors.form_add_clinic') }}</button>
		<table class="table table-bordered hidden" id="clinics" style="margin-top:15px;">
			<thead>
                <tr>
                    <th>{{ trans('doctors.form_clinic_name') }}</th>
                    <th>{{ trans('doctors.form_clinic_address') }}</th>
                    <th>{{ trans('doctors.form_clinic_city') }}</th>
                    <th>{{ trans('doctors.form_clinic_phone') }}</th>
                    <th style="width:50px;"></th>
                </tr>
            </thead>
			<tbody></tbody>
		</table>
    </div>
</div>

<script type="text/javascript">
    function resetInputClinics() {
        _name( 'clinic_name' )[0].value       = '' ,
        _name( 'clinic_address' )[0].value    = '' ,
        _name( 'clinic_city' )[0].value       = '' ,
        _name( 'clinic_phone' )[0].value       = '';
    }

    function removeRowClinics( e ) {
        var i                   = this.getAttribute('data-index') ,
            clinicRemoveButton  = tableClinic.querySelectorAll('.remove-clinic');

        for (var index in clinicRemoveButton) {
            if (clinicRemoveButton.hasOwnProperty(index)) {
                var indexButton = clinicRemoveButton[index].getAttribute('data-index');

                if ( indexButton > i ) clinicRemoveButton[index].setAttribute( 'data-index' , (indexButton-1) );
            }
        }

        tableClinic.deleteRow(i);

        if ( tableClinic.rows.length <= 1) {
            tableClinic.classList.add('hidden');
        }
    }

    function crtClinics( clinicIdVal , clinicNameVal , clinicAddressVal , clinicCityVal , clinicTelpVal ) {
        var iRow    = tableClinic.rows.length;
        var newRow  = tableClinic.insertRow( iRow );

        var a               = document.createElement('a') ,
            clinicName    = crtHideIinput( 'input' , 'clinics[' + (iRow-1) + '][name]' , clinicNameVal ) ,
            clinicAddress = crtHideIinput( 'input' , 'clinics[' + (iRow-1) + '][address]' , clinicAddressVal ) ,
            clinicCity    = crtHideIinput( 'input' , 'clinics[' + (iRow-1) + '][city]' , clinicCityVal ) ,
            clinicTelp    = crtHideIinput( 'input' , 'clinics[' + (iRow-1) + '][telp]' , clinicTelpVal ) ,
            clinicId      = crtHideIinput( 'input' , 'clinics[' + (iRow-1) + '][id]' , clinicIdVal );

        a.setAttribute( 'data-index' , ( iRow ) );
        a.classList.add('remove-clinic');
        a.innerHTML = '<b class="glyphicon glyphicon-remove"></b>';

        var cell1 = newRow.insertCell(0) ,
            cell2 = newRow.insertCell(1) ,
            cell3 = newRow.insertCell(2) ,
            cell4 = newRow.insertCell(3) ,
            cell5 = newRow.insertCell(4);

        cell1.innerHTML = clinicNameVal;
        cell2.innerHTML = clinicAddressVal;
        cell3.innerHTML = clinicCityVal;
        cell4.innerHTML = clinicTelpVal;

        cell1.appendChild( clinicName );
        cell1.appendChild( clinicId );
        cell2.appendChild( clinicAddress );
        cell3.appendChild( clinicCity );
        cell4.appendChild( clinicTelp );
        cell5.appendChild( a );

        a.onclick = removeRowClinics;

        tableClinic.classList.remove( 'hidden' );
    }

    var btnAddClinic      = _id( 'addClinicButton' ) ,
        btnCancelClinic   = _id( 'cancelClinicBtn' ) ,
        btnSubmitClinic   = _id( 'submitClinicBtn' ) ,
        clinicFields      = _id( 'clinicFields' ) ,
        tableClinic       = _id( 'clinics' );

    var arrFormClinic = [
        @if ($doctor->id)
    		@foreach ($doctor->clinic as $clinic)
    			{
                    _id     : "{{ $clinic->id }}" ,
                    _name   : "{{ $clinic->name }}" ,
                    address : "{{ $clinic->address }}" ,
                    city    : "{{ $clinic->city }}" ,
                    telp    : "{{ $clinic->telp }}"
                } ,
    		@endforeach
    	@endif
    ];

    window.addEventListener('load', function(){
        arrFormClinic.forEach( function(clinic) {
            crtClinics(
                clinic._id ,
                clinic._name ,
                clinic.address ,
                clinic.city ,
                clinic.telp
            );
        });
    });

    btnAddClinic.onclick = function( e ) {
        clinicFields.classList.remove( 'hidden' );
        this.classList.add('hidden');
    }

    btnCancelClinic.onclick = function( e ) {
        clinicFields.classList.add( 'hidden' );
        btnAddClinic.classList.remove( 'hidden' );
    }

    btnSubmitClinic.onclick =  function( e ) {
        var clinicNameVal     = _name( 'clinic_name' )[0].value ,
            clinicAddressVal  = _name( 'clinic_address' )[0].value ,
            clinicCityVal     = _name( 'clinic_city' )[0].value ,
            clinicTelpVal     = _name( 'clinic_phone' )[0].value;

        clinicFields.classList.add( 'hidden' );
        btnAddClinic.classList.remove( 'hidden' );

        crtClinics( 0 , clinicNameVal , clinicAddressVal , clinicCityVal , clinicTelpVal );
        resetInputClinics();
    }
</script>
