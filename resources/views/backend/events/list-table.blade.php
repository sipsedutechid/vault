@foreach ($events as $event)
  <tr>
    <td>
      <div>{{ $event->name }} </div>
      <a href="/backend/topic/{{ $event->slug }}/edit">{{ trans('events.edit') }}</a> / <a href="/backend/topic/{{ $event->slug }}/trash" class="text-danger">{{ trans('events.trash') }}</a>
    </td>
    <td style="width: 150px;">
      <div>{{ $event->organizer }}</div>
      <div>{{ $event->created_at }}</div>
    </td>
  </tr>
@endforeach
