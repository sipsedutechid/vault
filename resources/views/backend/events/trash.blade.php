@extends('layout.backend')

@section('title') Move {{ $event->name }} to Trash @endsection

@section('content')

	<h1>Move to Trash</h1>
	{!! Form::open(['url' => 'backend/event/trash']) !!}
		<p>
			You are about to move the event <strong>{{ $event->name }}</strong> to trash.<br />
			Are you sure? This action cannot be undone.
		</p>
		{!! Form::hidden('slug', $event->slug) !!}
		<a href="/backend/event/all" class="btn btn-default">No, take me back</a>
		<button type="submit" class="btn btn-danger">Yes, move this event to trash</button>

	{!! Form::close() !!}

@endsection