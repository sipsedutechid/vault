@extends('layout.backend')

@section('title')
	{{ trans('events.list_title') }}
@endsection

@section('nav_content')

	<div class="navbar-text navbar-back">
		<a href="/backend/events" title="{{ trans('events.back_dashboard') }}">
				<b class="glyphicon glyphicon-chevron-left"></b>
		</a>
	</div>

    <div class="navbar-text navbar-title">{{ trans('events.list_title') }}</div>

    <a href="/backend/event/new?redirect=list" class="btn navbar-btn navbar-left">
        <b class="glyphicon glyphicon-plus"></b>&ensp;{{ trans('events.add') }}
    </a>
	{!! Form::open(['url' => 'backend/event/all', 'class' => 'navbar-form navbar-left','method' => 'get']) !!}
		<div class="form-group">
			<div class="input-group">
				{!! Form::text('s', Request::input('s'), ['class' => 'form-control', 'placeholder' => trans('events.list_search'), 'style' => 'width:300px;']) !!}
				<span class="input-group-btn">
					<button type="submit" class="btn btn-default"><b class="glyphicon glyphicon-search"></b></button>
					@if (Request::input('s'))
						<a href="/backend/event/all" class="btn btn-default">{{ trans('events.list_search_clear') }}</a>
					@endif
				</span>
			</div>
		</div>
	{!! Form::close() !!}

@endsection

@section('content')
	<?php $lastId = 0; ?>
	@if (count($events) > 0 || strlen(Request::input('s')) > 0)

		<div class="panel panel-default panel-table">
			<table class="table {{ count($events) > 0 ? "table-hover" : "" }}" id="listTable">
				<tbody>
					@if(count($events) == 0)
						<tr><td colspan="3" class="text-center" style="padding-top: 30px;padding-bottom: 30px;">{{ trans('events.list_search_empty', ['term' => Request::input('s')]) }}</td></tr>
					@else
						@foreach ($events as $event)
							<tr>
								<td>
									<div>{{ $event->name }} </div>
									<a href="/backend/event/{{ $event->slug }}/edit">{{ trans('events.edit') }}</a> / <a href="/backend/event/{{ $event->slug }}/trash" class="text-danger">{{ trans('events.trash', ['title' => $event->name]) }}</a>
								</td>
								<td style="width: 150px;">
									<div>{{ $event->organizer }}</div>
									<div>{{ $event->created_at }}</div>
								</td>
							</tr>
							<?php $lastId = $event->id; ?>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>

		@if (!Request::input('s'))
			@if( $loadMore )
				<p class="text-center">
					<button type="button" id="loadMoreBtn" class="btn btn-default">{{ trans('app.load_more') }}</button>
				</p>
				<script>
					const loadMoreBtn = document.getElementById('loadMoreBtn');
					let lastId = {{ $lastId }};
					loadMoreBtn.onclick = e => {
						const xhttp = new XMLHttpRequest();
						xhttp.onreadystatechange = function() {
								if (this.readyState == 4 && this.status == 200) {
									const response = JSON.parse(xhttp.responseText);
									const elmTable = document.getElementById('listTable').getElementsByTagName('tbody')[0];
									lastId = response.last_id;
									elmTable.insertAdjacentHTML('beforeend', response.html);
									loadMoreBtn.style.display = lastId > 0 ? 'inline-block' : 'none';
								}
						};
						xhttp.open("GET", "/backend/event/all?last_id=" + lastId, true);
						xhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
						xhttp.send();
					}
				</script>
			@endif
		@endif

	@else
		<p>{{ trans('events.list_none') }} <a href="/backend/event/new">{{ trans('events.add') }}</a></p>
	@endif

@endsection
