@extends('layout.backend')

@section('title')
	{{ trans('events.title') }}
@endsection

@section('nav_content')

	<div class="navbar-text navbar-title">{{ trans('events.title') }}</div>
	<a href="/backend/event/new" class="btn navbar-btn"><b class="glyphicon glyphicon-plus"></b>&ensp;{{ trans('events.add') }}</a>

@endsection

@section('content')

	<div class="row">
		<div class="col-lg-6">
				<div class="panel panel-default">
					<div class="panel-body h3">
						<a href="/backend/event/all">
							{{ trans('events.home_all') }} <span class="badge pull-right">{{ $event_count }}</span>
						</a>
					</div>
					<table class="table table-condensed">
						<thead><tr><th colspan="2">{{ trans('events.home_all_recent') }}</th></tr></thead>
						<tbody>
							@foreach($recent_events as $event)
								<tr>
									<td style="width:100px;">{{ $event->created_at }}</td>
									<td><a href="/backend/event/{{ $event->slug }}/edit?redirect={{ urlencode('backend/events') }}">{{ $event->name }}</a></td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</a>
		</div>
	</div>

@endsection
