@extends('layout.backend')

@section('title')
	{{ $user->id ? 'Edit' : 'Add New' }} User
@endsection

@section('nav_content')

	<div class="navbar-text navbar-back"><a href="/backend/user/all" title="{{ trans('users.form.back_list') }}"><b class="glyphicon glyphicon-chevron-left"></b></a></div>
	<div class="navbar-text navbar-title">{{ trans('users.form.title_' . ($user->id ? 'edit' : 'add')) }}</div>

@endsection

@section('content')

	<div class="row">
		<div class="col-sm-6 col-md-8">
			{!! Form::model($user, ['url' => 'backend/user/' . (!$user->id ? 'new' : $user->login . '/edit'), 'class' => 'form-horizontal']) !!}
				<div class="form-group">
					<label class="control-label col-sm-4">{{ trans('users.form.username') }}</label>
					<div class="col-sm-8">
						{!! Form::text('login', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) !!}
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-4">{{ trans('users.form.role') }}</label>
					<div class="col-sm-8">
						<div class="input-group">
							{!! Form::select('role', $roles, null, ['class' => 'form-control']) !!}
							<span class="input-group-btn">
								<a href="/backend/role/add?ref=user/{{ $user->id ? 'edit/' . $user->id : 'add'}}" class="btn btn-default"><b class="glyphicon glyphicon-plus"></b>&ensp;{{ trans('users.form.new_role') }}</a>
								
							</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-4">{{ trans('users.form.password') }}</label>
					<div class="col-sm-8">
						{!! Form::password('password', ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-4">{{ trans('users.form.password_verify') }}</label>
					<div class="col-sm-8">
						{!! Form::password('confirm_password', ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class="form-group" style="margin-top:30px;">
					<label class="control-label col-sm-4">{{ trans('users.form.name') }}</label>
					<div class="col-sm-8">
						{!! Form::text('name', null, ['class' => 'form-control']) !!}
					</div>
				</div>

				<div class="form-group" style="margin-top:30px;">
					<div class="col-sm-8 col-sm-offset-4">
						<button type="submit" class="btn btn-primary">{{ trans('users.form.submit') }}</button>
					</div>
				</div>

			{!! Form::close() !!}
		</div>
	</div>

@endsection
