@extends('layout.backend')

@section('title')
	{{ trans('users.title') }}
@endsection

@section('nav_content')
	<div class="navbar-text navbar-title">{{ trans('users.title') }}</div>
@endsection

@section('content')
	
	<div class="row">
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-body h3">
					<a href="/backend/user/all">{{ trans('users.home_all') }}</a>
					<span class="pull-right badge">{{ $user_count }}</span>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="row">
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-body h4">
							<a href="/backend/user/roles">{{ trans('users.home_roles') }}</a>
							<span class="pull-right badge">{{ $role_count }}</span>
						</div>
					</div>
				</div>
				<div class="col-md-6"></div>
			</div>
		</div>
	</div>

@endsection