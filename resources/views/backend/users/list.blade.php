@extends('layout.backend')

@section('title')
	{{ trans('users.list.title') }}
@endsection

@section('nav_content')
	<a href="/backend/users" class="navbar-text navbar-back"><b class="glyphicon glyphicon-chevron-left"></b></a>
	<div class="navbar-text navbar-title">{{ trans('users.list.title') }}</div>
	<a href="/backend/user/new" class="btn navbar-btn"><b class="glyphicon glyphicon-plus"></b> {{ trans('users.list.add') }}</a>
@endsection

@section('content')

	<div class="panel panel-default">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>{{ trans('users.list.username')}}</th>
					<th>{{ trans('users.list.name')}}</th>
					<th>{{ trans('users.list.role')}}</th>
					<th style="width:150px;">{{ trans('users.list.since') }}</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $user)
					<tr>
						<td><a href="/backend/user/{{ $user->login }}/edit">{{ $user->login }}</a></td>
						<td>{{ $user->name }}</td>
						<td>{{ $user->role->name }}</td>
						<td>{{ $user->created_at }}</td>
					</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>	
					<th>{{ trans('users.list.username')}}</th>
					<th>{{ trans('users.list.name')}}</th>
					<th>{{ trans('users.list.role')}}</th>
					<th>{{ trans('users.list.since') }}</th>
				</tr>
			</tfoot>
		</table>
	</div>

@endsection