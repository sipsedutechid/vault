@extends('layout.backend')

@section('title')
	{{ trans('users.roles.form.' . ($role->id ? 'edit' : 'add')) }}
@endsection

@section('nav_content')
	<a href="/backend/user/roles" class="navbar-text navbar-back"><b class="glyphicon glyphicon-chevron-left"></b></a>
	<div class="navbar-text navbar-title">{{ trans('users.roles.form.' . ($role->id ? 'edit' : 'add')) }}</div>
@endsection

@section('content')

	{!! Form::model($role, ['url' => '/backend/user/role/' . ($role->id ? $role->slug . '/edit' : 'add')]) !!}
	<div class="row">
		<div class="col-lg-8">
			<div class="form-group">
				{!! Form::text('name', null, ['class' => 'form-control input-lg seamless', 'placeholder' => trans('users.roles.form.name'), 'autofocus' => 'autofocus']) !!}
			</div>
			<div class="form-group">
				@foreach ($abilities as $ability)
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="h4 panel-title">	
								{{ trans('abilities.module_title', ["module" => trans('abilities.' . $ability->name)]) }}
								<span class="pull-right">
									{!! Form::checkbox('abilities[]', $ability->name, $role->abilities->search($ability->name)) !!}
								</span>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
		<div class="col-lg-4">
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-block">{{ trans('users.roles.form.save') }}</button>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading"><div class="panel-title h4">{{ trans('users.roles.form.users') }}</div></div>
				<table class="table table-condensed">
					@foreach($role->users as $user)
						<tr><td>{{ $user->name }}</td></tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>
	{!! Form::close() !!}

@endsection