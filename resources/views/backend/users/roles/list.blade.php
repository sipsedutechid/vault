@extends('layout.backend')

@section('title')
	{{ trans('users.roles.title') }}
@endsection

@section('nav_content')

	<a href="/backend/users" class="navbar-text navbar-back"><b class="glyphicon glyphicon-chevron-left"></b></a>
	<div class="navbar-text navbar-title">{{ trans('users.roles.title') }}</div>
	<a href="/backend/user/role/add?redirect=list" class="btn navbar-btn"><b class="glyphicon glyphicon-plus"></b> {{ trans('users.roles.add') }}</a>

@endsection

@section('content')

	<div class="panel panel-default">
		<table class="table table-default">
			<thead><tr><th>{{ trans('users.roles.name') }}</th><th style="width: 200px;">{{ trans('users.roles.created') }}</th></tr></thead>
			<tbody>
				@foreach($roles as $role)
					<tr>
						<td><a href="/backend/user/role/{{ $role->slug }}/edit">{{ $role->name }}</a></td>
						<td>{{ $role->createdBy->name or trans('users.roles.created_by_empty') }}</td>
					</tr>
				@endforeach
			</tbody>
			<tfoot><tr><th>{{ trans('users.roles.name') }}</th><th>{{ trans('users.roles.created') }}</th></tr></tfoot>
		</table>
	</div>
	
@endsection