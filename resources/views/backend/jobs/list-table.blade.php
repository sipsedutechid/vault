@foreach ($jobs as $job)
<tr>
    <td>
      <div>{{ $job->position }} - {{ $job->institution }} </div>
      <a href="/backend/job/{{ $job->slug }}/edit">{{ trans('jobs.edit') }}</a> / <a href="/backend/job/{{ $job->slug }}/trash" class="text-danger">{{ trans('jobs.trash') }}</a>
    </td>
    <td style="width: 150px;">
      <div>{{ $job->created_at }}</div>
    </td>
  </tr>
@endforeach
