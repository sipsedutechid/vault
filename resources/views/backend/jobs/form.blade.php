@extends('layout.backend')

@section('title')
	{{ trans('jobs.form_' . ($job->id ? 'edit' : 'add')) }}
@endsection

@section('nav_content')

	<div class="navbar-text navbar-back">

		<a href="/backend/jobs" title="Doctors">
			<b class="glyphicon glyphicon-chevron-left"></b>
		</a>

	</div>
	<div class="navbar-text navbar-title">
		{{ trans('jobs.form_' . ($job->id ? 'edit' : 'add')) }}
	</div>
@endsection

@section('headscript')
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.structure.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.theme.min.css') }}">
	<script type="text/javascript" src="{{ asset('libs/jquery-ui/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('assets/js/tinymce/tinymce.min.js') }}" charset="utf-8"></script>
    <script type="text/javascript">
        tinymce.init({ selector:'textarea#description' });
    </script>
@endsection

@section('content')
	{!! Form::model($job, ['files'=> true, 'url' => 'backend/job/' . ($job->id ? $job->slug . '/edit' : 'new'), 'class' => 'form-horizontal', 'novalidate' => 'novalidate', 'id' => 'formJobs']) !!}
		@if ($job->id)
			{!! Form::hidden('id') !!}
			{!! Form::hidden('slug') !!}
		@endif
		<div class="row">
			<div class="col-sm-8">

				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="panel-title h4">
							{!! trans('jobs.form_detail') !!}
							<a class="pull-right" data-toggle="collapse" href="#jobDetail"><b class="glyphicon glyphicon-menu-down"></b></a>
						</div>
					</div>
					<div class="panel-body collapse in" id="jobDetail">
                        <div class="form-group">
							<label class="control-label col-sm-3">{{ trans('jobs.form_position') }}</label>
							<div class="col-sm-9">
                                {!! Form::text('position', null, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-3">{{ trans('jobs.form_institution') }}</label>
							<div class="col-sm-9">
                                {!! Form::text('institution', null, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-3">{{ trans('jobs.form_logo') }}</label>
							<div class="col-sm-9">
								<input type="file" name="logo" placeholder="{{ trans('topics.form_logo') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-3">{{ trans('jobs.form_description') }}</label>
							<div class="col-sm-9">
                                {!! Form::textarea('desc', null, ['class' => 'form-control', 'id' => 'desc']) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-3">{{ trans('jobs.form_requirements') }}</label>
							<div class="col-sm-9">
                                {!! Form::textarea('requirements', null, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-3">{{ trans('jobs.form_location') }}</label>
							<div class="col-sm-4">
								{!! Form::textarea('address', null, ['class' => 'form-control'	]) !!}
							</div>
							<div class="col-sm-5">
								<div id="map" style="height:210px"></div> <br />
								<div style="display: block; position:absolute; top: 170px; left: 20px;">
									{!! Form::text('map_location', null, ['class' => 'form-control', 'placeholder' => 'enter location here...', 'id' => 'map_location']) !!}
								</div>
								{!! Form::text('long', null, ['id' => 'long', 'style' => 'display:none;']) !!}
								{!! Form::text('lat', null, ['id' => 'lat', 'style' => 'display:none;']) !!}
							</div>
						</div>

                        <div class="form-group">
							<label class="control-label col-sm-3">{{ trans('jobs.form_phone') }}</label>
							<div class="col-sm-9">
                                <div class="input-group">
                                    {!! Form::number('phone_number', null, ['class' => 'form-control', 'placeholder' => trans('jobs.form_contact_phone'), 'style' =>'width:60%']) !!}
                                    {!! Form::text('phone_name', null, ['class' => 'form-control',  'placeholder' => trans('jobs.form_contact_name'), 'style' =>'width:40%']) !!}
                                </div>
                            </div>
						</div>

                        <div class="form-group">
							<label class="control-label col-sm-3">{{ trans('jobs.date') }}</label>
							<div class="col-sm-9">
                                <div class="col-sm-5">
                                    {!! Form::date('start_at', null, ['class' => 'form-control']) !!}
                                </div>
								<div class="col-sm-2">
                                    Till
                                </div>
								<div class="col-sm-5">
                                    {!! Form::date('end_at', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-3">{{ trans('jobs.form_province') }}</label>
							<div class="col-sm-9">
                                {!! Form::text('province', null, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-3">{{ trans('jobs.form_city') }}</label>
							<div class="col-sm-9">
                                {!! Form::text('city', null, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-3">{{ trans('jobs.form_email') }}</label>
							<div class="col-sm-9">
                                {!! Form::text('email', null, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-3">{{ trans('jobs.form_website') }}</label>
							<div class="col-sm-9">
                                {!! Form::text('website', null, ['class' => 'form-control']) !!}
							</div>
						</div>

					</div>
				</div>

			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<div class="col-xs-12">
						@if (!$job->id)
							<button type="submit" name="post_action" value="new" class="btn btn-primary btn-block">{{ trans('jobs.form_cdp_save') }}</button>
							<button type="submit" name="post_action" value="add_other" class="btn btn-default btn-block">{{ trans('jobs.form_cdp_save_add_other') }}</button>
						@else
							<button type="submit" name="post_action" value="edit" class="btn btn-primary btn-block">{{ trans('jobs.form_cdp_update') }}</button>
							<button type="submit" name="post_action" value="continue" class="btn btn-default btn-block">{{ trans('jobs.form_cdp_update_continue') }}</button>
						@endif
					</div>
				</div>

			</div>
		</div>

	{!! Form::close() !!}
	<script>
      	function initMap() {
	        const uluru = {lat: -25.363, lng: 131.044};
	        const map = new google.maps.Map(document.getElementById('map'), {
	        	zoom: 4,
	          	center: uluru
	        });

			let marker;

			marker = new google.maps.Marker({
				position: uluru,
				map: map
			});

			const input = document.getElementById('map_location');
			const latForm = document.getElementById('lat');
			const lngForm = document.getElementById('long');

			const autocomplete = new google.maps.places.Autocomplete(input);
			autocomplete.bindTo('bounds', map);

			autocomplete.addListener('place_changed', () => {
				var place = autocomplete.getPlace();
      			if (!place.geometry) {
		            // User entered the name of a Place that was not suggested and
		            // pressed the Enter key, or the Place Details request failed.
		            window.alert("No details available for input: '" + place.name + "'");
		            return;
	            } else {
					map.setCenter(place.geometry.location);
					map.setZoom(16);
					marker.setPosition(place.geometry.location);
	          		marker.setVisible(true);
					latForm.value = place.geometry.location.lat();
					lngForm.value = place.geometry.location.lng();
				}
			});

			map.addListener('click', function(job) {
				marker.setPosition(job.latLng);

				latForm.value = job.latLng.lat();
				lngForm.value = job.latLng.lng();
			});
      	}
    </script>
    <script async defer
    	src="https://maps.googleapis.com/maps/api/js?key={{ env('MAPS_KEY') }}&libraries=places&callback=initMap">
    </script>
@endsection
