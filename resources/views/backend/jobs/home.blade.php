@extends('layout.backend')

@section('title')
	{{ trans('jobs.title') }}
@endsection

@section('nav_content')

	<div class="navbar-text navbar-title">{{ trans('jobs.title') }}</div>
	<a href="/backend/job/new" class="btn navbar-btn"><b class="glyphicon glyphicon-plus"></b>&ensp;{{ trans('jobs.add') }}</a>

@endsection

@section('content')

	<div class="row">
		<div class="col-lg-6">
				<div class="panel panel-default">
					<div class="panel-body h3">
						<a href="/backend/job/all">
							{{ trans('jobs.home_all') }} <span class="badge pull-right">{{ $job_count }}</span>
						</a>
					</div>
					<table class="table table-condensed">
						<thead><tr><th colspan="2">{{ trans('jobs.home_all_recent') }}</th></tr></thead>
						<tbody>
							@foreach($recent_jobs as $job)
								<tr>
									<td style="width:100px;">{{ $job->created_at }}</td>
									<td><a href="/backend/job/{{ $job->slug }}/edit?redirect={{ urlencode('backend/jobs') }}">{{ $job->position }}</a></td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</a>
		</div>
	</div>

@endsection
