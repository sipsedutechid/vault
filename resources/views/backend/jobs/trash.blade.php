@extends('layout.backend')

@section('title') Move {{ $job->position }} ({{ $job->institution }}) to Trash @endsection

@section('content')

	<h1>Move to Trash</h1>
	{!! Form::open(['url' => 'backend/job/trash']) !!}
		<p>
			You are about to move the job <strong>{{ $job->position }}</strong> to trash.<br />
			Are you sure? This action cannot be undone.
		</p>
		{!! Form::hidden('slug', $job->slug) !!}
		<a href="/backend/job/all" class="btn btn-default">No, take me back</a>
		<button type="submit" class="btn btn-danger">Yes, move this job to trash</button>

	{!! Form::close() !!}

@endsection