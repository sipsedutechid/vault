@foreach ($diseases as $disease)
    <tr>
        <td>
            <div>{{ $disease->name }}</div>
            <a href="/backend/disease/{{ $disease->slug }}/edit">{{ trans('diseases.edit') }}</a> / <a href="/backend/disease/{{ $disease->slug }}/trash" class="text-danger">{{ trans('diseases.trash') }}</a>
        </td>
        <td style="max-width:450px;">
            <div class="ellipsis">
                {!! $disease->parent ? 'Parent - <a href="/backend/disease/' . $disease->parent->slug . '/edit">' . $disease->parent->name . '</a>' : '' !!}
            </div>
        </td>
    </tr>
@endforeach
