@extends('layout.backend')

@section('title') Move {{ $disease->name }} to Trash @endsection

@section('content')

	<h1>Move to Trash</h1>
	{!! Form::open(['url' => 'backend/disease/trash']) !!}
		<p>
			You are about to move the disease <strong>{{ $disease->name }}</strong> to trash.<br />
			Are you sure? This action cannot be undone.
		</p>
		{!! Form::hidden('slug', $disease->slug) !!}
		<a href="/backend/disease/all" class="btn btn-default">No, take me back</a>
		<button type="submit" class="btn btn-danger">Yes, move this disease to trash</button>

	{!! Form::close() !!}

@endsection
