@extends('layout.backend')

@section('title')
	{{ trans('disease.form_' . ($disease->id ? 'edit' : 'add')) }}
@endsection

@section('nav_content')

	<div class="navbar-text navbar-back">

		<a href="/backend/topics" title="Topics">
			<b class="glyphicon glyphicon-chevron-left"></b>
		</a>

	</div>
	<div class="navbar-text navbar-title">
		{{ trans('diseases.form_' . ($disease->id ? 'edit' : 'add')) }}
	</div>
@endsection

@section('headscript')
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.structure.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.theme.min.css') }}">
	<script type="text/javascript" src="{{ asset('libs/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/js/tinymce/tinymce.min.js') }}" charset="utf-8"></script>
	<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}" media="screen" charset="utf-8">
@endsection

@section('content')

	{!! Form::model($disease, ['url' => 'backend/disease/' . ($disease->id ? $disease->slug . '/edit' : 'new') . ($request->redirect ? '/?redirect=' . urlencode($request->redirect) : ''), 'class' => 'form-horizontal', 'novalidate' => 'novalidate', 'id' => 'formTopic']) !!}
		@if ($disease->id)
			{!! Form::hidden('id') !!}
			{!! Form::hidden('slug') !!}
		@endif
		<div class="row">
			<div class="col-sm-8">

				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="panel-title h4">
							{!! trans('diseases.form_detail') !!}
							<a class="pull-right" data-toggle="collapse" href="#diseaseDetail"><b class="glyphicon glyphicon-menu-down"></b></a>
						</div>
					</div>
					<div class="panel-body collapse in" id="diseaseDetail">
						<div class="form-group">
							<label class="control-label col-sm-2">{{ trans('diseases.form_name') }}</label>
							<div class="col-sm-10">
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">{{ trans('diseases.form_parent') }}</label>
							<div class="col-sm-10">
                                {!! Form::select('parent_id', $parents, null, [ 'class' => 'form-control']) !!}
							</div>
						</div>

					</div>
				</div>

			</div>

            <div class="col-sm-4">
				<div class="form-group">
					<div class="col-xs-12">
						@if (!$disease->id)
							<button type="submit" name="post_action" value="all" class="btn btn-primary btn-block">{{ trans('diseases.form_def_save') }}</button>
							<button type="submit" name="post_action" value="new" class="btn btn-default btn-block">{{ trans('diseases.form_def_save_add_other') }}</button>
						@else
							<button type="submit" name="post_action" value="all" class="btn btn-primary btn-block">{{ trans('diseases.form_def_update') }}</button>
							<button type="submit" name="post_action" value="edit" class="btn btn-default btn-block">{{ trans('diseases.form_def_update_continue') }}</button>
						@endif
					</div>
				</div>

			</div>
		</div>

	{!! Form::close() !!}

@endsection
