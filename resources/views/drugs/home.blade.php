@extends('layout.frontend')

@section('title') | Drugs @endsection

@section('hscripts')

	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.structure.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery-ui/jquery-ui.theme.min.css') }}">
	<script type="text/javascript" src="{{ asset('libs/jquery-ui/jquery-ui.min.js') }}"></script>

@endsection

@section('content')

	<div class="section">
		<div class="jumbotron">
			<div class="container text-center">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h2>Search from <strong>thousands</strong> of drugs</h2>
						{!! Form::open(['url' => '/drugs/search', 'method' => 'get', 'id' => 'omnisearch']) !!}
							<div class="input-group input-group-lg">
								{!! Form::text('q', null, ['class' => 'form-control input-lg', 'placeholder' => 'ex. Paracetamol', 'autofocus' => 'autofocus']) !!}
								<span class="input-group-btn">
									<button type="submit" class="btn btn-default btn-lg"><b class="glyphicon glyphicon-search"></b></button>
								</span>
							</div>

						{!! Form::close() !!}
						<br />

						<p>
							or <a href="/drugs/browse">browse alphabetically</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('fscripts')

	<script type="text/javascript">
		$(document).ready(function () {
			$("#omnisearch [name=q]").autocomplete({
				source: '/service/list/drugs',
				minLength: 3,
				response: function (e, ui) {
					$.each(ui.content, function(index, item) {
						item.value = item.slug;
						item.label = item.name;
					});
				},
				focus: function (e, ui) {
					return false;
				},
				select: function (e, ui) {
					$(this).val(ui.item.label);
				}
			});
		});
	</script>

@endsection