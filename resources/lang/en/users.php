<?php

return [
	'form'			=> [
		'back_list'			=> 'Back to Users List',
		'name'				=> 'Full Name',
		'new_role'			=> 'Define New Role',
		'password'			=> 'Password',
		'password_verify'	=> 'Verify Password',
		'role'				=> 'Role',
		'submit'			=> 'Submit',
		'title_add'			=> 'Add New User',
		'title_edit'		=> 'Edit User',
		'username'			=> 'Username',
	],
	'home_all'		=> 'All Users',
	'home_roles'	=> 'All Roles',
	'list'			=> [
		'add'		=> 'Add New',
		'name'		=> 'Full Name',
		'role'		=> 'Role',
		'since'		=> 'User Since',
		'title'		=> 'Users List',
		'username'	=> 'Username',
	],
	'roles'			=> [
		'add'				=> 'Add New',
		'created'			=> 'Created',
		'created_by_empty'	=> 'System',
		'form'				=> [
			'edit'		=> 'Edit Role',
			'module'	=> 'Module :module',
			'save'		=> 'Save Changes',
			'users'		=> 'Assigned Users',
		],
		'name'				=> 'Role Name',
		'title'				=> 'Roles List',
	],
	'title'			=> 'Users',
];