<?php
    return [
        'list_title' => 'All Content' ,

        'add'        => 'Add' ,
        'edit'       => 'Edit' ,
        'trash'      => 'Trash' ,

        'form_title'        => 'Title' ,
        'form_detail'       => 'Detail' ,
        'form_appearance'   => 'Appearance' ,
        'form_edit'         => 'Edit Content' ,
        'form_level'        => 'Level',
        'order'             => 'Order' ,

        'list_search_clear' => 'Clear'
    ]
?>
