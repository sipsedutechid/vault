<?php
    return [
        'list_title' => 'All Disease' ,
        'list_search'   => 'Search Disease...' ,

        'add'   => 'Add' ,
        'edit'  => 'Edit' ,
        'trash' => 'Trash' ,

        'form_edit'     => 'Edit Disease' ,
        'form_detail'   => 'Detail' ,
        'form_name'     => 'Name' ,
        'form_parent'   => 'Parent',

        'form_def_update'           => 'Update' ,
        'form_def_update_continue'  => 'Update and Continue'
    ]
?>
