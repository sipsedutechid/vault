<?php

return [

    'modal_title' => 'New Article Journal' ,

    'form' 	=> [
        'detail_title'      => 'Article' ,
        'new'	            => 'New',
        'title'             => 'Title' ,
        'author'            => 'Author' ,
        'publish_date'      => 'Publish Date' ,
        'file'              => 'Journal pdf' ,
        'save'				=> 'Save' ,
		'saving'			=> 'Saving' ,
		'success_saving'	=> 'Success' ,
		'failed_saving'		=> 'Failed Saving' ,
        'volume'            => 'Jilid'
    ] ,
];
