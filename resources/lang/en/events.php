<?php

return [
    'title' 		    => 'Events',
    'home_all'          => 'All Events' ,
    'home_all_recent'   => 'Recently Added' ,
    'add'               => 'Add New' ,
    'list_title'        => 'Event List' ,
    'edit'              => 'Edit' ,
    'trash'             => 'Trash' ,
    'list_none'         => 'No Events Found' ,
    'list_search'       => 'Search Events...' ,
    'list_search_clear' => 'Clear' ,
    'list_search_empty' => 'No Events found for ":Term".' ,
    'back_dashboard'    => 'Back' ,

    'form_cdp_save'            => 'Save' ,
    'form_cdp_save_add_other'   => 'Save and add Other' ,
    'form_cdp_update'           => 'Update' ,
    'form_cdp_update_continue'  => 'Update and Continue' ,
    'form_name'                 => 'Event Name' ,
    'form_add'                  => 'Form Add' ,
    'form_edit'                 => 'Form Edit' ,
    'form_detail'               => 'Detail' ,
    'form_organizer'            => 'Organizer' ,
    'form_description'          => 'Description' ,
    'form_location'             => 'Location' ,
    'form_phone'                => 'Phone' ,
    'form_date'                 => 'Date' ,
    'form_contact_phone' 	    => 'Phone' ,
	'form_contact_name' 		=> 'Name'
]
?>
