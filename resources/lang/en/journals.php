<?php

return [
	'list_title'		=> 'All Journal' ,
	'edit'				=> 'Edit' ,
	'trash'				=> 'Trash' ,
	'list_search'		=> 'Search' ,
	'back_to_dashboard'	=> 'Back To Journal Dashboard' ,
	'tags_instruction'	=> 'Use comma to separate tag' ,
	'title'				=> 'Journals' ,
	'explanation'		=> 'Journals are books made up of several scholarly articles' ,
	'posted_journals'	=> 'Posted Journals' ,
	'draft'				=> 'Draft' ,
	'list_none'			=> 'Jurnal is empty' ,
	'back_dashboard'	=> 'Back' ,

	'form'		=> [
		'title' 			=> 'Title' ,
		'detail'			=> 'Journal Detail' ,
		'issn'				=> 'ISSN' ,
		'e_issn'			=> 'E-ISSN' ,
		'description'		=> 'Description' ,
		'publisher'			=> 'Publisher' ,
		'editor'			=> 'Editor' ,
		'cover'				=> 'Cover' ,
		'save'				=> 'Save' ,
		'save_add_other'	=> 'Save and add other' ,
		'update'			=> 'Update' ,
		'update_continue'	=> 'Update and continue editing' ,
		'access'			=> 'Access' ,
		'category'			=> 'Category' ,
		'specialist'		=> 'Specialist' ,
		'tags'				=> 'Tags' ,
		'save_publish'		=> 'Save & Publish' ,
		'save_draft'		=> 'Save as Draft' ,
		'publish_date'		=> 'Date Publish' ,
		'update_publish'	=> 'Update and Publish' ,
		'update_draft'		=> 'Save to Draft' ,
		'additional'		=> 'Additional' ,
		'free'				=> 'Free' ,
		'url'				=> 'URL'
	] ,

	'category' => [
		'list_title'		=> 'All Category' ,
		'list_search'		=> 'Sarch Category' ,
		'add'				=> 'Add' ,
		'back_to_dashboard'	=> 'Back To List Category' ,
		'actions_add'		=> 'Add Category' ,
		'actions_edit'		=> 'Edit Category' ,
		'form_label'		=> 'Label' ,
		'form_description'	=> 'Description'
	] ,

	'actions'	=> [
		'add'	=> 'New Journal',
		'edit'	=> 'Edit'
	],

	'add'		=> [
		'title'	=> 'New Journal',
	],

	'home'		=> [
		'empty_recents'	=> 'No journals have been added recently',
		'title'			=> 'Journals',
		'title_all'		=> 'All Journals',
		'title_recents'	=> 'Recently Added',
	] ,
];
