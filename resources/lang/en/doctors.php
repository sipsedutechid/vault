<?php

return [
	'title' 		=> 'Doctors',
    'description' 	=> 'Lorem ipsum dolor sit amet',
    'add' 			=> 'Add New',
    'edit' 			=> 'Edit',
    'trash' 		=> 'Trash',
    'created_by' 	=> 'Created by :name',
    'bulk_action' 	=> 'Bulk Action',

    'home_all' 			=> 'All Doctors',
	'home_all_recent' 	=> 'Recently Added' ,

	'form_edit' 				=> 'Form Edit' ,
	'form_add' 					=> 'Form Add' ,
	'form_detail' 				=> 'Doctor Details' ,
	'form_cdp_add'				=> 'Add' ,
	'form_cdp_cancel'			=> 'Cancel' ,
	'form_cdp_save' 			=> 'Save' ,
	'form_cdp_save_add_other' 	=> 'Save and add other' ,
	'form_cdp_update'			=> 'Update' ,
	'form_cdp_update_continue'	=> 'Update and continue editing' ,
	'form_select'				=> 'Please Select' ,

	'form_first_title' 	=> 'First Title' ,
	'form_name' 		=> 'Name' ,
	'form_back_title' 	=> 'Back Title' ,
	'form_gender' 		=> 'Gender' ,
	'form_gender_male' 	=> 'Male' ,
	'form_gender_female'=> 'Female' ,
	'form_birth' 		=> 'Birth' ,
	'form_birth_place' 	=> 'Place' ,
	'form_email' 		=> 'Email' ,
	'form_phone' 		=> 'Phone Number' ,
	'form_phone_home' 	=> 'Home' ,
	'form_phone_mobile' => 'Mobile' ,
	'form_address' 		=> 'Address' ,

	'form_academic' 				=> 'Academic Forms' ,
	'form_add_academic' 			=> 'Add Form' ,
	'form_academic_level' 			=> 'Level Academic' ,
	'form_academic_school_college'	=> 'School / College' ,
	'form_academic_years' 			=> 'Year' ,
	'form_academic_address'			=> 'Address' ,

	'form_clinic'			=> 'Clinic Forms' ,
	'form_add_clinic'		=> 'Add Form' ,
	'form_clinic_name'		=> 'Name' ,
	'form_clinic_address'	=> 'Address' ,
	'form_clinic_city'		=> 'City' ,
	'form_clinic_phone'		=> 'Phone' ,

	'form_specialist' 					=> 'Specialist' ,
	'form_specialist_class' 			=> 'Class' ,
	'form_specialist_sub_class'			=> 'Subclass' ,
	'form_select_specialist_subclass' 	=> 'Select Class First'
];
