<?php

return [
	'journal_saved_published' 	=> 'Journal published',
	'journal_saved_scheduled'	=> 'Journal scheduled to publish',
	'journal_saved_draft' 		=> 'Journal saved as draft',
	'journal_saved_updated'		=> 'Journal updated',
	'journal_updated'			=> 'Journal updated' ,

	'drug_saved' 						=> 'Drug saved successfully',
	'drug_updated' 						=> 'Drug updated',
	'drug_trash' 						=> 'Drug moved to trash',
	'drug_manufacturer_saved' 			=> 'Drug manufacturer saved successfully',
	'drug_manufacturer_updated' 		=> 'Drug manufacturer updated',
	'drug_manufacturer_trash' 			=> 'Drug manufacturer moved to trash',
	'drug_manufacturer_logo_removed'	=> 'Logo removed',
	'drug_generic_merged' 				=> 'Generic :old successfully merged with :new',
	'drug_generic_saved' 				=> 'Generic saved',
	
	'topic_saved'		=> 'Topic saved successfully',
	'topic_updated'	=> 'Topic updated',
	'topic_trash'		=> ':title moved to trash' ,
	
	'event_saved'		=> 'Event saved successfully',
	'event_updated'	=> 'Event updated',
	'event_trash'		=> ':title moved to trash' ,

	'job_saved'		=> 'Job saved successfully',
	'job_updated'	=> 'Job updated',
	'job_trash'		=> ':title moved to trash' ,

	'content_updated'	=> 'Content updated'
];
