<?php

return [
	'title' => 'GakkenVAULT',

	'sign_out' => 'Sign Out',
	'language' => 'Language',
	'menu_dashboard' => 'Dashboard',
	'menu_articles' => 'Articles',
	'menu_journals' => 'Journals',
	'menu_drugs' => 'Drugs',
	'menu_doctors' => 'Doctors',
	'menu_hospitals' => 'Hospitals',
	'menu_users' => 'Users',
	'menu_topics' => 'Topics' ,
	'menu_events' => 'Events' ,
	'menu_jobs' => 'Jobs' ,

	'load_more'	=> 'Load more..',
];
