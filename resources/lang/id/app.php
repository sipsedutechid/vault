<?php

return [
	'sign_out' => 'Keluar',
	'language' => 'Bahasa',
	'menu_dashboard' => 'Dasbor',
	'menu_articles' => 'Artikel',
	'menu_journals' => 'Jurnal',
	'menu_drugs' => 'Obat',
	'menu_doctors' => 'Dokter',
	'menu_hospitals' => 'Rumah Sakit',
	'menu_users' => 'Pengguna',
	'menu_topics' => 'Topik' ,
	'menu_events' => 'Kegiatan' ,
	'menu_jobs' => 'Lowongan Kerja' ,

	'load_more'	=> 'Muat Lebih..',
];
