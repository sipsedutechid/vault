<?php

return [
	'journal_saved_published' 	=> 'Jurnal dipublikasi',
	'journal_saved_scheduled'	=> 'Jurnal terjadwal',
	'journal_saved_draft' 		=> 'Jurnal tersimpan sebagai draf',
	'journal_saved_updated'		=> 'Jurnal diperbarui',
	'journal_updated'			=> 'Jurnal diperbarui' ,

	'drug_saved' => 'Obat disimpan',
	'drug_updated' => 'Obat diperbarui',
	'drug_trash' => 'Obat dihapus',
	'drug_manufacturer_saved' => 'Pabrik disimpan',
	'drug_manufacturer_updated' => 'Pabrik diperbarui',
	'drug_manufacturer_trash' => 'Pabrik dihapus',
	'drug_manufacturer_logo_removed' => 'Logo dihapus',
	'drug_generic_merged' 				=> 'Generic :old successfully merged with :new',
	'drug_generic_saved' 				=> 'Generic saved',
	
	'topic_saved'		=> 'Topik disimpan',
	'topic_updated'	=> 'Topik diperbarui',
	'topic_trash'		=> ':title dihapus' ,
	
	'event_saved'		=> 'Kegiatan disimpan',
	'event_updated'	=> 'Kegiatan diperbarui',
	'event_trash'		=> ':title berhasil dihapus' ,

	'job_saved'		=> 'Lowongan kerja disimpan',
	'job_updated'	=> 'Lowongan kerja diperbarui',
	'job_trash'		=> ':title berhasil dihapus' ,

	'content_updated'	=> 'Konten diperbarui'
];
