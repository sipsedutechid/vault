<?php

return [

    'modal_title' => 'Artikel Jurnal Baru' ,

    'form' 	=> [
        'detail_title'      => 'Artikel' ,
        'new'	            => 'Tambahkan',
        'title'             => 'Judul' ,
        'author'            => 'Penulis' ,
        'publish_date'      => 'Tanggal Terbit' ,
        'file'              => 'pdf Jurnal' ,
        'save'				=> 'Simpan' ,
		'saving'			=> 'Menyimpan' ,
		'success_saving'	=> 'Berhasil Disimpan' ,
		'failed_saving'		=> 'Gagal Disimpan' ,
        'volume'            => 'Jilid'
    ] ,
];
