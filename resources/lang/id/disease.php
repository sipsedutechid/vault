<?php
    return [
        'list_title' => 'Semua Penyakit' ,
        'list_search'   => 'Cari Penyakit...' ,

        'add'   => 'Add' ,
        'edit'  => 'Edit' ,
        'trash' => 'Trash' ,

        'form_edit'     => 'Edit Disease' ,
        'form_detail'   => 'Detail' ,
        'form_name'     => 'Name' ,
        'form_parent'   => 'Parent',

        'form_def_update'           => 'Update' ,
        'form_def_update_continue'  => 'Update and Continue'
    ]
?>
