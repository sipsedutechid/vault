<?php

return [
	'list_title'		=> 'Semua Jurnal' ,
	'edit'				=> 'Ubah' ,
	'trash'				=> 'Hapus' ,
	'list_search'		=> 'Cari' ,
	'back_to_dashboard'	=> 'Kembali ke dashboard journal' ,
	'tags_instruction'	=> 'Guanakan koma untuk memisahtan label' ,
	'title'				=> 'Jurnal' ,
	'explanation'		=> 'Jurnal adalah buku yang terdiri dari beberapa tulisan ilmiah' ,
	'posted_journals'	=> 'Jurnal terpulikasikan' ,
	'draft'				=> 'draf' ,
	'list_none'			=> 'Jurnal kosong' ,
	'back_dashboard'	=> 'Kembali' ,

	'form'		=> [
		'title' 			=> 'Judul' ,
		'detail'			=> 'Rincian Jurnal' ,
		'issn'				=> 'ISSN' ,
		'e_issn'			=> 'E-ISSN' ,
		'description'		=> 'Deskripsi' ,
		'publisher'			=> 'Penerbit' ,
		'editor'			=> 'Penyunting' ,
		'cover'				=> 'Sampul' ,
		'save'				=> 'Simpan' ,
		'save_add_other'	=> 'Simpan dan tambah baru' ,
		'update'			=> 'Perbarui' ,
		'update_continue'	=> 'Perbarui dan lanjut suntign' ,
		'access'			=> 'Akses' ,
		'category'			=> 'Kategori' ,
		'specialist'		=> 'Spesialis' ,
		'tags'				=> 'Label' ,
		'save_publish'		=> 'Simpan dan Publikasikan' ,
		'save_draft'		=> 'Simpan sebagai draf' ,
		'publish_date'		=> 'Tanggal publikasi' ,
		'update_publish'	=> 'Perbarui dan Publikasikan' ,
		'update_draft'		=> 'Prebarui dan masukkan ke draf' ,
		'additional'		=> 'Tambahan' ,
		'free'				=> 'Free' ,
		'url'			=> 'URL'
	] ,

	'category' => [
		'list_title'		=> 'Semua Kategori' ,
		'list_search'		=> 'Cari Kategori' ,
		'add'				=> 'Add' ,
		'back_to_dashboard'	=> 'Kembali ke daftar kategori' ,
		'actions_add'		=> 'Tambah Category' ,
		'actions_edit'		=> 'Edit Category' ,
		'form_label'		=> 'Label' ,
		'form_description'	=> 'Deksripsi'
	] ,

	'actions'	=> [
		'add'	=> 'Jurnal Baru',
		'edit'	=> 'Ubah'
	],

	'add'		=> [
		'title'	=> 'Jurnal Baru',
	],

	'home'		=> [
		'empty_recents'	=> 'Jurnal masih kosong',
		'title'			=> 'Jurnal',
		'title_all'		=> 'Semua Jurnal',
		'title_recents'	=> 'Terakhir ditambahkan',
	] ,
];
