<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'sign_in' => 'Masuk',
    'failed' => 'Data pengguna tidak dapat ditemukan',
    'throttle' => 'Sudah terlalu banyak sign-in gagal. Coba lagi setelah :seconds detik.',
    'pre_warning' => 'Akses hanya diperbolehkan untuk staf',
    'username' => 'Nama pengguna',
    'password' => 'Kata sandi',
    'remember_me' => 'Ingat saya',

];
