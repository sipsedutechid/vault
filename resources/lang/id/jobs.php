<?php

return [
	'title' => 'GakkenVAULT',

	'home_all'          => 'Semua lowongan kerja' ,
	'home_all_recent'   => 'Baru Ditambahkan' ,
	'add'               => 'Buat Baru' ,
	'list_title'        => 'Daftar lowongan kerja' ,
	'edit'              => 'Ubah' ,
	'trash'             => 'Hapus' ,
	'list_none'         => 'Kosong' ,
	'list_search'       => 'Cari lowongan kerja...' ,
	'list_search_clear' => 'Kosongkan' ,
	'list_search_empty' => 'Lowongan kerja dengan kata kunci ":Term" tidak ditemukan' ,
	'back_dashboard'    => 'Kembali' ,

	'form_cpd_save'             => 'Simpan' ,
	'form_cdp_save_add_other'   => 'Simpan dan tambahkan baru' ,
	'form_cdp_update'           => 'Perbarui' ,
	'form_cdp_update_continue'  => 'Perbarui dan lanjut mengubah' ,
	'form_name'                 => 'Nama Kegiatan' ,
	'form_add'                  => 'Form Menambah' ,
	'form_edit'                 => 'Form Mengubah' ,
	'form_detail'               => 'Detail' ,
	'form_position'             => 'Posisi' ,
	'form_institution'          => 'Insitusi' ,
	'form_logo'          				=> 'Logo' ,
	'form_description' 					=> 'Deskripsi' ,
	'form_requirements' 				=> 'Persyaratan' ,
	'form_location' 						=> 'Lokasi' ,
	'form_phone' 								=> 'Telfon' ,
	'form_contact_phone' 				=> 'Telfon' ,
	'form_contact_name' 				=> 'Nama' ,
	'form_date' 								=> 'Tanggal' ,
	'form_province' 						=> 'Porvinsi' ,
	'form_city' 								=> 'Kota' ,
	'form_email' 								=> 'Email' ,
	'form_website' 							=> 'Website'
];
