<?php

return [
    'title' 		    => 'Kegiatan',
    'home_all'          => 'Semua Kegiatan' ,
    'home_all_recent'   => 'Baru ditambahkan' ,
    'add'               => 'Tambahkan' ,
    'list_title'        => 'kegiatan List' ,
    'edit'              => 'Edit' ,
    'trash'             => 'Trash' ,
    'list_none'         => 'Tidak ada kegiatan yang ditemukan' ,
    'list_search'       => 'Mencari kegiatan...' ,
    'list_search_clear' => 'Hapus' ,
    'list_search_empty' => 'Tidak ditemukan kegiatan dengan kata kunci ":Term".',
    'back_dashboard'    => 'Kembali' ,

    'form_cpd_save'             => 'Simpan' ,
    'form_cdp_save_add_other'   => 'Simpan dan tambahkan baru' ,
    'form_cdp_update'           => 'Perbarui' ,
    'form_cdp_update_continue'  => 'Perbarui dan lanjut ubah kegiatan' ,
    'form_name'                 => 'Nama Kegiatan' ,
    'form_add'                  => 'Menambahkan Kegiatan' ,
    'form_edit'                 => 'Mengubah Kegiatan' ,
    'form_detail'               => 'Detail' ,
    'form_organizer'            => 'Pengelolah' ,
    'form_description'          => 'Deskripsi' ,
    'form_location'             => 'Lokasi' ,
    'form_phone'                => 'Telfon' ,
    'form_date'                 => 'Tanggal' ,
    'form_contact_phone'        => 'Telfon' ,
	'form_contact_name' 		=> 'Nama' ,
]
?>
