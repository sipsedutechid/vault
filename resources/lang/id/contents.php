<?php
    return [
        'list_title' => 'Semua Konten' ,

        'add'        => 'Tambahkan' ,
        'edit'       => 'Ubah' ,
        'trash'      => 'Hapus' ,

        'form_title'        => 'Judul' ,
        'form_detail'       => 'Detail' ,
        'form_appearance'   => 'Tampilan' ,
        'form_edit'         => 'Ubah Konten' ,
        'form_level'        => 'Level' ,
        'order'             => 'Urutan' ,

        'list_seaech_clear' => 'Clear'
    ]
?>
